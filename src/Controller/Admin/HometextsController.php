<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Hometexts Controller
 *
 * @property \App\Model\Table\HometextsTable $Hometexts
 */
class HometextsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $hometexts = $this->Hometexts->find()->order(['position'=>'ASC']);
        $title = "Gestion du texte de la page d'accueil";

        $this->set(compact('hometexts', 'title'));
    }

    /**
     * View method
     *
     * @param string|null $id hometext id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $hometext = $this->Hometext->get($id, [
            'contain' => []
        ]);

        $this->set('hometext', $hometext);
        $this->set('_serialize', ['hometext']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $hometext = $this->Hometexts->newEntity();
        if ($this->request->is('post')) {
            $hometext = $this->Hometexts->patchEntity($hometext, $this->request->data);
            if ($this->Hometexts->save($hometext)) {
                $this->Flash->success(__('The hometext has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The hometext could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('hometext'));
        $this->set('_serialize', ['hometext']);
    }

    /**
     * Edit method
     *
     * @param string|null $id hometext id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $hometext = $this->Hometexts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $hometext = $this->Hometexts->patchEntity($hometext, $this->request->data);
            if ($this->Hometexts->save($hometext)) {
                $this->Flash->success(__('The hometext has been saved.'));
            } else {
                $this->Flash->error(__('The hometext could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['controller'=>'Hometexts','action' => 'homehometext']);
    }

    /**
     * Delete method
     *
     * @param string|null $id hometext id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $hometext = $this->Hometexts->get($id);
        if ($this->Hometexts->delete($hometext)) {
            $this->Flash->success(__('The hometext has been deleted.'));
        } else {
            $this->Flash->error(__('The hometext could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
