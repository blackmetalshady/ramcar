<?php
//namespace App\Controller;
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\I18n;
use Cake\Event\Event;
/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 */
class CarsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $cars = $this->Cars->find()->contain([
            'Medias'=> function ($q) {
                           return $q->order(['Medias.primary_img' => 'DESC']);
                        },
            'Types',
            'Models',
            'Brands' => function ($q) {
                           return $q->where(['Brands.disabled' => '0']);
                        }])->toArray();
        //print_r($cars);die();
        $this->loadModel('Brands');
        $brands = $this->Brands->find('list')
                               ->select(['id','name'])
                               ->where(['disabled'=>'0'])
                               ->order(['name'=>'ASC']);
        $this->loadModel('Models');
        $models = $this->Models->find()->combine('id', 'name');

        $this->loadModel('Types');
        $types = $this->Types->find()->combine('id', 'name_fr');

        $this->loadModel('Fuels');
        $fuels = $this->Fuels->find()->combine('id', 'name_fr');

        $title = __('Cars Listing');

        $carEntity = $this->Cars->newEntity();
        $this->set(compact('cars','types','models','brands','title','fuels','carEntity'));
        $this->set('_serialize', ['cars']);
    }

    /**
     * View method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);

        $this->set('car', $car);
        $this->set('_serialize', ['car']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $car = $this->Cars->newEntity();
        if ($this->request->is('post')) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('car'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('car'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $car = $this->Cars->get($id);
        if ($this->Cars->delete($car)) {
            $this->Flash->success(__('The car has been deleted.'));
        } else {
            $this->Flash->error(__('The car could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function exists($id = null){
                $car = $this->Cars->find()
                                  ->where([
                                      'registration'=>$this->request->data['matricule']
                                      ])
                                  ->first();
                //print_r($pompiste); die();
                if (!empty($car) && $car->id != $id) {
                    echo "ko";
                }else{
                    echo "ok";
                }

        $this->autoRender = false ;
    }
}
