<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Prices Controller
 *
 * @property \App\Model\Table\PricesTable $Prices
 */
class PricesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($car_id = null)
    {
        $this->loadModel('Cars');
        $priceEntity = $this->Prices->newEntity();
        $title = "Gestion des prix";

        if (!empty($car_id)) {
            $prices = $this->Prices->find()
                               ->contain(['Cars'=>['Brands','Models']])
                               ->where(['car_id'=>$car_id])
                               ->order(['car_id'=>'ASC','from_day'=>'ASC'])
                               ->toArray();
            $car = $this->Cars->get($car_id,['contain'=>['Brands','Models']]);
            $this->set(compact('prices','title','car','priceEntity'));
        }else{
            $prices = $this->Prices->find()
                               ->contain(['Cars'=>['Brands','Models']])
                               ->order(['car_id'=>'ASC','from_day'=>'ASC']);

            $cars_list = $this->Cars->find()->contain(['Brands','Models']);
            $cars = array();
            foreach ($cars_list as $car) {
                $cars[$car->id] = $car->brand->name." ".$car->model->name." (".$car->registration.")";
            }
            $car = null;
        }
        
        $this->set(compact('prices','title','cars','car','priceEntity','car_id'));
        //////
        
        //$this->set('_serialize', ['prices']);
    }

    public function extras($extra_id = null)
    {
        if (isset($extra_id) && $extra_id >0 && $extra_id !=null) {
            $prices = $this->Prices->find()
                               ->contain(['Extras'])
                               ->where(['extra_id'=>$extra_id])
                               ->order(['extra_id'=>'ASC','from_day'=>'ASC'])
                               ->toArray();
        }else{
            $prices = $this->Prices->find()
                               ->contain(['Extras'])
                               ->order(['extra_id'=>'ASC','from_day'=>'ASC']);
        }
        
        $this->loadModel('Extras');
        //////
        $extras_list = $this->Extras->find();
        $extras = array();
        foreach ($extras_list as $extra) {
            $extras[$extra->id] = $extra->name;
        }
        $priceEntity = $this->Prices->newEntity();
        $title = "Gestion des prix des Extras";
        $this->set(compact('prices','title','extras','priceEntity','extra_id'));
    }

    /**
     * View method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => ['Cars']
        ]);

        $this->set('price', $price);
        $this->set('_serialize', ['price']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $price = $this->Prices->newEntity();
        if ($this->request->is('post')) {
            $price = $this->Prices->patchEntity($price, $this->request->data);
            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));
                return $this->redirect(['action' => 'index', $this->request->data['car_id']]);
            } else {
                print_r($price); die();
                $this->Flash->error(__('The price could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $price = $this->Prices->patchEntity($price, $this->request->data);
            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));
            } else {
                $this->Flash->error(__('The price could not be saved. Please, try again.'));
            }

            if (!empty($this->request->data['car_id'])) {
                return $this->redirect(['action' => 'index', $this->request->data['car_id']]);
            }else if (!empty($this->request->data['extra_id'])){
                return $this->redirect(['action' => 'extras', $this->request->data['extra_id']]);
            }
            
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $price = $this->Prices->get($id);
        if ($this->Prices->delete($price)) {
            $this->Flash->success(__('The price has been deleted.'));
        } else {
            $this->Flash->error(__('The price could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
