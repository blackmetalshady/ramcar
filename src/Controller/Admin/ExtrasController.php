<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Extras Controller
 *
 * @property \App\Model\Table\ExtrasTable $Extras
 */
class ExtrasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $extras = $this->Extras->find();
        $title = "Gestion des Extras";
        $types = array(
                'gps' => 'GPS',
                'ics'=>__('Infant Child Seats'),
                'hc'=>__('Hand Controle')
                );
        $extraEntity = $this->Extras->newEntity();
        $this->set(compact('extras','title','types','extraEntity'));
        //$this->set('_serialize', ['extras']);
    }

    /**
     * View method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $extra = $this->Extras->get($id, [
            'contain' => ['Medias']
        ]);

        $this->set('extra', $extra);
        $this->set('_serialize', ['extra']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $extra = $this->Extras->newEntity();
        if ($this->request->is('post')) {
            $upload_dir = WWW_ROOT.'img/extras/';
            $filename   = basename($this->request->data['img']['name']);
            $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            $newname    = 'extra_'.time().'.'.$extension;
            $finalfile = $upload_dir.$newname;
            if(move_uploaded_file($this->request->data['img']['tmp_name'], $finalfile)){
                $this->request->data['img'] = $newname ;
                $extra = $this->Extras->patchEntity($extra, $this->request->data);
                if ($this->Extras->save($extra)) {
                    $this->Flash->success(__('The extra has been saved.'));
                } else {
                    $this->Flash->error(__('The extra could not be saved. Please, try again.'));
                }
            }
            
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $extra = $this->Extras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->request->data['img']['size'] > 0) {
                $upload_dir = WWW_ROOT.'img/extras/';
                $finalfile = $upload_dir.$extra->img;

                if(move_uploaded_file($this->request->data['img']['tmp_name'], $finalfile)){
                    $this->request->data['img'] = $extra->img;
                }else{
                    echo "upload error";
                }
            }else{
                $this->request->data['img'] = $extra->img;
            }
            $extra = $this->Extras->patchEntity($extra, $this->request->data);
            if ($this->Extras->save($extra)) {
                $this->Flash->success(__('The extra has been saved.'));
            } else {
                print_r($extras); die();
                $this->Flash->error(__('The extra could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $extra = $this->Extras->get($id);
        if ($this->Extras->delete($extra)) {
            $this->Flash->success(__('The extra has been deleted.'));
        } else {
            $this->Flash->error(__('The extra could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
