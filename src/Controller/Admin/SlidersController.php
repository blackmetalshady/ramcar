<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Sliders Controller
 *
 * @property \App\Model\Table\SlidersTable $Sliders
 */
class SlidersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $sliders = $this->paginate($this->Sliders);

        $this->set(compact('sliders'));
        $this->set('_serialize', ['sliders']);
    }

    /**
     * View method
     *
     * @param string|null $id Slider id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $slider = $this->Sliders->get($id, [
            'contain' => []
        ]);

        $this->set('slider', $slider);
        $this->set('_serialize', ['slider']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $slider = $this->Sliders->newEntity();
        if ($this->request->is('post')) {
            $slider = $this->Sliders->patchEntity($slider, $this->request->data);
            if ($this->Sliders->save($slider)) {
                $this->Flash->success(__('The slider has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The slider could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('slider'));
        $this->set('_serialize', ['slider']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Slider id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $slider = $this->Sliders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $slider = $this->Sliders->patchEntity($slider, $this->request->data);
            if ($this->Sliders->save($slider)) {
                $this->Flash->success(__('The slider has been saved.'));
            } else {
                $this->Flash->error(__('The slider could not be saved. Please, try again.'));
            }
        }
        return $this->redirect(['controller'=>'Sliders','action' => 'homeslider']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Slider id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $slider = $this->Sliders->get($id);
        if ($this->Sliders->delete($slider)) {
            $this->Flash->success(__('The slider has been deleted.'));
        } else {
            $this->Flash->error(__('The slider could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function carousel(){
        $carousels = $this->Sliders->find()
                                  ->contain(['Cars'=> [
                                        'sort' => ['Slidercars.cars_order' => 'ASC'],
                                        'Medias'=>['sort'=>'primary_img'],
                                        'Brands',
                                        'Fuels',
                                        'Models'
                                    ]])
                                  ->where(['slider_type'=>'carousel'])->toArray();
        $carsincarousel = array();
        foreach ($carousels[0]->cars as $car) {
            $carsincarousel[] = $car->id;
        }

        $this->loadModel('Slidercars');
        $slidercarEntity = $this->Slidercars->newEntity();

        $this->loadModel('Cars');
        $carlist = $this->Cars->find()
                           ->contain(['Brands','Models','Fuels'])
                           ->where(function ($exp) use ($carsincarousel){
                                return $exp->notIn('Cars.id', $carsincarousel);
                            })
                           ->toArray();
        $cars = array();
        foreach ($carlist as $car) {
            $cars[$car->id] = $car->brand->name." ".$car->model->name." (".$car->registration.")";
        }
        //print_r($cars);die();
        $title = "Gestion du Carousel d'Acceuil";
        //print_r($carousels[0]); die();
        $this->set(compact('carousels','title','slidercarEntity','cars'));
    }

    public function homeslider(){
        $homeslider = $this->Sliders->find()
                                  ->contain(['Medias'=>['sort'=>['front_img'=>'DESC']]])
                                  ->where(['slider_type'=>'slider'])->first();

        $title = "Gestion du Slider d'Acceuil";
        $this->set(compact('homeslider','title','slidercarEntity','cars'));
        //$this->autoRender = false;
    }
}
