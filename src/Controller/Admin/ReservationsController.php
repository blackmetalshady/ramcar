<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Event\Event;

/**
 * Reservations Controller
 *
 * @property \App\Model\Table\ReservationsTable $Reservations
 */
class ReservationsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow(['add','status']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($status = null)
    {
        $reservations = $this->Reservations->find()
                                           ->contain([
                                                'Guests',
                                                'Cars'=>[
                                                    'Medias'=>[
                                                        'sort'=>['primary_img'=>'DESC']
                                                    ]
                                                ],
                                                'Extras',
                                                'Users'
                                                ])
                                           
                                           ->where(['confirmed'=>'1'])
                                           ->order(['id'=>'DESC']);
        $title = "List des reservations confirmées";

        $this->loadModel('Extras');
        $extras = $this->Extras->find()->combine('id', 'name_fr');;
        //print_r($reservations); die();
        $this->set(compact('reservations','title','extras','status'));
        $this->set('_serialize', ['reservations']);
    }

    public function rejected($status = null)
    {
        $reservations = $this->Reservations->find()
                                           ->contain([
                                                'Guests',
                                                'Cars'=>[
                                                    'Medias'=>[
                                                        'sort'=>['primary_img'=>'DESC']
                                                    ]
                                                ],
                                                'Extras',
                                                'Users'
                                                ])
                                           
                                           ->where(['rejected'=>'1'])
                                           ->order(['id'=>'DESC']);
        $title = "List des reservations rejetées";

        $this->loadModel('Extras');
        $extras = $this->Extras->find()->combine('id', 'name_fr');;
        //print_r($reservations); die();
        $this->set(compact('reservations','title','extras','status'));
        $this->set('_serialize', ['reservations']);
    }

    public function requests($status = null)
    {
        $reservations = $this->Reservations->find()
                                           ->contain([
                                                'Guests',
                                                'Cars'=>[
                                                    'Medias'=>[
                                                        'sort'=>['primary_img'=>'DESC']
                                                    ]
                                                ],
                                                'Extras',
                                                'Users'
                                                ])
                                           
                                           ->where(['confirmed'=>'0','rejected'=>'0','canceled'=>'0'])
                                           ->order(['id'=>'DESC']);

        $title = "List des nouvelles demandes de reservations";

        $this->loadModel('Extras');
        $extras = $this->Extras->find()->combine('id', 'name_fr');;
        //print_r($reservations); die();
        $this->set(compact('reservations','title','extras','status'));
        $this->set('_serialize', ['reservations']);
    }

    public function edit($id = null)
    {
        //print_r($this->referer()); die();
        $status = true;
        $reservation = $this->Reservations->get($id, [
            'contain' => []
        ]);
        $data = ['from_date' => $this->request->data['from_date'],
                 'from_time' => $this->request->data['from_time'],
                 'to_date' => $this->request->data['to_date'],
                 'to_time' => $this->request->data['to_time'],
                 'pickup_location' => $this->request->data['pickup_location'],
                 'return_location' => $this->request->data['return_location']
                 ];

        $extras = $this->request->data['extras'];

        if ($this->request->is(['patch', 'post', 'put'])) {
            $reservation = $this->Reservations->patchEntity($reservation, $data);
            if ($this->Reservations->save($reservation)) {
                echo "success save reservation";
                $this->loadModel('Extrasreservations');
                $resextras = $this->Extrasreservations->find()
                                                      ->where(['reservation_id'=>$id])
                                                      ->toArray();
                if (!empty($resextras)) {
                    if(!$this->Extrasreservations->deleteAll(['reservation_id' => $id])){
                        $status = false;
                    }
                }
                
                foreach ($extras as $ex) {
                    $extra = $this->Extrasreservations->newEntity();
                    $extra = $this->Extrasreservations->patchEntity($extra, ['extra_id'=>$ex, 'reservation_id' => $id]);
                    if(!$this->Extrasreservations->save($extra)){
                        $status = false;
                        break;
                    }
                }
            } else {
                $status = false;
            }

            return $this->redirect($this->referer().'?status='.$status);
        }
    }

    public function cancel($id){
        $status = true;
        $reservation = $this->Reservations->get($id, [
            'contain' => []
        ]);

        $cancel = 0;
        if ($reservation->canceled == 0) {
            $cancel = 1;
        }

        $reservation = $this->Reservations->patchEntity($reservation, ['canceled'=>$cancel]);
        //print_r($reservation); die();
        if (!$this->Reservations->save($reservation)) {
            $status = false;
        }
        return $this->redirect(['action' => 'index', $status]);
    }

    public function confirm($id){
        $reservation = $this->Reservations->get($id, [
            'contain' => []
        ]);
         $reservation = $this->Reservations->patchEntity($reservation, ['confirmed'=>'1','rejected'=>'0','canceled'=>'0']);

         if (!$this->Reservations->save($reservation)) {
            $this->Flash->error('Une Erreur est Survenue lors de la confirmation de la reservation, veuillez reessayer plutard');
         }

         return $this->redirect($this->referer());
    }

    public function reject($id){
        $reservation = $this->Reservations->get($id, [
            'contain' => []
        ]);
         $reservation = $this->Reservations->patchEntity($reservation, ['confirmed'=>'0','rejected'=>'1','canceled'=>'0']);

         if (!$this->Reservations->save($reservation)) {
            $this->Flash->error('Une Erreur est Survenue lors du rejet de la reservation, veuillez reessayer plutard');
         }

         return $this->redirect($this->referer());
    }
    /**
     * Delete method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reservation = $this->Reservations->get($id);
        if ($this->Reservations->delete($reservation)) {
            $this->Flash->success(__('The reservation has been deleted.'));
        } else {
            $this->Flash->error(__('The reservation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function generatecode(){
        $chars = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'];

        $str = '';
        for ($i=0; $i < 8; $i++) { 
            $str .= $chars[array_rand($chars, 1)];
        }
        //echo '-----> string '.$str;
        return $str;
    }

    private function unique(){
        //echo "entred unique";
        while (true) {
            $str = $this->generatecode();
            //echo '-----> string from unique '.$str;
            $resbycode = $this->Reservations->find()
                           ->where(['code'=>$str])
                           ->toArray();
            //print_r($code); die();
            if (empty($resbycode)) {
                break;
            }
        }
        //echo 'unique code ----> '.
        //print_r($code);
        return $str;
    }

}
