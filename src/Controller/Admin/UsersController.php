<?php
namespace App\Controller;
namespace App\Controller\Admin;

use Cake\Event\Event;
use App\Controller\Admin\AppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add', 'logout','login','index']);
    }

     public function index()
     {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function edit($id = null)
    {
        $id = $this->request->session()->read('Auth.User')['id'];
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'profile']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                if ($user['role'] === 'admin') {
                    return $this->redirect(['prefix'=>'admin', 'controller'=>'Cars','action'=>'index']);
                }else{
                    return $this->redirect('/');
                }
            }else{
                $msg = 'Utilisateur ou Mot de passe Incorrecte';
            }
            $this->set(compact('msg'));
            //$this->Flash->error(__(''));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function profile()
    {
       $id = $this->request->session()->read('Auth.User')['id'];
       $user = $this->Users->get($id); 
       $this->loadModel('Countries');
       $countries = $this->Countries->find('all');
       $this->set(compact('user','countries'));
    }

    public function changepassword(){
        $id = $this->request->session()->read('Auth.User')['id'];
        $user = $this->Users->get($id); 
        $passwordhasher = new DefaultPasswordHasher;
        if (!$passwordhasher->check($this->request->data['password'], $user->password)) {
            echo "oldpassnomatch";
        }else if (strlen($this->request->data['newpassword']) < 6) {
            echo "lengtherror";
        }else if ($this->request->data['newpassword'] != $this->request->data['renewpassword']) {
            echo "newpassnomatch";
        }else{
            $user = $this->Users->patchEntity($user,['password'=> $this->request->data['newpassword']]);
                if ($this->Users->save($user)) {

                    echo "success";
                }else{
                    echo "fail";
                }
        }
        $this->autoRender=false;
    }
}
