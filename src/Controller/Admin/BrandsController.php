<?php
//namespace App\Controller;
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\I18n;
use Cake\Event\Event;
/**
 * Brands Controller
 *
 * @property \App\Model\Table\BrandsTable $Brands
 */
class BrandsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $brands = $this->Brands->find('all')->order(['name'=>'ASC']);
        $title = __('Brands Listing');
        $this->set(compact('brands','title'));
        $this->set('_serialize', ['brands']);
    }


    /**
     * Edit method
     *
     * @param string|null $id brand id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $brand = $this->Brands->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $brand = $this->Brands->patchEntity($brand, $this->request->data);
            if ($this->Brands->save($brand)) {
                $this->Flash->success(__('The brand has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The brand could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('brand'));
        $this->set('_serialize', ['brand']);
    }

    public function enable($id){
        $brand = $this->Brands->get($id);
        $brand = $this->Brands->patchEntity($brand, ['disabled'=>'0']);
        if ($this->Brands->save($brand)) {
            $this->Flash->success(__('The brand has been enabled.'));
        } else {
            $this->Flash->error(__('The brand could not be enabled. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function disable($id){
        $brand = $this->Brands->get($id);
        $brand = $this->Brands->patchEntity($brand, ['disabled'=>'1']);
        if ($this->Brands->save($brand)) {
            $this->Flash->success(__('The brand has been enabled.'));
        } else {
            $this->Flash->error(__('The brand could not be enabled. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
