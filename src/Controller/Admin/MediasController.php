<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Filesystem\File;

/**
 * Medias Controller
 *
 * @property \App\Model\Table\MediasTable $Medias
 */
class MediasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cars']
        ];
        $medias = $this->paginate($this->Medias);

        $this->set(compact('medias'));
        $this->set('_serialize', ['medias']);
    }

    /**
     * View method
     *
     * @param string|null $id Media id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($carid = null)
    {
        $medias = $this->Medias->find()
                               ->where(['car_id'=>$carid])
                               ->order(['primary_img'=>'DESC'])
                               ->toArray();
        $this->loadModel('Cars');
        $car = $this->Cars->get($carid, [
            'contain' => ['Brands','Models','Fuels']
        ]);
        //print_r($car); die();
        $title = "Gestion des images de la Voiture";
        $this->set(compact('medias', 'title','car'));
        $this->set('_serialize', ['medias']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $media = $this->Medias->newEntity();
        if ($this->request->is('post')) {
            $media = $this->Medias->patchEntity($media, $this->request->data);
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The media could not be saved. Please, try again.'));
            }
        }
        $cars = $this->Medias->Cars->find('list', ['limit' => 200]);
        $this->set(compact('media', 'cars'));
        $this->set('_serialize', ['media']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Media id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $media = $this->Medias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $media = $this->Medias->patchEntity($media, $this->request->data);
            if ($this->Medias->save($media)) {
                $this->Flash->success(__('The media has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The media could not be saved. Please, try again.'));
            }
        }
        $cars = $this->Medias->Cars->find('list', ['limit' => 200]);
        $this->set(compact('media', 'cars'));
        $this->set('_serialize', ['media']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Media id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $type = null)
    {

        $this->request->allowMethod(['post', 'delete']);
        $media = $this->Medias->get($id);
        if ($this->Medias->delete($media)) {
            $folder = (isset($type) && $type=='slider') ? 'sliders' : 'cars' ;
            $filepath = WWW_ROOT.'img/'.$folder.'/'.$media->name;
            $file = new File($filepath);
            $file->delete();
            $this->Flash->success(__('The media has been deleted.'));
        } else {
            $this->Flash->error(__('The media could not be deleted. Please, try again.'));
        }
        //redirection a la bonne page, si slider redirection a la page des sliders
        if (isset($type) && $type=='slider') {
            return $this->redirect(['controller'=>'Sliders','action' => 'homeslider']);
        }else{
            return $this->redirect(['controller'=>'Medias','action' => 'view', $media->car_id]);
        }
        
    }

    public function upload(){

        if (isset($this->request->data['type']) && $this->request->data['type'] == 'slider') {
            $upload_dir = WWW_ROOT.'img/sliders/';
            $filename   = basename($this->request->data['img']['name']);
            
            if (isset($this->request->data['frontimg'])) {
                $newname    = 'slider_front.png';
            }else{
                $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                $newname    = 'sliderimg_'.time().'.'.$extension;
            }
            $finalfile = WWW_ROOT.'img/sliders/'.$newname;
        }else{
            $upload_dir = WWW_ROOT.'img/cars/';
            $filename   = basename($this->request->data['img']['name']);
            $extension  = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            $newname    = 'car_'.time().'.'.$extension;
            $finalfile = WWW_ROOT.'img/cars/'.$newname;
        }
        
        ///// si image et slider
        if(move_uploaded_file($this->request->data['img']['tmp_name'], $finalfile)){
            if (isset($this->request->data['type'])) {
                if ($this->request->data['type'] == 'slider') {
                    if (isset($this->request->data['frontimg'])) {
                        echo "success";
                    }else{
                        $media = $this->Medias->newEntity();
                        $newEntity = array(
                            'type'=>$this->request->data['img']['type'],
                            'size'=>$this->request->data['img']['size'],
                            'name'=>$newname
                            );
                        $media = $this->Medias->patchEntity($media, $newEntity);

                        if ($result = $this->Medias->save($media)) {
                            $this->loadModel('Slidermedias');
                            $slidermedia = $this->Slidermedias->newEntity();
                            $params = array(
                                        'slider_id'=>$this->request->data['slider_id'],
                                        'media_id'=>$result->id,
                                        'front_img'=>'0'
                                      );
                            $slidermedia = $this->Slidermedias->patchEntity($slidermedia, $params);
                            if ($this->Slidermedias->save($slidermedia)) {
                                echo "success";
                            }else{
                                //print_r(expression)
                                echo "fail";
                            }
                            
                        }else{
                            echo "fail";
                        }
                    }
                    
                }
            }else{
                $media = $this->Medias->newEntity();
                $newEntity = array(
                    'type'=>$this->request->data['img']['type'],
                    'size'=>$this->request->data['img']['size'],
                    'name'=>$newname,
                    'car_id'=>$this->request->data['car_id']
                    );
                $media = $this->Medias->patchEntity($media, $newEntity);

                if ($this->Medias->save($media)) {
                    echo "success";
                }else{
                    echo "fail";
                }
            }
            
        }else{
            echo "fail";
        }

        $this->autoRender = false;
    }

    public function setprimary(){
        $list = $this->Medias->find()
                             ->where(['car_id'=>$this->request->data['car_id']])
                             ->toArray();


        if($this->Medias->updateAll(['primary_img' => '0'],['car_id' => $this->request->data['car_id']])){
            $primary = $this->Medias->get($this->request->data['id']);
            $primary = $this->Medias->patchEntity($primary, $this->request->data);
            //print_r($primary);
            if ($this->Medias->save($primary)) {
                echo "ok";
            }else{
                echo "ko";
            }
        }else{
            echo "ko";
        }

        $this->autoRender = false;
    }
}
