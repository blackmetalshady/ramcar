<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Sliders Controller
 *
 * @property \App\Model\Table\SlidersTable $Sliders
 */
class MessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $messages = $this->Messages->find()
                                         ->contain(['FromUser','ChildMessages'])
                                         ->where(['is_read'=>'0'])
                                         ->andWhere(function ($exp, $q) {
                                                return $exp->isNull('Messages.to_user');
                                            })
                                         ->order(['Messages.sent_at']);
        $title = "Gestion des Messages non Lus";
        $this->set(compact('messages','title'));
    }

    public function old()
    {
        $messages = $this->Messages->find()
                                   ->contain(['FromUser','ChildMessages'])
                                   ->where(['is_read'=>'1'])
                                   ->andWhere(function ($exp, $q) {
                                          return $exp->isNull('Messages.to_user');
                                      })
                                   ->order(['Messages.sent_at']);
        $title = "Gestion des anciens Messages";
        $this->set(compact('messages','title'));
    }
    /**
     * Delete method
     *
     * @param string|null $id Slider id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'get']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function markasread(){
        if ($this->request->is('post')) {
            $message = $this->Messages->get($this->request->data['id'], [
                'contain' => []
            ]);
            $message = $this->Messages->patchEntity($message, ['is_read'=>'1']);
            if($this->Messages->save($message)){
                echo "true";
            }else{ print_r($message); }
        }
        $this->autoRender = false;
    }

    public function conversation($id){
        $message = $this->Messages->get($id, [
                'contain' => ['ChildMessages'=>[
                                'sort'=>'ChildMessages.sent_at'
                                ],
                              'FromUser'
                              ]
            ]);

        $message = $this->Messages->patchEntity($message, ['is_read'=>'1']);
        if($this->Messages->save($message)){
                $this->Messages->query()
                               ->update()
                               ->set(['is_read' => '1'])
                               ->where(['parent_id' => $message->id, 'from_user !='=>$this->request->session()->read('Auth.User.id')])
                               ->execute();
            }else{ print_r($message); }
        $title = "Conversation";
        $this->set(compact('message','title'));
    }

    public function addanswer(){
        if ($this->request->is('post')) {
            //IF no user id was sent an user is logged (anonymous users must provide an email) then add the logged user id as the user_id for the message
            if (empty($this->request->data['from_user']) && empty($this->request->data['email'])) {
                $this->request->data['from_user'] = $this->request->session()->read('Auth.User.id');
            }
            //print_r($this->request->data); die();
            $message = $this->Messages->newEntity();
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if($this->Messages->save($message)){
                echo "success";
            }else{
                print_r($message);
            }
        }
        $this->autoRender = false;
    }
}
