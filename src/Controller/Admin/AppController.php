<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'logoutRedirect' => [
                'prefix'=>false,
                'controller' => 'Pages',
                'action' => 'index'
            ],
            'authorize' => 'Controller'
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function isAuthorized($user = null)
        {
            $stat = false;
            // Any registered user can access public functions
            if (empty($this->request->params['prefix'])) {
                $stat = true;
                return true;
            }

            // Only admins can access admin functions
            if ($this->request->params['prefix'] === 'admin') {
                return (bool)($user['role'] === 'admin');
            }
            //echo $stat; die();
            // Default deny
            return false;
        }

    public function beforeFilter(Event $event)
    {
        $notifications = array();
        //$this->Auth->allow(['index', 'view']);
        $this->loadModel('Reservations');
        $nouvellesdemandes = $this->Reservations->find()->where(['confirmed'=>'0','rejected'=>'0','canceled'=>'0'])->count();
        if ($nouvellesdemandes > 0) {
            array_push($notifications, ['value'=>$nouvellesdemandes, 'message'=>'Nouvelle(s) demande(s)','icon'=>'car','url'=>'/admin/reservations/requests']);
        }
        $this->loadModel('Messages');
        $unreadmessages = $this->Messages->find()
                                    ->contain(['FromUser','ChildMessages'])
                                    ->where(['is_read'=>'0'])
                                    ->andWhere(function ($exp, $q) {
                                        return $exp->isNull('Messages.to_user');
                                    });
        $count = 0;
        foreach ($unreadmessages as $msg) {
            if ($msg->fromUser->role != 'admin'){
                $count++;
            }
        }
        if ($count > 0) {
            array_push($notifications, ['value'=>$count, 'message'=>'Nouveau(x) Message(s)','icon'=>'envelope', 'url'=>'/admin/messages']);
        }
        $this->set(compact('notifications'));
    }
}
