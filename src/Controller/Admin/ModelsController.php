<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\I18n\Time;

/**
 * Models Controller
 *
 * @property \App\Model\Table\ModelsTable $Models
 */
class ModelsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $models = $this->Models->find()->contain(['Brands']);
        $modelEntity = $this->Models->newEntity();

        $this->loadModel('Brands');
        $brands = $this->Brands->find()->order(['name'=>'ASC'])->combine('id', 'name');

        $now = Time::now();
        $range = array();
        foreach (range(2007, $now->year) as $year) {
            $range[$year] = $year;
        }
        $title = __('Models Listing');
        $this->set(compact('models','modelEntity','brands','title','range'));
        $this->set('_serialize', ['models']);
    }

    /**
     * View method
     *
     * @param string|null $id Model id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $model = $this->Models->get($id, [
            'contain' => ['Cars']
        ]);

        $this->set('model', $model);
        $this->set('_serialize', ['model']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $model = $this->Models->newEntity();
        if ($this->request->is('post')) {
            $model = $this->Models->patchEntity($model, $this->request->data);
            if ($this->Models->save($model)) {
                $this->Flash->success(__('The model has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The model could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('model'));
        $this->set('_serialize', ['model']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Model id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $model = $this->Models->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $model = $this->Models->patchEntity($model, $this->request->data);
            if ($this->Models->save($model)) {
                $this->Flash->success(__('The model has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The model could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('model'));
        $this->set('_serialize', ['model']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Model id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $model = $this->Models->get($id);
        if ($this->Models->delete($model)) {
            $this->Flash->success(__('The model has been deleted.'));
        } else {
            $this->Flash->error(__('The model could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function loadbrandmodels(){
        $models = $this->Models->find()
                               ->where([
                                'brand_id'=>$this->request->data['brandid']
                                ])->toArray();
        if (!empty($models)) {
            $modelOptions = "<option value=''>(Choisissez un Model)</option>";
            foreach ($models as $model) {
                $modelOptions .= "<option value='".$model->id."' >".$model->name."</option>";
            }
        }else{
            $modelOptions = '';
        }
        

        echo $modelOptions;
        $this->autoRender = false;
    }

    public function exists($id = null){
        $model = $this->Models->find()
                          ->where([
                              'name'=>$this->request->data['name'],
                              'brand_id'=>$this->request->data['brand_id'],
                              'year'=>$this->request->data['year']
                              ])
                          ->first();
        if (!empty($model) && $model->id != $id) {
            echo "ko";
        }else{
            echo "ok";
        }
        $this->autoRender = false ;
    }
}
