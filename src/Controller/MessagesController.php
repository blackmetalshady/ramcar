<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $messages = $this->Messages
                         ->find()
                         ->where(['Messages.to_user'=>$this->request->session()->read('Auth.User.id')])
                         ->orWhere(['Messages.from_user'=>$this->request->session()->read('Auth.User.id')])
                         ->contain(['FromUser', 'ParentMessages'=>[
                                                    'ChildMessages'=>[
                                                        'sort'=>['sent_at'=>'DESC']
                                                    ]
                                                ]
                                            ])
                         ->order(['Messages.sent_at'])
                         ->group(['Messages.parent_id']);
        //print_r($messages->toArray()); die();
        $this->loadModel('Cars');
        $cars = $this->Cars->find()
                           ->contain([
                                'Brands',
                                'Models',
                                'Types',
                                'Fuels',
                                'Medias'=>[
                                    'sort'=>[
                                        'primary_img'=>'DESC',
                                        ]
                                    ],
                                'Prices'=>[
                                    'sort'=>[
                                        'price_mad'=>'ASC'
                                    ]
                                ]
                            ])
                           ->where(['published'=>'1'])
                           ->order('RAND()')
                           ->limit(3)
                           ->toArray();
        $this->set(compact('messages','cars'));
        $this->set('_serialize', ['messages']);
    }

    /**
     * View method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => ['Users', 'ParentMessages', 'ChildMessages']
        ]);

        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $message = $this->Messages->newEntity();
        if ($this->request->is('post')) {
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The message could not be saved. Please, try again.'));
            }
        }
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $parentMessages = $this->Messages->ParentMessages->find('list', ['limit' => 200]);
        $this->set(compact('message', 'users', 'parentMessages'));
        $this->set('_serialize', ['message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $message = $this->Messages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->data);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('The message has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The message could not be saved. Please, try again.'));
            }
        }
        $users = $this->Messages->Users->find('list', ['limit' => 200]);
        $parentMessages = $this->Messages->ParentMessages->find('list', ['limit' => 200]);
        $this->set(compact('message', 'users', 'parentMessages'));
        $this->set('_serialize', ['message']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Message id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $message = $this->Messages->get($id);
        if ($this->Messages->delete($message)) {
            $this->Flash->success(__('The message has been deleted.'));
        } else {
            $this->Flash->error(__('The message could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
