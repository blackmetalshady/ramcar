<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Slidercars Controller
 *
 * @property \App\Model\Table\SlidercarsTable $Slidercars
 */
class SlidercarsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sliders', 'Cars']
        ];
        $slidercars = $this->paginate($this->Slidercars);

        $this->set(compact('slidercars'));
        $this->set('_serialize', ['slidercars']);
    }

    /**
     * View method
     *
     * @param string|null $id Slidercar id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $slidercar = $this->Slidercars->get($id, [
            'contain' => ['Sliders', 'Cars']
        ]);

        $this->set('slidercar', $slidercar);
        $this->set('_serialize', ['slidercar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $slidercar = $this->Slidercars->newEntity();
        if ($this->request->is('post')) {
            $slidercar = $this->Slidercars->patchEntity($slidercar, $this->request->data);
            if ($this->Slidercars->save($slidercar)) {
                $this->Flash->success(__('The slidercar has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The slidercar could not be saved. Please, try again.'));
            }
        }
        $sliders = $this->Slidercars->Sliders->find('list', ['limit' => 200]);
        $cars = $this->Slidercars->Cars->find('list', ['limit' => 200]);
        $this->set(compact('slidercar', 'sliders', 'cars'));
        $this->set('_serialize', ['slidercar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Slidercar id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $slidercar = $this->Slidercars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $slidercar = $this->Slidercars->patchEntity($slidercar, $this->request->data);
            if ($this->Slidercars->save($slidercar)) {
                $this->Flash->success(__('The slidercar has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The slidercar could not be saved. Please, try again.'));
            }
        }
        $sliders = $this->Slidercars->Sliders->find('list', ['limit' => 200]);
        $cars = $this->Slidercars->Cars->find('list', ['limit' => 200]);
        $this->set(compact('slidercar', 'sliders', 'cars'));
        $this->set('_serialize', ['slidercar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Slidercar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $slidercar = $this->Slidercars->get($id);
        if ($this->Slidercars->delete($slidercar)) {
            $this->Flash->success(__('The slidercar has been deleted.'));
        } else {
            $this->Flash->error(__('The slidercar could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
