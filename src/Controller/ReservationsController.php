<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Reservations Controller
 *
 * @property \App\Model\Table\ReservationsTable $Reservations
 */
class ReservationsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['add','status']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $reservations = $this->paginate($this->Reservations);

        $this->set(compact('reservations'));
        $this->set('_serialize', ['reservations']);
    }

    /**
     * View method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $reservation = $this->Reservations->get($id, [
            'contain' => ['Users', 'Reservationarticles']
        ]);

        $this->set('reservation', $reservation);
        $this->set('_serialize', ['reservation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $reservation = $this->Reservations->newEntity();
        if ($this->request->is('post')) {
            //See if Cart has a car
            $cart = $this->request->session()->read('Cart');
            //print_r($cart); die();
            if (!empty($cart['car_id'])) {
                //get reservation informations
                $reservation = $cart['searchinfo'];

                //See if the user is a registred and authenticated user
                if ($this->Auth->user()) {
                    $reservation['user_id'] = $this->request->session()->read('Auth.User.id');
                //if the user is a Guest
                }else{
                    //initiat a EMPTY or NOT stat to true
                    $stat = true;
                    //For each data send check if it's not empty
                    foreach ($this->request->data as $input) {
                        if (empty($input)) {
                            $stat = false;
                            break;
                        }
                    }
                    //if all value has data, check if email and confirm_email match (case insensitive)
                    if ($stat && strcasecmp($this->request->data['email'], $this->request->data['confirm_email']) == 0) {

                        $this->loadModel('Guests');
                        $guest = $this->Guests->newEntity();
                        $guest = $this->Guests->patchEntity($guest, $this->request->data);
                        //Saving the Guest data and get the result row
                        if ($g = $this->Guests->save($guest)) {
                            //set guest_id from the newly added guest
                            $reservation['guest_id'] = $guest->id;
                        }else{
                            return $this->redirect(
                                ['controller' => 'Cars', 'action' => 'index']
                            );
                        }
                    //if an input was empty or emails doesn't match, redirect
                    }else{
                        return $this->redirect(
                            ['controller' => 'Cars', 'action' => 'index']
                        );
                    }
                }
                //Generating the unique code for Guests
                $reservation['code'] = $this->unique();
                //$reservation['confirmed'] = 0;
                //print_r($reservation); die();
                $rsv = $this->Reservations->newEntity();
                $rsv = $this->Reservations->patchEntity($rsv, $reservation);
                //Saving the reservation
                //print_r($res); die();
                if ($res = $this->Reservations->save($rsv)) {
                    //IF Success, start saving the car
                    $this->loadModel('Carsreservations');
                    $carsv = $this->Carsreservations->newEntity();
                    $carsv = $this->Carsreservations->patchEntity($carsv, [
                                 'car_id'=>$cart['car_id'],
                                 'reservation_id'=>$res->id
                             ]);
                    //Saving the car reservation, Success
                    if ($this->Carsreservations->save($carsv)) {
                        //if Success, start saving extras
                        $this->loadModel('Extrasreservations');
                        //For each extra, save it
                        foreach ($cart['extras'] as $extra) {
                            $extrarsv = $this->Extrasreservations->newEntity();
                            $extrarsv = $this->Extrasreservations->patchEntity($extrarsv, [
                                         'extra_id'=>$extra,
                                         'reservation_id'=>$res->id
                                     ]);
                            //print_r($extrarsv); die();
                            if (!$this->Extrasreservations->save($extrarsv)) {
                                print_r($extrarsv); die();
                                break;
                            }
                        }
                        //IF Everything went ok, if the user is logged redirect to his Cart
                        $this->request->session()->delete('Cart');
                        if ($this->Auth->user()) {
                            return $this->redirect(
                                ['controller' => 'Cars', 'action' => 'index']
                            );
                        }else{
                            $rescode = $reservation['code'];
                            $this->set(compact('rescode'));
                        }
                        //echo "succes"; die();
                    //if Saving Car Reservation Faild
                    }else{
                        print_r($carsv);die();
                        return $this->redirect(
                            ['controller' => 'Cars', 'action' => 'index']
                        );
                    }
                //IF reservation save Faild    
                }else{
                    print_r($rsv); die();
                    return $this->redirect(
                        ['controller' => 'Cars', 'action' => 'index']
                    );
                }
            //If Cart is empty, redirect
            }else{
                 if ($this->Auth->user()){
                    return $this->redirect(
                        ['controller' => 'Users', 'action' => 'cart']
                    );
                 }
            }
        }else{
            //IF Request is not POST
            return $this->redirect(['controller'=>'Cars','action' => 'index']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $reservation = $this->Reservations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reservation = $this->Reservations->patchEntity($reservation, $this->request->data);
            if ($this->Reservations->save($reservation)) {
                $this->Flash->success(__('The reservation has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The reservation could not be saved. Please, try again.'));
            }
        }
        $users = $this->Reservations->Users->find('list', ['limit' => 200]);
        $this->set(compact('reservation', 'users'));
        $this->set('_serialize', ['reservation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Reservation id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $reservation = $this->Reservations->get($id);
        if ($this->Reservations->delete($reservation)) {
            $this->Flash->success(__('The reservation has been deleted.'));
        } else {
            $this->Flash->error(__('The reservation could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function generatecode(){
        $chars = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'];

        $str = '';
        for ($i=0; $i < 8; $i++) { 
            $str .= $chars[array_rand($chars, 1)];
        }
        //echo '-----> string '.$str;
        return $str;
    }

    private function unique(){
        //echo "entred unique";
        while (true) {
            $str = $this->generatecode();
            //echo '-----> string from unique '.$str;
            $resbycode = $this->Reservations->find()
                           ->where(['code'=>$str])
                           ->toArray();
            //print_r($code); die();
            if (empty($resbycode)) {
                break;
            }
        }
        //echo 'unique code ----> '.
        //print_r($code);
        return $str;
    }

    public function status(){
        if ($this->request->is('post')) {
            $reservation = $this->Reservations->find()
                                            ->contain([
                                                'Guests',
                                                'Cars'=>[
                                                    'Brands',
                                                    'Models',
                                                    'Types',
                                                    'Fuels',
                                                    'Medias'=>[
                                                        'sort'=>[
                                                            'primary_img'=>'DESC'
                                                            ]
                                                        ]
                                                    ],
                                                'Extras'])
                                            ->where(['code'=>$this->request->data['code']])
                                            ->first();
            //print_r($reservation); die();
            $this->set(compact('reservation'));
        }
    }
   /* public function find()
    {
        $from_date = $this->request->data['from_date'];
        $to_date = $this->request->data['to_date'];
        $cartype = $this->request->data['cartype'];

        $reservedcar_ids = $this->Reservations
                                ->find()
                                ->select(['id'])
                                ->where(function ($exp) use ($from_date, $to_date) {
                                    return $exp->between('from_date', $from_date, $to_date);
                                  });
        $this->loadModel('Cars');
        $cars = $this->Cars->find()
                           ->where(['type_id'=>$cartype, function ($exp) {
                                return $exp->notIn('id', $cars);
                            }])
    }*/
}
