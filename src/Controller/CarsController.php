<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Event\Event;
/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 */
class CarsController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index','lang','find', 'addtocart','review']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('Brands');
        $carsbybrand = $this->Brands->find()
                                    ->contain(['Cars'])
                                    ->where(['disabled'=>'0'])
                                    ->order(['name'=>'ASC']);
                                    //->all();
        //print_r($carsbybrand); die();
        $this->loadModel('Types');
        $types = $this->Types->find()
                             ->contain(['Cars']);

        $this->loadModel('Cars');
        $carsp = $this->Cars->find()
                            ->contain(['Fuels', 
                                        'Brands',
                                        'Models',
                                        'Types',
                                        'Medias'=>[
                                            'sort'=>['primary_img'=>'DESC']
                                            ],
                                        'Prices'=>[
                                            'sort'=>['price_mad'=>'ASC']
                                            ]
                                       ])
                            ->where(['published'=>'1']);
        $total = sizeof($carsp->toArray());

        $this->paginate = ['limit'=>5];
        $cars = $this->paginate($carsp);

        $this->loadModel('Fuels');
        $fuels = $this->Fuels->find()
                             ->contain(['Cars']);

        $this->loadModel('Types');
        $lang = $this->request->session()->read('lang');
        $cartypes = $this->Types->find()->combine('id', 'name_'.$lang);

        $this->loadModel('Locations');
        $locations = $this->Locations->find();

        $this->set(compact('carsbybrand', 'types','cars','locations','total','fuels','cartypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);

        $this->set('car', $car);
        $this->set('_serialize', ['car']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $car = $this->Cars->newEntity();
        if ($this->request->is('post')) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('car'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $car = $this->Cars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $car = $this->Cars->patchEntity($car, $this->request->data);
            if ($this->Cars->save($car)) {
                $this->Flash->success(__('The car has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The car could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('car'));
        $this->set('_serialize', ['car']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Car id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $car = $this->Cars->get($id);
        if ($this->Cars->delete($car)) {
            $this->Flash->success(__('The car has been deleted.'));
        } else {
            $this->Flash->error(__('The car could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function find(){
        if (empty($this->request->data)) {
            return $this->redirect(['action' => 'index']);
        }else{
        //print_r($this->request->data); die();
        $data = $this->request->data;

        $this->request->session()->write('Cart.searchinfo', $data);

        //print_r($this->request->session()->read('Cart.searchinfo'));
        //die();
       //get all Reservations BETWEEN from_date and to_date
        $this->loadModel('Reservations');
        $subquery1 = $this->Reservations->find()
                                        ->select(['Reservations.id'])
                                        ->contain(['Cars'])
                                        ->where(function ($exp) use ($data) {
                                            return $exp->between(
                                                            'from_date', 
                                                            $data['from_date'], 
                                                            $data['to_date']
                                                        );
                                        })
                                        ->orWhere(function ($exp) use ($data) {
                                            return $exp->between(
                                                    'to_date', 
                                                    $data['from_date'], 
                                                    $data['to_date']);
                                        })->andWhere([
                                            'confirmed' => 1
                                        ]);
        //Get car ids from Carsreservation table that are reserved BETWEEN from_date and to_date
        $this->loadModel('Carsreservations');
        $subquery = $this->Carsreservations->find()
                                           ->select(['Carsreservations.car_id'])
                                           ->where(['Carsreservations.reservation_id IN'=>$subquery1]);

        //Query conditions to select all cars that are not reserved, add car type to condition if exists
        $conditions = array('Cars.id NOT IN' => $subquery);
        if (!empty($data['cartype'])) {
            $conditions['type_id'] = $data['cartype'];
        }
        //executin the query
        $carsp = $this->Cars->find()
                           ->contain(['Fuels', 
                                        'Brands',
                                        'Models',
                                        'Types',
                                        'Medias'=>[
                                            'sort'=>['primary_img'=>'DESC']
                                            ],
                                        'Prices'=>[
                                            'sort'=>['price_mad'=>'ASC']
                                            ]
                                       ])
                           ->where($conditions)
                           ->andWhere(['published'=>'1']);

        //print_r($carsp->toArray()); die();
        $total = sizeof($carsp->toArray());

        $this->paginate = ['limit'=>5];
        $cars = $this->paginate($carsp);

        $this->loadModel('Types');
        $lang = $this->request->session()->read('lang');
        $cartypes = $this->Types->find()->combine('id', 'name_'.$lang);
        //$cartypes = $this->Types->find();

        $this->loadModel('Locations');
        $locations = $this->Locations->find();

        $this->loadModel('Brands');
        $carsbybrand = $this->Brands->find()
                                    ->contain(['Cars'])
                                    ->where(['disabled'=>'0'])
                                    ->order(['name'=>'ASC']);
        $this->loadModel('Types');
        $types = $this->Types->find()
                             ->contain(['Cars']);

        $this->loadModel('Fuels');
        $fuels = $this->Fuels->find()
                             ->contain(['Cars']);
                             
        $this->set(compact('carsbybrand', 'types','cars','locations','total','fuels','cartypes'));
        }
    }

    public function addtocart(){
        if ($this->request->is('post')) {
            $this->request->session()->write('Cart.car_id', $this->request->data['car_id']);
            return $this->redirect(
                ['controller' => 'Extras', 'action' => 'choosextra']
            );
        }else{
            return $this->redirect(
                ['action' => 'index']
            );
        }
    }

    public function review(){
        $islogged=false;
        $this->loadModel('Extras');
        if (empty($this->request->session()->read('Cart.car_id'))) {
            return $this->redirect(
                ['controller' => 'Cars', 'action' => 'index']
            );
        }

        $extras = $this->Extras->find()->contain(['Prices']);
        $this->paginate = ['limit'=>3];
        $extras = $this->paginate($extras);
        $car_id = $this->request->session()->read('Cart.car_id');
        $searchdata = $this->request->session()->read('Cart.searchinfo');
        $ce = $this->request->session()->read('Cart.extras');
        //print_r($cartextras); die();
        $cartextras = array();
        if (!empty($ce)) {
            foreach ($ce as $extra_id) {
                $cartextras[] = $this->Extras->get($extra_id, [
                                    'contain' => ['Prices']
                                ]);
                //print_r($ce);
            }
        }
        //print_r($cartextras);die();
        if (!empty($car_id)) {
            $car = $this->Cars->get($car_id, [
                            'contain' => [
                                'Fuels', 
                                'Brands',
                                'Models',
                                'Medias'=>[
                                        'sort'=>['primary_img'=>'DESC']
                                        ],
                                'Prices'=>[
                                        'sort'=>['price_mad'=>'ASC']
                                        ],
                                'Types'
                            ]
                        ]);
        }

        $from_date = new \DateTime($searchdata['from_date']." ".$searchdata['from_time']);
        $to_date = new \DateTime($searchdata['to_date']." ".$searchdata['from_time']);
        $nbrofdays = ((($to_date->getTimestamp() - $from_date->getTimestamp())/60)/60)/24;

        foreach ($car->prices as $price) {
            if ($nbrofdays <= $price->to_day && $nbrofdays >= $price->from_day) {
                $price_mad = $price->price_mad * $nbrofdays;
                $price_ox =  ($this->request->session()->read('lang') =='fr') ? $price->price_euro*$nbrofdays : $price->price_usd*$nbrofdays;
                break;
            }
        }

        $total_mad = $price_mad;
        $total_ox = $price_ox;
        //print_r($cartextras); die();
        foreach ($cartextras as $extra) {
            if (!empty($extra->price)) {
                $total_mad += $extra->price->price_mad;
                $total_ox +=  ($this->request->session()->read('lang') =='fr') ? $extra->price->price_euro*$nbrofdays : $extra->price->price_usd*$nbrofdays;
            }
        }

        if ($this->Auth->user()) {
            $islogged = true;
        }

        $this->set(compact('extras','car','searchdata','price_mad','price_ox','cartextras','nbrofdays','total_mad','total_ox', 'islogged'));
    }

    public function lang()
    {
        //echo "test"; die();
        if ($this->request->is('post')) {
            $this->request->session()->write('lang', $this->request->data['lang']);
            $this->Cookie->write('lang', $this->request->data['lang']);
            echo "success";
        }
        $this->autoRender = false;
    }
}
