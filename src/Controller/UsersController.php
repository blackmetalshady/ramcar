<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register','verify','logout','login','index','forgotpassword','resetpassword']);
    }

     public function index()
     {
        $this->set('users', $this->Users->find('all'));
    }

    public function view()
    {
        //$this->viewBuilder()->layout(false);
        //$this->render('login');
    }

    public function register()
    {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['registrationtoken'] = bin2hex(openssl_random_pseudo_bytes(16));
            if ($this->Users->exists(['email'=>$data['email']])) {
                $this->Flash->error(__('The email you entred already exists'));
                //return false;
            }else{
                $user = $this->Users->newEntity();
                $user = $this->Users->patchEntity($user,$data);
                if ($usr = $this->Users->save($user)) {
                    if($this->sendverifyemail($usr, $data)){
                        $this->render('registersuccess');
                        /*$this->Flash->success(__('Successfully registred, Please follow the link sent to your email to confirm your registration'));*/
                            //echo "success";die();
                    }else{
                        $this->Flash->error(__('An Error Occured, please try again later'));
                        //echo "emailfail";die();
                    }
                } else {
                    //print_r($user); die();
                    $this->Flash->error(__('An Error Occured, please try again later'));
                }
            }
        }
    }

    public function verify($id, $token){
        $user = $this->Users->find()
                            ->where(['registrationtoken'=>$token])
                            ->first();

        if (empty($user) || $user->id !== $id || $user->confirmed == '1') {
            throw new NotFoundException();
        }else{
            $user = $this->Users->patchEntity($user,['confirmed'=>'1']);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Your account has been successfully activated'));
                return $this->redirect(['controller'=>'Users','action' => 'login']);

            }else{
                print_r($user); die();
            }
        }
        $this->autoRender = false;
    }

    public function edit($id = null)
    {
        $id = $this->request->session()->read('Auth.User')['id'];
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'profile']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function login()
    {
        if (!$this->Auth->user()) {
            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    if ($user['confirmed'] == '0') {
                        $this->render('notconfirmed');
                    }elseif ($user['enabled'] == '0') {
                        $this->render('notenabled');
                    }else{
                        $this->Auth->setUser($user);
                        if ($user['role'] === 'admin') {
                            return $this->redirect([
                                'prefix'=>'admin',
                                'controller' => 'Cars',
                                'action' => 'index']
                            );
                        }else{
                            return $this->redirect($this->referer());
                        }
                    }
                    
                }else{
                    $this->Flash->error(__('Wrong E-mail/Password'));
                    //$msg = __('non autorisé');
                }
                //$this->set(compact('msg'));
            }
        }else{
            return $this->redirect('/');
        }
        
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function profile()
    {
       $id = $this->request->session()->read('Auth.User')['id'];
       $user = $this->Users->get($id); 
       $this->loadModel('Countries');
       $countries = $this->Countries->find('all');
       $this->set(compact('user','countries'));
    }

    public function changepassword(){
        $id = $this->request->session()->read('Auth.User')['id'];
        $user = $this->Users->get($id); 
        $passwordhasher = new DefaultPasswordHasher;
        if (!$passwordhasher->check($this->request->data['password'], $user->password)) {
            echo "oldpassnomatch";
        }else if (strlen($this->request->data['newpassword']) < 6) {
            echo "lengtherror";
        }else if ($this->request->data['newpassword'] != $this->request->data['renewpassword']) {
            echo "newpassnomatch";
        }else{
            $user = $this->Users->patchEntity($user,['password'=> $this->request->data['newpassword']]);
                if ($this->Users->save($user)) {

                    echo "success";
                }else{
                    echo "fail";
                }
        }
        //print_r($this->request->data);
        $this->autoRender=false;
    }

    public function forgotpassword(){
        $status = 0;
        if (!$this->Auth->user()) {
            if ($this->request->is('post')) {
                //Get user by Email
                $user = $this->Users->find()
                                    ->where(['email'=>$this->request->data['email']])
                                    ->first();
                //If the user exists, proceed with token generation
                if (!empty($user)) {
                    //generating the token based on hashed password concatinated with the current timestamp
                    $key = Configure::read('key');
                    $string = $user->password.Time::now()->toUnixString();
                    $token = Security::hash($string,'sha256', true);

                    //all other tokens shall be expired, updating all rows for the user
                    $this->loadModel('Passwordtokens');
                    $update = $this->Passwordtokens->query()->update()
                                         ->set(['expired'=>'1'])
                                         ->where(['user_id'=>$user->id]);
                    //if the update was successfull, proceed
                    if ($update->execute()) {
                        $ptoken = $this->Passwordtokens->newEntity();
                        $ptoken = $this->Passwordtokens->patchEntity($ptoken, [
                                                                        'token'=>$token,
                                                                        'user_id'=>$user->id,
                                                                        'start_time'=>Time::now()
                                                                    ]);
                        //Saving the new token
                        if ($this->Passwordtokens->save($ptoken)) {
                            $params = ['id'=>$user->id,'email'=>$user->email, 'token'=>$token];
                            //Sending the RESET link to the user
                            $this->sendemail($user->email, 1, $params);
                            //Status 1: Everything is ok
                            $status = 1;
                        //if the save of the token went wrong
                        }else{
                            //Status 2 : "An Error Occured, please Try Again Later";
                            $status = 2;
                        }
                    //if updating token as expired went wrong
                    }else{
                        //Status 2 : "An Error Occured, please Try Again Later";
                        $status = 2;
                    }
                //if the user was not found in the database
                }else{
                    //Status 3 : "No user with the email '".$user->email."' has been found in our database";
                    $status = 3;
                }
            }
        }else{
            return $this->redirect('/');
        }
        $this->set(compact('status'));
    }


    public function resetpassword(){
        $status = 0;
        if (!empty($this->request->query['id']) && !empty($this->request->query['email']) && !empty($this->request->query['token'])) {
            $this->loadModel('Passwordtokens');
            $token = $this->Passwordtokens->find()->where([
                                                'user_id'=>$this->request->query['id'],
                                                'token'=>$this->request->query['token']
                                                ])
                                         ->first();
            if (!empty($token) && $token->expired == '0') {
                $dayinseconds = 24*60*60;
                $tokentime = $token->start_time->toUnixString();
                $now = Time::now()->toUnixString();
                $diff = $now - $tokentime;
                if ($diff > $dayinseconds) {
                    $status = 1;
                }
            }else{
                $status = 1;
            }
        }else{
            $status = 1;
        }
        $this->set(compact('status'));
    }

    public function cart(){

        $cart = $this->request->session()->read('Cart');
        //print_r($cart); die();
        if (!empty($cart['car_id'])) {
            //get reservation informations
            //$reservation = $cart;
            $this->loadModel('Cars');
            $car = $this->Cars->get($cart['car_id'],[
                'contain'=>['Medias']]);
            $cart['current_car'] = $car;
            $this->loadModel('Extras');
            $extras = $this->Extras->find()->where(function ($exp, $q) use ($cart) {
                                                 return $exp->in('id', $cart['extras']);
                                             })->toArray();
            $cart['current_extras'] = $extras;
            //print_r($extras); die();
        }
        $this->loadModel('Reservations');
        $reservations = $this->Reservations->find()
                                           ->contain(['Cars'=>['Medias'],'Extras'])
                                           ->where(['user_id'=>$this->request->session()->read('Auth.User.id')])
                                           ->order(['request_time'=>'ASC']);
        //print_r($reservations->toArray()); die();
        $this->set(compact('cart','reservations'));
    }

}
