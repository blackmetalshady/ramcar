<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Mailer\Email;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie',[
            'encryption'=>false
            ]);
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'logoutRedirect' => '/',
            'loginRedirect' => '/',
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        if (empty($this->request->session()->read('lang'))) {
            $lang = ($this->Cookie->read('lang') !== null) ? $this->Cookie->read('lang') : 'en';
            $this->request->session()->write('lang', $lang);
        }
    }

    public function beforeFilter(Event $event)
    {
        
        $languages = array('fr'=>'Français','en'=>'English');
        $this->set(compact('languages'));
    }

    //generating and sending the email to destination
    public function sendemail($useremail, $type, $linkparams){

        //Type 1 : Reset Password Message
        if ($type = 1) {
            $url = Router::url([
                    'controller' => 'Users',
                    'action' => 'resetpassword',
                    '_full'=>true,
                    '?' => $linkparams
                ]);
            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/logo.jpg" style="width: 220px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;">'.__('We have received a password reset request for your account, if you were the source of this action please follow the link bellow ').': </span><br><br><a href="'.$url.'" style="font-size: 18px;">'.$url.'</a><br><br><span style="font-size: 18px; margin-top: 20px;">if you\'re not the source of this request, please make sur to <a href="http://'.$this->request->env('HTTP_HOST').'/users/profile">change your password</a></span>';
        }
        //echo $message; die();
        $email = new Email('default');
        $email->from(['info@ramcarloc.com' => __('Ramcar Service')])
            ->emailFormat('html')
            ->to($useremail)
            ->subject('Ramcar: '.__('Password Reset Confirmation'))->send($message);
    }

    //generating and sending the email to destination
    public function sendverifyemail($user, $data){

            $url = Router::url([
                '_full'=>true,
                '_name' => 'verify' //configured in Routes
            ]);
            $url .= $user->id.'/'.$user->registrationtoken;
            $message = '<center><img src="http://'.$this->request->env('HTTP_HOST').'/img/logo.jpg" style="width: 220px"></center><br><br><span style="font-size: 18px; margin-bottom: 20px;">'.__('To complet your registration please follow or copy/past the link bellow to your browser').' : </span><br><br><a href="'.$url.'" style="font-size: 18px;">'.$url.'</a><br><br><span style="font-size: 18px; margin-top: 20px;">if you\'re not the source of this request, please make sur to <a href="http://'.$this->request->env('HTTP_HOST').'/login">change your password</a></span>';
        //echo $message; die();
        $email = new Email('default');
        $email->from(['info@ramcarloc.com' => __('Ramcar Service')])
            ->emailFormat('html')
            ->to($user->email)
            ->subject('Ramcar: '.__('Please confirm your registration'));
        if ($email->send($message)) {
            return true;
        }else{
            return false;
        }
    }
}