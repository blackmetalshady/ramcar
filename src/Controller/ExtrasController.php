<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Event\Event;

/**
 * Extras Controller
 *
 * @property \App\Model\Table\ExtrasTable $Extras
 */
class ExtrasController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index','choosextra','addtocart','getextra','removefromcart']);
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $extras = $this->Extras->find()->contain(['Prices'])->order(['type'=>'ASC']);
        $this->paginate = ['limit'=>3];
        $extras = $this->paginate($extras);

        $this->loadModel('Cars');
        $cars = $this->Cars->find()
                           ->contain([
                                'Brands',
                                'Models',
                                'Types',
                                'Fuels',
                                'Medias'=>[
                                    'sort'=>[
                                        'primary_img'=>'DESC',
                                        ]
                                    ],
                                'Prices'=>[
                                    'sort'=>[
                                        'price_mad'=>'ASC'
                                    ]
                                ]
                            ])
                           ->where(['published'=>'1'])
                           ->order('RAND()')
                           ->limit(3)
                           ->toArray();
        //print_r($cars->toArray()); die();
        $this->set(compact('extras','cars'));
    }

    /**
     * View method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $extra = $this->Extras->get($id, [
            'contain' => ['Medias']
        ]);

        $this->set('extra', $extra);
        $this->set('_serialize', ['extra']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $extra = $this->Extras->newEntity();
        if ($this->request->is('post')) {
            $extra = $this->Extras->patchEntity($extra, $this->request->data);
            if ($this->Extras->save($extra)) {
                $this->Flash->success(__('The extra has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The extra could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('extra'));
        $this->set('_serialize', ['extra']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $extra = $this->Extras->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $extra = $this->Extras->patchEntity($extra, $this->request->data);
            if ($this->Extras->save($extra)) {
                $this->Flash->success(__('The extra has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The extra could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('extra'));
        $this->set('_serialize', ['extra']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Extra id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $extra = $this->Extras->get($id);
        if ($this->Extras->delete($extra)) {
            $this->Flash->success(__('The extra has been deleted.'));
        } else {
            $this->Flash->error(__('The extra could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function choosextra(){
        if (empty($this->request->session()->read('Cart.car_id'))) {
            return $this->redirect(
                ['controller' => 'Cars', 'action' => 'index']
            );
        }

        $extras = $this->Extras->find()->contain(['Prices']);
        $this->paginate = ['limit'=>3];
        $extras = $this->paginate($extras);
        $car_id = $this->request->session()->read('Cart.car_id');
        $searchdata = $this->request->session()->read('Cart.searchinfo');
        $ce = $this->request->session()->read('Cart.extras');
        //print_r($cartextras); die();
        $cartextras = array();
        if (!empty($ce)) {
            foreach ($ce as $extra_id) {
                $cartextras[] = $this->Extras->get($extra_id, [
                                    'contain' => ['Prices']
                                ]);
                //print_r($ce);
            }
        }
        //print_r($cartextras);die();
        if (!empty($car_id)) {
            $this->loadModel('Cars');
            $car = $this->Cars->get($car_id, [
                            'contain' => [
                                'Fuels', 
                                'Brands',
                                'Models',
                                'Medias'=>[
                                        'sort'=>['primary_img'=>'DESC']
                                        ],
                                'Prices'=>[
                                        'sort'=>['price_mad'=>'ASC']
                                        ],
                                'Types'
                            ]
                        ]);
        }

        $from_date = new \DateTime($searchdata['from_date']." ".$searchdata['from_time']);
        $to_date = new \DateTime($searchdata['to_date']." ".$searchdata['from_time']);
        $nbrofdays = ((($to_date->getTimestamp() - $from_date->getTimestamp())/60)/60)/24;

        foreach ($car->prices as $price) {
            if ($nbrofdays <= $price->to_day && $nbrofdays >= $price->from_day) {
                $price_mad = $price->price_mad * $nbrofdays;
                $price_ox =  ($this->request->session()->read('lang') =='fr') ? $price->price_euro*$nbrofdays : $price->price_usd*$nbrofdays;
                break;
            }
        }

        $total_mad = $price_mad;
        $total_ox = $price_ox;
        //print_r($cartextras); die();
        foreach ($cartextras as $extra) {
            if (!empty($extra->price)) {
                $total_mad += $extra->price->price_mad;
                $total_ox +=  ($this->request->session()->read('lang') =='fr') ? $extra->price->price_euro*$nbrofdays : $extra->price->price_usd*$nbrofdays;
            }
        }

        $this->set(compact('extras','car','searchdata','price_mad','price_ox','cartextras','nbrofdays','total_mad','total_ox'));
    }

    public function addtocart(){
        if ($this->request->is('post')) {
            $extras = $this->request->session()->read('Cart.extras');
            $extra_id = $this->request->data['extra_id'];
            
            //if The Cart.extras session Array is empty, add extra to session
            if (empty($extras)) {
                $extras['id'.$extra_id] = $this->request->data['extra_id'];
                if($this->request->session()->write('Cart.extras', $extras)){
                    echo '1'; // if successfully added to session
                }else{
                    echo '0'; // if something went wrong when adding extra to session
                }
            // The Cart.extras session Array is NOT empty
            }else{
                //Check if the Extra already exists in the array, if not add it
                if (!array_search($extra_id, $extras)) {
                    $extras['id'.$extra_id] = $this->request->data['extra_id'];
                    if($this->request->session()->write('Cart.extras', $extras)){
                        echo '1'; // if successfully added to session
                    }else{
                        echo '0'; // if something went wrong when adding extra to session
                    }
                }else{
                    echo '1'; //if already exists in session
                }
            }
        }else{
            echo '0'; //if request is not Post
        }

        $this->autoRender = false;
    }

    public function removefromcart(){
        if ($this->request->is('post')) {
            $session = $this->request->session();
            $extra_id = $this->request->data['extra_id'];
            //print_r($this->request->session()->read('Cart.extras.id'.$extra_id)); die();
            if (!empty($extra_id)) {
                $session->delete('Cart.extras.id'.$extra_id);
                if (empty($session->read('Cart.extras.id'.$extra_id))) {
                    echo "0";
                }else{
                    echo "1";
                }
            }else{
                echo "1";
            }
        }

        $this->autoRender = false;
    }

    public function getextra(){
        if ($this->request->is('post')) {
            $searchdata = $this->request->session()->read('Cart.searchinfo');
            $id = $this->request->data['extra_id'];
            $extra = $this->Extras->get($id, [
                'contain' => ['Prices']
            ]);
            $data['extraname_fr'] = $extra->name_fr;
            $data['extraname_en'] = $extra->name_en;
            $data['extraid'] = $extra->id;

            if (!empty($extra->price)) {
                $from_date = new \DateTime($searchdata['from_date']." ".$searchdata['from_time']);
                $to_date = new \DateTime($searchdata['to_date']." ".$searchdata['from_time']);
                $nbrofdays = ((($to_date->getTimestamp() - $from_date->getTimestamp())/60)/60)/24;

                //get the price for the appropriate periode
                if ($nbrofdays <= $extra->price->to_day && $nbrofdays >= $extra->price->from_day) {
                    $price_mad = $extra->price->price_mad * $nbrofdays;
                    $price_ox =  ($this->request->session()->read('lang') =='fr') ? $extra->price->price_euro*$nbrofdays : $extra->price->price_usd*$nbrofdays;
                    $data['price_mad'] = $price_mad;
                    $data['price_ox'] = $price_ox;
                }
                
            }else{
                $data['price_mad'] = 0;
                $data['price_ox'] = 0;
            }

            echo json_encode($data);
            $this->RequestHandler->renderAs($this, 'json');
        }else{
            echo "0";
        }

        $this->autoRender = false;
    }
}
