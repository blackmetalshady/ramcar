<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Event\Event;
/**
 * Cars Controller
 *
 * @property \App\Model\Table\CarsTable $Cars
 */
class InfosController extends AppController
{
	public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['terms','contactus','savemessage']);
    }

	public function terms(){}

    public function contactus(){
        $islogged = false;
        if ($this->Auth->user()) {
              $islogged = true;
        }
        $this->set(compact('islogged'));
    }

    public function savemessage(){
    	if (empty($this->request->data['subject']) || empty($this->request->data['message'])) {
    		echo "empty";die();
    	}else{
    		if ($this->Auth->user()) {
    			$this->request->data['from_user'] = $this->request->session()->read('Auth.User.id');
	    	}else{
	    		if (empty($this->request->data['full_name']) || empty($this->request->data['email'])){
    				echo "empty";die();
    			}
	    	}
    	}
    	$this->loadModel('Messages');
		$message = $this->Messages->newEntity();
		$message = $this->Messages->patchEntity($message, $this->request->data);

		if ($this->Messages->save($message)) {
			echo "success";die();
		}else{
            print_r($message); die();
			echo "fail";die();
		}
    	
    	$this->autoRender = false;
    }
}

