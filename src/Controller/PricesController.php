<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Prices Controller
 *
 * @property \App\Model\Table\PricesTable $Prices
 */
class PricesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $this->loadModel('Cars');
        $carsq = $this->Cars->find()
                           ->contain([
                                'Fuels',
                                'Brands',
                                'Models',
                                'Types',
                                'Prices'=>[
                                    'sort'=>['price_mad'=>'ASC']
                                ],
                                'Medias'=>[
                                    'sort'=>['primary_img'=>'DESC']
                                ]
                            ])
                           ->order(['fuel_id'=>'ASC']);
        $this->paginate = ['limit'=>10];
        $cars = $this->paginate($carsq);
        $this->loadModel('Brands');
        $carsbybrand = $this->Brands->find()
                                    ->contain(['Cars'])
                                    ->where(['disabled'=>'0'])
                                    ->order(['name'=>'ASC']);
                                    //->all();
        $this->loadModel('Fuels');
        $fuels = $this->Fuels->find()
                             ->contain(['Cars']);

        //print_r($carsbybrand); die();
        $this->loadModel('Types');
        $types = $this->Types->find()
                             ->contain(['Cars']);

        $this->set(compact('cars', 'types', 'carsbybrand','fuels'));
    }

    /**
     * View method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => ['Cars']
        ]);

        $this->set('price', $price);
        $this->set('_serialize', ['price']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $price = $this->Prices->newEntity();
        if ($this->request->is('post')) {
            $price = $this->Prices->patchEntity($price, $this->request->data);
            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The price could not be saved. Please, try again.'));
            }
        }
        $cars = $this->Prices->Cars->find('list', ['limit' => 200]);
        $this->set(compact('price', 'cars'));
        $this->set('_serialize', ['price']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $price = $this->Prices->patchEntity($price, $this->request->data);
            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The price could not be saved. Please, try again.'));
            }
        }
        $cars = $this->Prices->Cars->find('list', ['limit' => 200]);
        $this->set(compact('price', 'cars'));
        $this->set('_serialize', ['price']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Price id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $price = $this->Prices->get($id);
        if ($this->Prices->delete($price)) {
            $this->Flash->success(__('The price has been deleted.'));
        } else {
            $this->Flash->error(__('The price could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
