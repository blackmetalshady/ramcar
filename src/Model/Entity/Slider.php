<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slider Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $slider_type
 * @property string $title_en
 * @property string $title_fr
 * @property string $description_fr
 * @property string $description_en
 * @property int $disabled
 * @property \App\Model\Entity\Slidercar[] $slidercars
 * @property \App\Model\Entity\Slidermedia[] $slidermedias
 * @property \App\Model\Entity\Car[] $cars
 * @property \App\Model\Entity\Media[] $medias
 */
class Slider extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
