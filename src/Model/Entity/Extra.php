<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Extra Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $description_fr
 * @property string $description_en
 * @property string $details_fr
 * @property string $details_en
 * @property \App\Model\Entity\Media[] $medias
 */
class Extra extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
