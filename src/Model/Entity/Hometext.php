<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HomeText Entity
 *
 * @property string $id
 * @property string $title_en
 * @property string $title_fr
 * @property string $content_en
 * @property string $content_fr
 * @property int $position
 */
class Hometext extends Entity
{

}
