<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Car Entity.
 *
 * @property int $id
 * @property int $type_id
 * @property \App\Model\Entity\Type $type
 * @property string $registration
 * @property int $passengers
 * @property int $auto
 * @property int $airconditioning
 * @property string $color
 * @property string $details_fr
 * @property string $details_en
 * @property int $brand_id
 * @property \App\Model\Entity\Brand $brand
 * @property int $model_id
 * @property \App\Model\Entity\Model $model
 * @property int $horses
 * @property int $fuel_id
 * @property \App\Model\Entity\Media[] $medias
 */
class Car extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
