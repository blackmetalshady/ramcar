<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property string $id
 * @property string $full_name
 * @property string $email
 * @property string $subject
 * @property string $message
 * @property \Cake\I18n\Time $sent_at
 * @property string $from_user
 * @property string $to_user
 * @property int $is_read
 * @property string $parent_id
 *
 * @property \App\Model\Entity\Message $parent_message
 * @property \App\Model\Entity\Message[] $child_messages
 */
class Message extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
