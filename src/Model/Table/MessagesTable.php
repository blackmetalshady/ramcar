<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Messages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentMessages
 * @property \Cake\ORM\Association\HasMany $ChildMessages
 *
 * @method \App\Model\Entity\Message get($primaryKey, $options = [])
 * @method \App\Model\Entity\Message newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Message[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Message|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Message patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Message[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Message findOrCreate($search, callable $callback = null)
 */
class MessagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('messages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FromUser', [
            'className' => 'Users',
            'propertyName'=>'fromUser',
            'foreignKey' => 'from_user'
        ]);
        $this->belongsTo('ToUser', [
            'className' => 'Users',
            'propertyName'=>'toUser',
            'foreignKey' => 'to_user'
        ]);
        $this->belongsTo('ParentMessages', [
            'className' => 'Messages',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildMessages', [
            'className' => 'Messages',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('full_name');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->dateTime('sent_at')
            ->allowEmpty('sent_at');

        $validator
            ->uuid('from_user')
            ->allowEmpty('from_user');

        $validator
            ->uuid('to_user')
            ->allowEmpty('to_user');

        $validator
            ->integer('is_read')
            ->allowEmpty('is_read');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['parent_id'], 'ParentMessages'));

        return $rules;
    }
}
