<?php
namespace App\Model\Table;

use App\Model\Entity\Fuel;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fuels Model
 *
 * @property \Cake\ORM\Association\HasMany $Cars
 */
class FuelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fuels');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Cars', [
            'foreignKey' => 'fuel_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name_fr', 'create')
            ->notEmpty('name_fr');

        $validator
            ->requirePresence('name_en', 'create')
            ->notEmpty('name_en');

        return $validator;
    }
}
