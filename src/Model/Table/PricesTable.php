<?php
namespace App\Model\Table;

use App\Model\Entity\Price;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Prices Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cars
 */
class PricesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('prices');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cars', [
            'foreignKey' => 'car_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Extras', [
            'foreignKey' => 'extra_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('from_day')
            ->requirePresence('from_day', 'create')
            ->notEmpty('from_day');

        $validator
            ->integer('to_day')
            ->requirePresence('to_day', 'create')
            ->notEmpty('to_day');

        $validator
            ->numeric('price_mad')
            ->requirePresence('price_mad', 'create')
            ->notEmpty('price_mad');

        $validator
            ->numeric('price_euro')
            ->requirePresence('price_euro', 'create')
            ->notEmpty('price_euro');

        $validator
            ->numeric('price_usd')
            ->requirePresence('price_usd', 'create')
            ->notEmpty('price_usd');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['car_id'], 'Cars'));
        $rules->add($rules->existsIn(['extra_id'], 'Extras'));
        return $rules;
    }
}
