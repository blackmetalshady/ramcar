<?php
namespace App\Model\Table;

use App\Model\Entity\Extrasreservation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Extrasreservations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Extras
 * @property \Cake\ORM\Association\BelongsTo $Reservations
 */
class ExtrasreservationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('extrasreservations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Extras', [
            'foreignKey' => 'extra_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Reservations', [
            'foreignKey' => 'reservation_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Reservations', [
            'joinTable' => 'extrasreservations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['extra_id'], 'Extras'));
        $rules->add($rules->existsIn(['reservation_id'], 'Reservations'));
        return $rules;
    }
}
