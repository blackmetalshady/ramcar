<?php
namespace App\Model\Table;

use App\Model\Entity\Reservation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reservations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Reservationarticles
 */
class ReservationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reservations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'strategy' => 'select'
        ]);

        $this->belongsTo('Guests', [
            'foreignKey' => 'guest_id',
            'strategy' => 'select'
        ]);

        $this->belongsToMany('Cars', [
            'joinTable' => 'carsreservations'
        ]);
        $this->belongsToMany('Extras', [
            'joinTable' => 'extrasreservations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->date('from_date')
            ->requirePresence('from_date', 'create')
            ->notEmpty('from_date');

        $validator
            ->time('from_time')
            ->requirePresence('from_time', 'create')
            ->notEmpty('from_time');

        $validator
            ->date('to_date')
            ->requirePresence('to_date', 'create')
            ->notEmpty('to_date');

        $validator
            ->time('to_time')
            ->requirePresence('to_time', 'create')
            ->notEmpty('to_time');

        $validator
            ->requirePresence('pickup_location', 'create')
            ->notEmpty('pickup_location');

        $validator
            ->requirePresence('return_location', 'create')
            ->notEmpty('return_location');

        $validator
            ->requirePresence('code', 'create')
            ->allowEmpty('code');

        $validator
            ->integer('confirmed')
            ->allowEmpty('confirmed');

        $validator
            ->integer('rejected')
            ->allowEmpty('rejected');
        
        $validator
            ->integer('canceled')
            ->allowEmpty('canceled');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['guest_id'], 'Guests'));
        return $rules;
    }
}
