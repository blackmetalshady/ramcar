<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomeText Model
 *
 * @method \App\Model\Entity\HomeText get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomeText newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomeText[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomeText|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeText patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomeText[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomeText findOrCreate($search, callable $callback = null)
 */
class HometextsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('home_text');
        $this->displayField('title');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->requirePresence('title_en', 'create')
            ->notEmpty('title_en');

        $validator
            ->requirePresence('title_fr', 'create')
            ->notEmpty('title_fr');

        $validator
            ->requirePresence('content_en', 'create')
            ->notEmpty('content_en');

        $validator
            ->requirePresence('content_fr', 'create')
            ->notEmpty('content_fr');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        return $validator;
    }
}
