<?php
namespace App\Model\Table;

use App\Model\Entity\Extra;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Extras Model
 *
 * @property \Cake\ORM\Association\HasMany $Medias
 */
class ExtrasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('extras');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasOne('Prices', [
            'foreignKey' => 'extra_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name_en', 'create')
            ->notEmpty('name_en');

        $validator
            ->requirePresence('name_fr', 'create')
            ->notEmpty('name_fr');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->allowEmpty('description_fr');

        $validator
            ->allowEmpty('description_en');

        $validator
            ->allowEmpty('details_fr');

        $validator
            ->allowEmpty('details_en');

        $validator
            ->requirePresence('img', 'create')
            ->notEmpty('img');
            
        return $validator;
    }
}
