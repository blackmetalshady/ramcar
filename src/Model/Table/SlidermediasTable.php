<?php
namespace App\Model\Table;

use App\Model\Entity\Slidermedia;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Slidermedias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Sliders
 * @property \Cake\ORM\Association\BelongsTo $Medias
 */
class SlidermediasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('slidermedias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Sliders', [
            'foreignKey' => 'slider_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Medias', [
            'foreignKey' => 'media_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->integer('front_img')
            ->requirePresence('front_img', 'create')
            ->notEmpty('front_img');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['slider_id'], 'Sliders'));
        $rules->add($rules->existsIn(['media_id'], 'Medias'));
        return $rules;
    }
}
