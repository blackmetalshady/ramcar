<?php
namespace App\Model\Table;

use App\Model\Entity\Slider;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sliders Model
 *
 * @property \Cake\ORM\Association\HasMany $Slidercars
 * @property \Cake\ORM\Association\HasMany $Slidermedias
 */
class SlidersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sliders');
        $this->displayField('name');
        $this->primaryKey('id');

         $this->hasMany('Slidercars', [
            'foreignKey' => 'slider_id'
        ]);

        $this->hasMany('Slidermedias', [
            'foreignKey' => 'slider_id'
        ]);

        $this->belongsToMany('Cars', [
            'foreignKey' => 'slider_id',
            'targetForeignKey' => 'car_id',
            'joinTable' => 'slidercars'
        ]);

        $this->belongsToMany('Medias', [
            'foreignKey' => 'slider_id',
            'targetForeignKey' => 'media_id',
            'joinTable' => 'slidermedias'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('slider_type', 'create')
            ->notEmpty('slider_type');

        $validator
            ->requirePresence('title_en', 'create')
            ->notEmpty('title_en');

        $validator
            ->requirePresence('title_fr', 'create')
            ->notEmpty('title_fr');

        $validator
            ->requirePresence('description_fr', 'create')
            ->notEmpty('description_fr');

        $validator
            ->requirePresence('description_en', 'create')
            ->notEmpty('description_en');

        $validator
            ->integer('disabled')
            ->requirePresence('disabled', 'create')
            ->notEmpty('disabled');

        return $validator;
    }
}
