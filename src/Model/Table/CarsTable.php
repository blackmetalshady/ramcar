<?php
namespace App\Model\Table;

use App\Model\Entity\Car;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cars Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Types
 * @property \Cake\ORM\Association\BelongsTo $Brands
 * @property \Cake\ORM\Association\BelongsTo $Models
 * @property \Cake\ORM\Association\BelongsTo $Fuels
 * @property \Cake\ORM\Association\HasMany $Medias
 */
class CarsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cars');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Types', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Brands', [
            'foreignKey' => 'brand_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Models', [
            'foreignKey' => 'model_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Fuels', [
            'foreignKey' => 'fuel_id'
        ]);
        $this->hasMany('Medias', [
            'foreignKey' => 'car_id'
        ]);
        $this->hasMany('Prices', [
            'foreignKey' => 'car_id'
        ]);
        $this->belongsToMany('Reservations', [
            'joinTable' => 'carsreservations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('registration', 'create')
            ->notEmpty('registration');

        $validator
            ->integer('passengers')
            ->requirePresence('passengers', 'create')
            ->notEmpty('passengers');

        $validator
            ->integer('auto')
            ->requirePresence('auto', 'create')
            ->notEmpty('auto');

        $validator
            ->integer('airconditioning')
            ->requirePresence('airconditioning', 'create')
            ->notEmpty('airconditioning');

        $validator
            ->allowEmpty('color');

        $validator
            ->allowEmpty('details_fr');

        $validator
            ->allowEmpty('details_en');

        $validator
            ->integer('horses')
            ->allowEmpty('horses');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_id'], 'Types'));
        $rules->add($rules->existsIn(['brand_id'], 'Brands'));
        $rules->add($rules->existsIn(['model_id'], 'Models'));
        $rules->add($rules->existsIn(['fuel_id'], 'Fuels'));
        return $rules;
    }
}
