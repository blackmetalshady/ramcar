<?php
namespace App\Model\Table;

use App\Model\Entity\Carsreservation;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Carsreservations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cars
 * @property \Cake\ORM\Association\BelongsTo $Reservations
 */
class CarsreservationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('carsreservations');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cars', [
            'foreignKey' => 'car_id'
        ]);
        $this->belongsTo('Reservations', [
            'foreignKey' => 'reservation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['car_id'], 'Cars'));
        $rules->add($rules->existsIn(['reservation_id'], 'Reservations'));
        return $rules;
    }
}
