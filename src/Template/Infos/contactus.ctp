<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;">
    	<div class="pg-title">
		    <h2 style="display: inline-block;">
		    	<?= __('Contact'); ?>
		    </h2>
		    <span id="sendmsg"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;<?= __('Send us a Message'); ?></span>
		    <center><div class="separator"></div></center>
		</div>
	    <div class="widget-content-range">
	    	<ul>
	    		<li>
		    		<table>
		    			<tr>
		    				<td class="ico"><i class="fa fa-location-arrow info" aria-hidden="true"></i></td>
		    				<td class="tdcntnt">N°2, Imm. 4, Complex Habssi, Bord Hadji Av Allal El Fassi Marrakech</td>
		    			</tr>
		    		</table>
	    		</li>
	    		<li>
	    			<table>
		    			<tr>
		    				<td class="ico"><i class="fa fa-phone info" aria-hidden="true"></i></td>
		    				<td class="tdcntnt">+212-524-303-100</td>
		    			</tr>
		    		</table>
	    		</li>
	    		<li>
	    			<table>
		    			<tr>
		    				<td class="ico"><i class="fa fa-fax info" aria-hidden="true"></i></td>
		    				<td class="tdcntnt">+212-524-303-100</td>
		    			</tr>
		    		</table>
	    		</li>
	    		<li>
	    			<table>
		    			<tr>
		    				<td class="ico"><i class="fa fa-mobile info" aria-hidden="true"></i></td>
		    				<td class="tdcntnt">+212-662-184-232</td>
		    			</tr>
		    		</table>
	    		</li>
	    	</ul>
	    </div>
	    <div class="widget-content-range">   
		    <center>
		    	<div id="map" style="height:500px;width:90%;border: 1px solid black;"></div>
		    </center>
		    <script>
		      var map;
		      function initMap() {
		      	  var myLatLng = {lat: 31.652736, lng: -8.001494};
				  var map = new google.maps.Map(document.getElementById('map'), {
				    zoom: 18,
				    center: myLatLng,
				    mapTypeId: 'terrain',
				    disableDefaultUI: true
				  });

				  var marker = new google.maps.Marker({
				    position: myLatLng,
				    map: map,
				    title: 'RamCar'
				  });
		      }
		    </script>
		    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFe87BZNJlsytOLpQBpuYS-xxCfKxJOHY&callback=initMap" async defer></script>
	    </div>
	    <br>
	    <h4><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;&nbsp;<?= __('Send a Message'); ?></h4>
	    <br>
	    <div class="widget-content-range" id="msgform">   
	    <?= $this->Form->create(null, ['url'=>['action'=>'savemessage'], 'id'=>'sendmsgform']) ?>
	    	<table>
		    	<?php if (!$islogged) : ?>
		    		<tr>
		    			<td>
		    				<label for="fullname"><?= __('Full Name'); ?></label>
		    			</td>
		    			<td>
		    				<input class="shortcode_input" type="text" value="" placeholder="<?= __('Enter your Full name') ?> ..." name="fullname" id="fullname" required />
		    				<span style="color: red">*</span>
		    			</td>
		    		</tr>
		    		<tr>
		    			<td>
		    				<label for="email"><?= __('E-mail'); ?></label>
		    			</td>
		    			<td>
		    				<input class="shortcode_input" type="email" value="" placeholder="<?= __('Enter your E-mail') ?> ..." name="email" id="email" required />
		    				<span style="color: red">*</span>
		    			</td>
		    		</tr>
		    	<?php endif; ?>
	    		<tr>
	    			<td>
	    				<label for="subject"><?= __('Subject'); ?></label>
	    			</td>
	    			<td>
	    				<input class="shortcode_input" type="text" value="" placeholder="<?= __('What would you like to tell us'); ?>" name="subject" id="subject" style="width: 550px;" required />
	    				<span style="color: red">*</span>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td class="inlabel">
	    				<label for="txtarea"><?= __('Message'); ?></label>
	    			</td>
	    			<td>
	    				<textarea class="shortcode_input" type="text" id="message" name="message" style="width: 550px; height: 150px;" required></textarea>
	    				<span style="color: red;vertical-align: top;">*</span>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td></td>
	    			<td>
	    				<center>
	    					<input class="shortcode_button orange_button" name="orange_button" type="submit" value="<?= __('Send Message'); ?>">
	    				</center>
	    			</td>
	    		</tr>
	    	</table>
	    	<?= $this->Form->end(); ?>
	    </div>
    </div>
</div>
<style type="text/css">
h4{
	font-size: 15px !important;
	background: #434b56 !important;
    color: #fff !important;
    padding: 12px 20px !important;
}

.widget-content-range {
    color: #000 !important;
    text-align: justify !important;
    font-size: 15px !important;
    word-spacing: 2px !important; 
    text-indent: 2em;
}

.widget li {
    padding: 10px 15px 2px !important;
}

</style>
<script type="text/javascript">
	$('#sendmsgform').on('submit', function(e){
		e.preventDefault();
		$('input, textarea').removeClass('shortcode_input_required');
			full_name =  $('#fullname');
			email =  $('#email');
			subject =  $('#subject');
			message =  $('#message');

		if (!subject.val()) {
			subject.addClass('shortcode_input_required');
		}else if(!message.val()){
			message.addClass('shortcode_input_required');
		}else{
			$('input, textarea').removeClass('shortcode_input_required');
			$.post('/infos/savemessage', {full_name: full_name.val(), email: email.val(), subject: subject.val(), message: message.val()})
		    	.done(function(data){
		    		if (data === 'empty') {
		    			$.each($('input, textarea'), function(){
		    				if (!$(this).val()) {
		    					$(this).addClass('shortcode_input_required');
		    				}
		    			});
		    		}else if(data === 'success'){
		    			alert("<?= __('Your message has been succesfully sent')?>");
		    			$('input, textarea').prop('disabled', true);
		    			$('input, textarea').css('background-color','#e6e6e6')
		    		}else{
		    			alert("<?= __('Something went wrong, please try again later')?>");
		    		}
		    });
		}
	});
</script>