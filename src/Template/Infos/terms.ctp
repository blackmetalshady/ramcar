<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<?php //print_r($reservation); die(); ?>
<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;" id="toprint">
    	<div class="pg-title">
		    <h2>
		    	<?= __('Our Terms &amp; Conditions'); ?>
		    </h2>
		    <div class="separator"></div>
		</div>
	    <div class="widget-content-range">
	    	<p>
	    		RamCar is a tour operator and travel wholesaler with over 6 years of experience in car rentals. We specialize in leisure and business clients traveling to Morocco, and due to our volume sales with major nationwide car rental companies we are able to offer deeply discounted and promotional rentals.
	    	</p>
	    </div>
	    <h4>1. Additional costs for renting a car</h4>
	    <div class="widget-content-range">   
	    	<p>                           
	        	There are no mandatory charges in addition to the amount shown on your confirmation voucher, however, upon vehicle pickup, the rental agent may offer additional services and insurance coverages, as well as vehicle category upgrades. Such services are optional and shall be refused by the customer if not interested. The non acceptance of such optional services does not impede the customer from renting the vehicle. Carefully read the rental contract before signing to make sure that only requested services have been included, because once any optional services have been accepted the total rental charges will be altered and no amounts will be refunded.
	        </p>
	    </div>
	    <h4>2. Rental Requirements</h4>
	    <div class="widget-content-range">   
	    	<p>                           
	    		In order to rent a car in the Morocco, and qualify for these promotional rates, the renter must be at least 18 years of age, have a valid driver’s license from the country of residence.
	    	</p>
	    </div>
	    <h4>3. Payment</h4>
	    <div class="widget-content-range">
	    	<p>                              
	    		All car rental reservations confirmed by RamCar are to be paid directly to the rental car company at the time the vehicle is collected (picked up). Payment is accepted via major credit cards (Visa, Master Card &amp; Amex) in the renters name.
	    	</p>
	    </div>
	    <div class="print-block">
	    	<a href="/files/terms_<?= $lang; ?>.pdf" download="terms_conditions.pdf">
	    		<i class="fa fa-download info" aria-hidden="true"></i>
	    		<span class="print">Download</span>
	    	</a>
	    	<a href="/files/terms_<?= $lang; ?>.pdf" target="_blank">
	    		<i class="fa fa-print info" aria-hidden="true"></i>
	    		<span class="print">Print</span>
	    	</a>
	    </div>
    </div>
</div>
<style type="text/css">
h4{
	font-size: 15px !important;
	background: #434b56 !important;
    color: #fff !important;
    padding: 12px 20px !important;
}

.widget-content-range {
    color: #000 !important;
    text-align: justify !important;
    font-size: 15px !important;
    word-spacing: 2px !important; 
    margin-bottom: 30px;
    text-indent: 2em;
}
</style>