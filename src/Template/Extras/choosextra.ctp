<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>

<div id="progress-bar"> 
    <div id="progress-bar-steps"> 
        <div class="progress-bar-step done"> 
            <div class="step_number">1</div> 
            <div class="step_name"><?= __('Create request') ?></div> 
            <div class="clear"></div> 
        </div> 
        <div class="progress-bar-step done"> 
            <div class="step_number">2</div> 
            <div class="step_name"><?= __('Choose a car') ?></div> 
            <div class="clear"></div> 
        </div> 
        <div class="progress-bar-step current"> 
            <div class="step_number">3</div> 
            <div class="step_name"><?= __('Choose extras') ?></div> 
            <div class="clear"></div> 
        </div> 
            <div class="progress-bar-step last"> 
            <div class="step_number">4</div> 
            <div class="step_name"><?= __('Review &amp; Book') ?></div> 
            <div class="clear"></div> 
        </div> 
    </div> 
    <div class="clear"></div> 
</div>
<div id="main">
    <div id="primary" style="background: none;">                  
    <aside class="sidebar-left"> 
        <div class="widget">
            <h3 class="widget-title">
                <?= $this->Html->image('icons/orderinfo.png'); ?>
                <?= __('Order Info') ?>
            </h3>
            <h4>
                <?= __('Car') ?>
                <?= $this->Html->link(__('Edit'),['controller'=>'Cars','action'=>'find','#'=>'slider-form']) ?>
            </h4>
            <div class="widget-content main-block product-widget-mini">                             
                <div class="product-img" style='width: 130px'>
                    <?= $this->Html->image('cars/'.$car->medias[0]->name,['style'=>'width: 130px']); ?>
                </div>
                <div class="product-info">
                    <div class="entry-format">
                        <div><?= ucfirst($car->brand->name)." ".ucfirst($car->model->name)?></div>
                        <span><?= ucfirst($car->type->{'name_'.$lang}) ?></span>
                    </div>
                    <div class="features">
                        <p><?= $this->Html->image('icons/passengers.png')?>
                        <?= $car->passengers; ?> <?= __('passengers') ?></p>
                        <?php if ($car->auto == 1): ?>
                            <p>
                                <?= $this->Html->image('icons/autotransmission.png')?> 
                                <?= __('automatic transmission') ?>
                            </p>
                        <?php endif ?>
                        <?php if ($car->airconditioning): ?>
                            <p>
                                <?= $this->Html->image('icons/airconditioning.png')?> 
                                <?= __('air conditioning') ?>
                            </p>
                            
                        <?php endif ?>
                        <p>
                            <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                        </p>
                    </div>
                    <?php if (!empty($car->{'details_'.$lang})): ?>
                        <div class="details">
                            <div class="view-details">[+] <?= __('View car details'); ?></div>
                            <div class="close-details">[-] <?= __('Close car details'); ?></div>
                            <span class="details-more">
                                <?= $car->{'details_'.$lang} ?>
                            </span>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <h4>
                <?= __('Date &amp; Location') ?>
                
                <?= $this->Html->link(__('Edit'),['controller'=>'Cars','action'=>'find','#'=>'slider-form']) ?>
            </h4>
            <div class="widget-content widget-info">                                    
                <h4>Pick Up time</h4>
                <p><?= $searchdata['from_date']." ".__('at')." ".$searchdata['from_time']; ?></p>
                <h4>Return time</h4>
                <p><?= $searchdata['to_date']." ".__('at')." ".$searchdata['to_time']; ?></p>
                <h4>Pickup Location</h4>
                <p><?= $searchdata['pickup_location']; ?></p>
                <h4>Return Location</h4>
                <p><?= $searchdata['return_location']; ?></p>
                <div class="subtotal_content">
                    <div class="subtotal" style="min-height: 35px;">              
                        <?= __('Subtotal') ?>: <span class="price" data-priceox="<?= $price_ox ?>"><?= ($lang=='fr') ? $price_ox.' €' : '$ '.$price_ox; ?></span>
                        <br>
                        <span class="price" data-pricemad="<?= $price_mad ?>" style="font-size: 12px;">(<?= $price_mad.' MAD' ?>)</span>
                    </div>
                </div>
            </div>
            <h4 class="extras"><?= __('Extras') ?></h4>
            <div class="widget-content widget-extras" id="extraslist">
                <?php if (!empty($cartextras)): ?>
                      <?php foreach ($cartextras as $ex) :?>  
                            <p id="pex<?= $ex->id ?>">
                                <?= $ex->{'name_'.$lang}?>
                                <span class="price">
                                    <?php if (empty($ex->price)): ?>
                                        FREE
                                    <?php else: ?>
                                       <?= ($lang == 'fr') ? $ex->price->price_euro*$nbrofdays.' €' : '$'.$ex->price->price_usd*$nbrofdays ; ?> 
                                    <?php endif ?>
                                <span class="removextra" data-extra="<?= $ex->id ?>">
                                    <i class='fa fa-remove'></i>
                                </span>
                                </span>
                            </p>
                      <?php endforeach; ?>                        
                <?php endif ?>                              
            </div>
            <div class="widget-footer widget-footer-total">
                <?= __('Total') ?>: <span class="price" id="total_ox" data-totalox="<?= $total_ox ?>"><?= ($lang == 'fr') ? $total_ox.' €': '$'.$total_ox ; ?> </span><br>
                <h5 data-totalmad="<?= $total_mad?>">(<?= $total_mad ?> MAD)</h5>
            </div>
        </div>                                                          
    </aside>  
    <div id="content" class="sidebar-middle">
        <div class="widget main-widget product-widget">
            <div class="content-overlay">
                <?= $this->Html->image('ajax-loader.gif',['class'=>'ajax-loader']); ?>
                
            </div>
            <div class="form">
                <div class="widget-title">
                    <div>
                        <?= $this->Html->image('icons/results.png') ?>
                        <?= __('Results') ?> <span><?= $this->Paginator->counter()." ".__('Pages'); ?></span>
                    </div>
                    <div class="widget-title-sort"><span class="viev-all">Sort by: </span> <a href="#" title="" class="current"><?= __('Price') ?></a> | <a href="#" title=""><?= __('Type') ?></a></div>
                    <div class="clear"></div>   
                </div>
                <?php foreach ($extras as $extra) :?>
                <div class="post">
                    <div class="main-block_container">
                        <div class="main-block">                                    
                            <div class="product-img">
                                <?= $this->Html->image("extras/".$extra->img);?>
                            </div>
                            <div class="product-info">
                                <h3 class="entry-format"><?= $extra->{'name_'.$lang}; ?></h3>
                                <div class="features">
                                    <?= $extra->{'description_'.$lang}; ?>
                                </div>
                                <?php if (!empty($extra->{'details_'.$lang})) : ?>
                                <div class="details">
                                    <div class="view-details">[+] View car details</div>
                                    <div class="close-details">[-] Close car details</div>
                                    <span class="details-more">
                                        <?= $extra->{'details_'.$lang}; ?>
                                    </span>
                                </div>
                                <?php endif; ?>
                            </div>                              
                        </div>
                        <div class="additional-block">
                            <br>
                            <?php if (empty($extra->price)) :?>
                                <p><?= __('FREE') ?></p>
                            <?php else: ?>
                                <?php $currency = ($lang=='fr') ? $extra->price->price_euro.' €' : '$'.$extra->price->price_usd ?>
                                <p><?= $extra->price->price_mad." MAD";?></p>
                                <p>(<?= $currency ?>)</p>
                            <?php endif; ?>
                            <br>
                            <?php if (!empty($this->request->session()->read('Cart.car_id'))) :?>
                                <div class="addtocartdiv" id="extra<?= $extra->id; ?>">
                                <?php if (!empty($this->request->session()->read('Cart.extras')) && in_array($extra->id, $this->request->session()->read('Cart.extras'))) : ?>
                                    <?= $this->Html->image("success.png")?>
                                <?php else: ?>
                                    <input class="continue_button blue_button addtocart" type="button" value="<?= __('Add to Cart')?>" data-extra="<?= $extra->id; ?>" />
                                <?php endif; ?>
                                
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="pagination-block"> 
            <div class="pagination"> 
                <ul class="pagination">
                    <?= $this->Paginator->prev('') ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next('') ?>
                </ul>
                <p class="clear"></p> 
            </div> 
        </div>
        <br><br>
        <div class="next_page">
            <?= $this->Form->create(null,['url'=>['controller'=>'Cars','action'=>'review']]);?>
            <input class="continue_button blue_button" type="submit" value="Review &amp; Book" />
            <?= $this->Form->end();?>
        </div>
    </div>              
    <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click','.addtocart', function(){
        var parent = $(this).parent();
        var extra_id = $(this).data('extra');
        parent.html('<?= $this->Html->image("ajax-loader.gif",["style"=>"width: 50px"])?>');
        $.post('/extras/addtocart', {extra_id: extra_id})
            .done(function(data){
                if (data == '0') {
                    $.post('/extras/getextra', {extra_id: extra_id})
                        .done(function(data){
                            if (data.price_mad == '0') {
                                price_mad = "<?= __('FREE') ?>";
                                price_ox = "<?= __('FREE') ?>";
                            }else{
                                price_mad = data.price_mad+' MAD';
                                if ('<?= $lang ?>' == 'fr') {
                                    price_ox = data.price_ox+' €';
                                }else{
                                    price_ox = '$ '+data.price_ox;
                                }
                            }
                            $('#extraslist').append('<p id="pex'+extra_id+'">'+data.extraname_<?= $lang ?>+' <span class="price">'+price_ox+'<span class="removextra" data-extra="'+data.extraid+'">'+
                                    '<i class="fa fa-remove"></i></span>'+
                                '</span></p>')
                        });
                    parent.html('<?= $this->Html->image("success.png")?>');
                }else{
                    parent.html(this);
                    alert("<?= __('An Error Occured, Please Try Later')?>")
                }
            });
    });

$(document).on('click','.removextra', function(){
    if (confirm("<?= __('Are you sur you want to remove this Extra from Cart?')?>")) {
        var extra_id = $(this).data("extra");
        $.post('/extras/removefromcart', {extra_id: extra_id})
            .done(function(data){
                if (data == '0') {
                    $('#extra'+extra_id).html('<input class="continue_button blue_button addtocart" type="button" value="<?= __('Add to Cart')?>" data-extra="'+extra_id+'" />');
                    $('p#pex'+extra_id).remove();
                }else{
                    alert("<?= __('Could Not Remove the Extra From your Cart, please Try Again') ?>")
                }
            })
    }
});
</script>