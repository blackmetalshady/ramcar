<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<br>
<div id="main">
    <div id="primary" style="background: none;">                  
    <aside class="sidebar-left"> 
        <div class="widget">

            <h3 class="widget-title">
                <?= $this->Html->image('icons/orderinfo.png'); ?>
                <?= __('Top Rented Cars') ?>
            </h3>
            <?php foreach ($cars as $car) : ?>
            <h4>
            <?php 
                $price = ($lang == 'fr') ?  $car->prices[0]->price_euro.'€': '$'.$car->prices[0]->price_usd ;
            ?>
            <?= __('Rent this car for only').'<span class="car-price">'.$price.'</span>('.$car->prices[0]->price_mad.' MAD )' ?>
            </h4>
            <div class="widget-content main-block product-widget-mini">                             
                <div class="product-img custom-img" style='width: 130px'>
                    <?= $this->Html->image('cars/'.$car->medias[0]->name,['style'=>'width: 130px']); ?>
                </div>
                <div class="product-info">
                    <div class="entry-format">
                        <div><?= ucfirst($car->brand->name)." ".ucfirst($car->model->name)?></div>
                        <span><?= ucfirst($car->type->{'name_'.$lang}) ?></span>
                    </div>
                    <div class="features">
                        <p><?= $this->Html->image('icons/passengers.png')?>
                        <?= $car->passengers; ?> <?= __('passengers') ?></p>
                        <?php if ($car->auto == 1): ?>
                            <p>
                                <?= $this->Html->image('icons/autotransmission.png')?> 
                                <?= __('automatic transmission') ?>
                            </p>
                        <?php endif ?>
                        <?php if ($car->airconditioning): ?>
                            <p>
                                <?= $this->Html->image('icons/airconditioning.png')?> 
                                <?= __('air conditioning') ?>
                            </p>
                            
                        <?php endif ?>
                        <p>
                            <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                        </p>
                    </div>
                    <?php if (!empty($car->{'details_'.$lang})): ?>
                        <div class="details">
                            <div class="view-details">[+] <?= __('View car details'); ?></div>
                            <div class="close-details">[-] <?= __('Close car details'); ?></div>
                            <span class="details-more">
                                <?= $car->{'details_'.$lang} ?>
                            </span>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <?php endforeach; ?>

        </div>                                                          
    </aside>  
    <div id="content" class="sidebar-middle" style="margin-left: 8px;">
        <div class="widget main-widget product-widget">
            <div class="content-overlay">
                <?= $this->Html->image('ajax-loader.gif',['class'=>'ajax-loader']); ?>
                
            </div>
            <div class="frm">
                <div class="widget-title">
                    <div>
                        <?= $this->Html->image('icons/results.png') ?>
                    <?= __('Results') ?> <span><?= $this->Paginator->counter()." ".__('Pages'); ?></span>
                    </div>
                    <div class="widget-title-sort">
                        <span class="viev-all"><?= __('Sort by') ?>: </span> 
                        <a href="#" onclick="sort('price');return false;">
                            <?= __('Price') ?>
                        </a> | 
                        <a href="#" class="current" onclick="sort('type');return false;">
                            <?= __('Type') ?>
                        </a>
                    </div>
                    <div class="clear"></div>   
                </div>
                <div class="sortable">
                <?php foreach ($extras as $extra) :?>
                <div class="post" data-price="<?= (!empty($extra->price)) ? $extra->price->price_mad : 0 ;?>" data-type="<?= $extra->type; ?>">
                    <div class="main-block_container">
                        <div class="main-block">                                    
                            <div class="product-img">
                                <?= $this->Html->image("extras/".$extra->img);?>
                            </div>
                            <div class="product-info">
                                <h3 class="entry-format"><?= $extra->{'name_'.$lang}; ?></h3>
                                <div class="features">
                                    <?= $extra->{'description_'.$lang}; ?>
                                </div>
                                <?php if (!empty($extra->{'details_'.$lang})) : ?>
                                <div class="details">
                                    <div class="view-details">[+] View car details</div>
                                    <div class="close-details">[-] Close car details</div>
                                    <span class="details-more">
                                        <?= $extra->{'details_'.$lang}; ?>
                                    </span>
                                </div>
                                <?php endif; ?>
                            </div>                              
                        </div>
                        <div class="additional-block">
                            <br>
                            <?php if (empty($extra->price)) :?>
                                <p><?= __('FREE') ?></p>
                            <?php else: ?>
                                <?php $currency = ($lang=='fr') ? $extra->price->price_euro.'€' : '$'.$extra->price->price_usd ?>
                                <p><?= $extra->price->price_mad." MAD";?></p>
                                <p>(<?= $currency ?>)</p>
                            <?php endif; ?>
                            <br>
                            <?php if (!empty($this->request->session()->read('Cart.car_id'))) :?>
                                <input class="continue_button blue_button" type="button" value="<?= __('Add to Cart')?>" />
                            <?php else: ?>
                                <?= $this->Html->link(__('Choose a Car First'), ['controller'=>'Cars','action'=>'index'],['class'=>'continue_button blue_button']) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="post-delimiter"></div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="pagination-block"> 
            <div class="pagination"> 
                <ul class="pagination">
                    <?= $this->Paginator->prev('') ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next('') ?>
                </ul>
                <p class="clear"></p> 
            </div> 
        </div>
    </div>              
    <div class="clear"></div>
    </div>
</div>
<style type="text/css">
    .widget-content.product-widget-mini{
        position: relative;
        min-height: 160px;
    }

    .custom-img{
        position: absolute;
        right: 10px;
    }
    .widget>h4{
        height: 30px;
        text-transform: capitalize;
        line-height: 30px;
        font-size: 12px;
        font-weight: bold;
    }

    .car-price{
        margin-left: 10px;
        margin-right: 5px;
        font-size: 16px;
        color: #000;
    }
</style>