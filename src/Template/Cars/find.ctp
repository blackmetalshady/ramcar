<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>

<!--<div id="progress-bar">
    <div id="progress-bar-steps">
        <div class="progress-bar-step done">
            <div class="step_number">1</div>
            <div class="step_name"><?= __('Create request') ?></div>
        </div>
        <div class="progress-bar-step current">
            <div class="step_number">2</div>
            <div class="step_name"><?= __('Choose a car') ?></div>
        </div>
        <div class="progress-bar-step">
            <div class="step_number">3</div>
            <div class="step_name"><?= __('Choose extras') ?></div>
        </div>
        <div class="progress-bar-step last">
            <div class="step_number">4</div>
            <div class="step_name"><?= __('Review &amp; Book') ?></div>
        </div>
    </div>
    <div class="clear"></div>
</div>   -->
<div id="progress-bar"> 
    <div id="progress-bar-steps"> 
        <div class="progress-bar-step done"> 
            <div class="step_number">1</div> 
            <div class="step_name"><?= __('Create request') ?></div> 
            <div class="clear"></div> 
        </div> 
        <div class="progress-bar-step current"> 
            <div class="step_number">2</div> 
            <div class="step_name"><?= __('Choose a car') ?></div> 
            <div class="clear"></div> 
        </div> 
        <div class="progress-bar-step "> 
            <div class="step_number">3</div> 
            <div class="step_name"><?= __('Choose extras') ?></div> 
            <div class="clear"></div> 
        </div> 
            <div class="progress-bar-step last"> 
            <div class="step_number">4</div> 
            <div class="step_name"><?= __('Review &amp; Book') ?></div> 
            <div class="clear"></div> 
        </div> 
    </div> 
    <div class="clear"></div> 
</div>  
<div id="main">
    <?= $this->Form->create(null, ['url'=>['controller'=>'Cars', 'action'=>'find'], 'class'=>'main-form','id'=>'slider-form','style'=>'float: none; width: calc(100% - 2px); margin: 18px auto;']) ?>
        <div id="book_car" class="title-form current">
            <?= $this->Html->image('icons/search.png') ?>
            <?= __('Search for a car'); ?>:
        </div>
        <div id="book_car_content" class="content-form ">
            <div class="location-block">
                <div class="form-block location"><?= __('Location'); ?></div>
                    <?= $this->Form->input('pickup_location',[
                        'label'=>false,
                        'required'=>true,
                        'class'=>'location',
                        'id'=>'pickup-location',
                        'placeholder'=>__('Enter a location')
                    ]);?>
                <input id="location-checkbox" type="checkbox" class="styled" name="checkbox_location" value="1" />
                <label for="location-checkbox"> <?= __('Return at different location'); ?></label>
                <div class="location-block return_location">
                    <div class="form-block location"><?= __('Return location'); ?></div>
                    <?= $this->Form->input('return_location',[
                        'label'=>false,
                        'class'=>'location',
                        'id'=>'return-location',
                        'placeholder'=>__('Return location')
                    ]);?>
                </div>
            </div>
            <div class="form-block pick-up">
                <h4><?= __('Pick-up date'); ?></h4>
                    <?= $this->Form->input('from_date',[
                        'label'=>false,
                        'required'=>true,
                        'class'=>'datepicker'
                    ]);?>
                    <?= $this->Form->input('from_time',[
                        'label'=>false,
                        'required'=>true,
                        'class'=>'time-select',
                        'size'=>'5'
                    ]);?>
            </div>
            <div class="form-block drop-off">
                <h4><?= __('Drop-off date'); ?></h4>
                    <?= $this->Form->input('to_date',[
                        'label'=>false,
                        'required'=>true,
                        'class'=>'datepicker'
                    ]);?>
                    <?= $this->Form->input('to_time',[
                        'label'=>false,
                        'required'=>true,
                        'class'=>'time-select',
                        'size'=>'5'
                    ]);?>
                <!-- <input class="datepicker" type="text" name="to_date" value="" id="to-date" />
                <input class="time-select" type="text" size="5" value="" name="to_time" id="to-time" /> -->
            </div>
            <div class="form-block car-type">
                <h4><?= __('Car type'); ?></h4>
                <div class="car-type-select">
                    <?= $this->Form->select(
                        'cartype',
                        $cartypes,
                        ['empty' => __('All'),'class'=>'select']
                        ); 
                    ?>
                </div>
            </div>
            <div class="form-block form-submit">
                <?= $this->Form->submit(__('Update'),['class'=>'orange_button form-update']) ?>
                <!-- <input class="orange_button form-update" type="submit" value="Update" /> -->
            </div>
        </div>                  
        <div class="clear"></div>
    <?= $this->Form->end(); ?> 
    <div id="primary">                  
        <div class="clear"></div>
        <aside class="sidebar-left" style="margin-left: -2px"> 
            <div class="widget info_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/info.png') ?>
                    Info
                </h3>
                <div class="widget-content"><strong><?= __('Please Note') ?>: </strong> <?= __('The vehicles shown are examples. Specific models within a car class may vary in availability and features, such as passenger seating, luggage capacity and mileage'); ?>.</div>
            </div>
            <div class="widget filter_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/filter.png') ?>
                    <?= __('Filter results') ?>
                </h3>
                <h4><?= __('Price range') ?></h4>
                <div class="widget-content-range">                              
                    <input class="price_range" value="0;1500" name="price_range" />
                </div>
                <h4><?= __('Fuel') ?></h4>
                <div class="widget-content-range">                              
                    <?php foreach ($fuels as $fuel) :?>    
                        <div class="filter">
                            <input id="fuels_<?= $fuel->{'name_'.$lang}; ?>" type="checkbox" class="styled" name="fuels_<?= $fuel->name; ?>" value="<?= $fuel->id; ?>" data-type="<?= $fuel->{'name_'.$lang}; ?>" checked/> 
                            <label for="manufacturers_<?= $fuel->{'name_'.$lang}; ?>"><?= $fuel->{'name_'.$lang}; ?></label> 
                            <div class="filter_quantity"><?= sizeof($fuel->cars); ?></div>
                        </div>
                    <?php endforeach; ?>  
                </div>
                <h4><?= __('Manufacturers') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($carsbybrand as $brand) :?>    
                        <?php if (sizeof($brand->cars) > 0) :?> 
                        <div class="filter">
                            <input id="manufacturers_<?= $brand->name; ?>" type="checkbox" class="styled" name="manufacturers_<?= str_replace(' ', '_', $brand->name); ?>" value="<?= $brand->id; ?>" data-type="<?= str_replace(' ', '_', $brand->name); ?>" checked /> 
                            <label for="manufacturers_<?= str_replace(' ', '_', $brand->name); ?>"><?= $brand->name; ?></label> 
                            <div class="filter_quantity"><?= sizeof($brand->cars); ?></div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>                             
                </div>
                <h4><?= __('Number of passangers') ?></h4>
                <div class="widget-content-range">        
                    <div class="shortcode_single_slider">
                        <input class="shortcode_range" value="0;5" name="shortcode_single_range" />
                    </div>
                </div>
                <h4><?= __('Vehicle type') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($types as $type) :?>
                    <div class="filter">
                        <input id="type_<?= $type->{'name_'.$lang} ?>" type="checkbox" class="styled" name="type_<?= $type->{'name_'.$lang} ?>" value="<?= $type->id ?>" data-type="<?= $type->{'name_'.$lang} ?>" checked /> 
                        <label for="type_<?= $type->{'name_'.$lang} ?>"><?= ucfirst($type->{'name_'.$lang})?></label> 
                        <div class="filter_quantity"><?= sizeof($type->cars) ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>                                                          
        </aside>    
        <div id="content" class="sidebar-middle" style="margin-left: 10px;">
            <div class="widget main-widget product-widget">
                <div class="content-overlay">
                    <?= $this->Html->image('ajax-loader.gif',['class'=>'ajax-loader']); ?>
                    
                </div>
                <div class="frm sortable">
                    <div class="widget-title">
                        <div>
                            <?= $this->Html->image('icons/results.png') ?>
                            <?= __('All Cars') ?> <span><?= $this->Paginator->counter()." ".__('Pages'); ?> </span>
                        </div>
                        <div class="widget-title-sort">
                            <span class="viev-all"><?= __('Sort by') ?>: </span> 
                             <a href="#"  onclick="sort('type');return false;" class="current sortby" id="sortbytype">
                                <?= __('Type') ?>
                             </a> 
                             | 
                             <a href="#" onclick="sort('price');return false;" class="sortby" id="sortbyprice">
                                <?= __('Price') ?>
                             </a>
                        </div>
                        <div class="clear"></div>   
                    </div>
                    <?php foreach ($cars as $car) :?>
                        <div class="post <?= str_replace(' ', '_', $car->brand->name); ?> <?= $car->fuel->{'name_'.$lang}; ?> <?= $car->type->{'name_'.$lang}; ?>" data-pmad="<?= (!empty($car->prices))? $car->prices[0]->price_mad : '0'; ?>" data-price="<?php if(!empty($car->prices)){ ($lang == 'fr') ? $car->prices[0]->price_euro : $car->prices[0]->price_usd ; } ?>" data-type="<?= $car->type_id; ?>" data-passengers="<?= $car->passengers; ?>">
                            <div class="main-block_container">
                                <div class="main-block">                                    
                                    <div class="product-img" data-id="<?= $car->id; ?>">
                                        <?= $this->Html->image("cars/".$car->medias[0]->name,['class'=>'grow']);?>
                                    </div>
                                    <div style="display: none" id="gallery<?= $car->id; ?>" class="gl">
                                        <?php foreach ($car->medias as $media) :?>
                                            <a href="/img/cars/<?= $media->name; ?>">
                                                <?= $this->Html->image("cars/".$media->name);?>
                                            </a>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="product-info">
                                        <h3 class="entry-format"><?php $carname = $car->brand->name." ".$car->model->name; ?> <?=(strlen($carname) > 24)?$car->model->name : $carname ?><span>|  <?= ucfirst($car->type->{'name_'.$lang}); ?></span></h3>
                                        <div class="features">
                                            <p><?= $this->Html->image('icons/passengers.png')?>
                                            <?= $car->passengers; ?> <?= __('passengers') ?></p>
                                            <?php if ($car->auto == 1): ?>
                                                <p>
                                                    <?= $this->Html->image('icons/autotransmission.png')?> 
                                                    <?= __('automatic transmission') ?>
                                                </p>
                                            <?php endif ?>
                                            <?php if ($car->airconditioning): ?>
                                                <p>
                                                    <?= $this->Html->image('icons/airconditioning.png')?> 
                                                    <?= __('air conditioning') ?>
                                                </p>
                                                
                                            <?php endif ?>
                                            <p>
                                                <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                                            </p>
                                        </div>
                                        <?php if (!empty($car->{'details_'.$lang})): ?>
                                            <div class="details">
                                                <div class="view-details">[+] <?= __('View car details'); ?></div>
                                                <div class="close-details">[-] <?= __('Close car details'); ?></div>
                                                <span class="details-more">
                                                    <?= $car->{'details_'.$lang} ?>
                                                </span>
                                            </div>
                                        <?php endif ?>
                                        
                                    </div>                              
                                </div>
                                <div class="additional-block">
                                    <?php if (!empty($car->prices)) : ?>
                                    <p><?= $car->prices[0]->price_mad." MAD"?></p>
                                    <p>(<?= ($lang == 'fr') ? $car->prices[0]->price_euro."€" : '$'.$car->prices[0]->price_usd ; ?>)</p>
                                    <?php endif; ?>
                                    <p class="span"><?= __('Unlimited free miles included')?></p>
                                    <?php if (!empty($car->prices)) : ?>
                                    <?= $this->Form->create(null, ['url'=>['controller'=>'Cars', 'action'=>'addtocart']]) ?>
                                        <?= $this->Form->hidden('car_id', ['value'=>$car->id])  ?>
                                        <input class="continue_button blue_button" type="submit" value="<?= __('Add to Cart'); ?>" />
                                    <?= $this->Form->end() ?>
                                    <?php else: ?>
                                        <?= __('Not Available') ?>
                                    <?php endif; ?> 
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="post-delimiter"></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="pagination-block"> 
                <div class="pagination"> 
                    <ul class="pagination">
                        <?= $this->Paginator->prev('') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('') ?>
                    </ul>
                    <p class="clear"></p> 
                </div> 
            </div>
        </div>              
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
    .pagination li{
        padding: 0px;
        margin-bottom: 15px;
    }
    .pagination li a{
        margin: 0px; 
        display: block; 
        width: 100%; 
        height: 100%
    }
    .pagination li a:hover{
        color: #000;
    }
    .product-widget .additional-block .span {
        margin: 10px 0 10px;
    }
    .product-img img{
        border-radius: 5px;
    }
    .input{
        display: inline;
    }
    <?php if ($lang == 'fr'):?>
    input.form-update.orange_button:hover{
        background:url(/img/icons/update.png) no-repeat 115px center #ec702f !important;
    }

    input.form-update.orange_button{
        min-width: 140px !important;
        background:url(/img/icons/update.png) no-repeat 115px center #f08643 !important;
    }
    <?php endif; ?>
</style>
<script type="text/javascript">
$(function() {
    <?php if ($lang == 'en') :?>
        $(".datepicker").datepicker({
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 2
        });
    <?php else: ?>
        $( ".datepicker" ).datepicker({
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
        weekHeader: 'Sem.',
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 2
        });
    <?php endif; ?>

    var availableTags = [
      <?php foreach ($locations as $location) {
          echo '"'.$location->{'name_'.$lang}.'",';
      } ?>
    ];
    $( "#pickup-location" ).autocomplete({
        minLength: 0,
        source: availableTags,
        select: function( event, ui ) {
            var returnlocation = $( "#return-location" ).val();
            if (!returnlocation) {
                $( "#return-location" ).val(ui.item.label);
            }
        }
    }).on('focus', function(event) {
            $(this).autocomplete("search", "");
        });

    $( "#return-location" ).autocomplete({
      minLength: 0,
      source: availableTags
    }).on('focus', function(event) {
            $(this).autocomplete("search", "");
        });

     $('.gl').lightGallery({
            thumbnail:true
        }); 

     

});
</script>