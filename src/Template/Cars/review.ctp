<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<?php //print_r($this->request->session()->read('Cart.extras')); die(); ?>
<div id="progress-bar">
    <div id="progress-bar-steps">
        <div class="progress-bar-step done">
            <div class="step_number">1</div>
            <div class="step_name"><?= __('Create request') ?></div>
        </div>
        <div class="progress-bar-step done">
            <div class="step_number">2</div>
            <div class="step_name"><?= __('Choose a car') ?></div>
        </div>
        <div class="progress-bar-step done">
            <div class="step_number">3</div>
            <div class="step_name"><?= __('Choose extras') ?></div>
        </div>
        <div class="progress-bar-step current last">
            <div class="step_number">4</div>
            <div class="step_name"><?= __('Review &amp; Book') ?></div>
        </div>
    </div>
    <div class="clear"></div>
</div>  
<div id="main">
    <div id="primary" style="background: none; padding-top: 38px;">                  
    <aside class="sidebar-left"> 
        <div class="widget">
            <h3 class="widget-title">
                <?= $this->Html->image('icons/orderinfo.png'); ?>
                Order Info
            </h3>
            <h4>
                <?= __('Car') ?>
                <?= $this->Html->link(__('Edit'),['controller'=>'Cars','action'=>'find','#'=>'slider-form']) ?>
            </h4>
            <div class="widget-content main-block product-widget-mini">                             
                <div class="product-img" style='width: 130px'>
                    <?= $this->Html->image('cars/'.$car->medias[0]->name,['style'=>'width: 130px']); ?>
                </div>
                <div class="product-info">
                    <div class="entry-format">
                        <div><?= ucfirst($car->brand->name)." ".ucfirst($car->model->name)?></div>
                        <span><?= ucfirst($car->type->{'name_'.$lang}) ?></span>
                    </div>
                    <div class="features">
                        <p><?= $this->Html->image('icons/passengers.png')?>
                        <?= $car->passengers; ?> <?= __('passengers') ?></p>
                        <?php if ($car->auto == 1): ?>
                            <p>
                                <?= $this->Html->image('icons/autotransmission.png')?> 
                                <?= __('automatic transmission') ?>
                            </p>
                        <?php endif ?>
                        <?php if ($car->airconditioning): ?>
                            <p>
                                <?= $this->Html->image('icons/airconditioning.png')?> 
                                <?= __('air conditioning') ?>
                            </p>
                            
                        <?php endif ?>
                        <p>
                            <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                        </p>
                    </div>
                    <?php if (!empty($car->{'details_'.$lang})): ?>
                        <div class="details">
                            <div class="view-details">[+] <?= __('View car details'); ?></div>
                            <div class="close-details">[-] <?= __('Close car details'); ?></div>
                            <span class="details-more">
                                <?= $car->{'details_'.$lang} ?>
                            </span>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <h4>
                Date &amp; Location
                <?= $this->Html->link(__('Edit'),['controller'=>'Cars','action'=>'find','#'=>'slider-form']) ?>
            </h4>
            <div class="widget-content widget-info">                                    
                <h4>Pick Up time</h4>
                <p><?= $searchdata['from_date']." ".__('at')." ".$searchdata['from_time']; ?></p>
                <h4>Return time</h4>
                <p><?= $searchdata['to_date']." ".__('at')." ".$searchdata['to_time']; ?></p>
                <h4>Pickup Location</h4>
                <p><?= $searchdata['pickup_location']; ?></p>
                <h4>Return Location</h4>
                <p><?= $searchdata['return_location']; ?></p>
                <div class="subtotal_content">
                    <div class="subtotal" style="min-height: 35px;">              
                        <?= __('Subtotal') ?>: <span class="price" data-priceox="<?= $price_ox ?>"><?= ($lang=='fr') ? $price_ox.' €' : '$ '.$price_ox; ?></span>
                        <br>
                        <span class="price" data-pricemad="<?= $price_mad ?>" style="font-size: 12px;">(<?= $price_mad.' MAD' ?>)</span>
                    </div>
                </div>
            </div>
            <h4 class="extras">Extras</h4>
            <div class="widget-content widget-extras" id="extraslist">
                <?php if (!empty($cartextras)): ?>
                      <?php foreach ($cartextras as $ex) :?>  
                            <p id="pex<?= $ex->id ?>">
                                <?= $ex->{'name_'.$lang}?>
                                <span class="price">
                                    <?php if (empty($ex->price)): ?>
                                        FREE
                                    <?php else: ?>
                                       <?= ($lang == 'fr') ? $ex->price->price_euro*$nbrofdays.' €' : '$'.$ex->price->price_usd*$nbrofdays ; ?> 
                                    <?php endif ?>
                                <span class="removextra" data-extra="<?= $ex->id ?>">
                                    <i class='fa fa-remove'></i>
                                </span>
                                </span>
                            </p>
                      <?php endforeach; ?>                        
                <?php endif ?>                              
            </div>
            <div class="widget-footer widget-footer-total">
                Total: <span class="price" id="total_ox" data-totalox="<?= $total_ox ?>"><?= ($lang == 'fr') ? $total_ox.' €': '$'.$total_ox ; ?> </span><br>
                <h5 data-totalmad="<?= $total_mad?>">(<?= $total_mad ?> MAD)</h5>
            </div>
        </div>                                                          
    </aside>  
    <div id="content" class="sidebar-middle" style="margin-left: 8px;">
    	<div class="widget main-widget product-widget">
			<div class="widget-title">
				<div>
					<?= $this->Html->image('icons/results.png') ?>
					<?= __('Complete Reservation Form') ?>
				</div>
				<div class="clear"></div>	
			</div>
			   <?php if (!$islogged) :?>
				<div class="widget-content widget-note notlogged">
		    		<span><?= __('Notice'); ?>:</span> <?= __('You are currently not logged, if you continue as an ANONYMOUS USER the content of your cart will be lost and we will get to you by e-mail to confirm your booking, We highely recommend that you'); ?> <a href="#" id="rev_sign"><?= __('Sign in') ?></a> <?= __('or') ?> <a href="#" id="rev_register"><?= __('Register') ?></a>.
		    	</div>
		    	<div class="ifanonymous" style="display: none;">
					<h4><?= __('Note') ?></h4>
					<div class="widget-content widget-note">									
						<h4><?= __('Upon completing this reservation, you will receive') ?>:</h4>								
						<ul>
							<li><?= __('Your rental voucher to produce on arrival at the rental desk') ?></li>
							<li><?= __('A toll-free customer support number') ?></li>
						</ul>
					</div>
					<h4><?= __('Personal information') ?></h4>
					<div class="widget-content">
						<?= $this->Form->create(null, ['url'=>['controller'=>'Reservations','action'=>'add'],'id'=>'anonform']);?>
						<div class="form_element">
							<div><?= __('First name') ?></div>
							<input type="text" value="" placeholder="<?= __('Enter your First name') ?>" name="first_name" required/>
						</div>
						<div class="form_element">
							<div><?= __('Last name') ?></div>
							<input type="text" value="" placeholder="<?= __('Enter your Last name') ?>" name="last_name" required/>
						</div>
						<div class="form_element">
							<div><?= __('Age') ?></div>
							<div class="product-select-count">
								<select class="custom-select" name="age" required>
									<?php for ($i=16; $i < 71; $i++){
										if ($i == 25) {
											echo "<option value='$i' selected>$i</option>";
										}else{
											echo "<option value='$i'>$i</option>";
										}
									} ?>
								</select>
							</div>
						</div>
						<div class="clear"></div>
						<div class="form_element">
							<div><?= __('Email Address') ?></div>
							<input type="text" value="" placeholder="<?= __('Enter your Email Address') ?>" name="email" required/>
						</div>
						<div class="form_element">
							<div><?= __('Confirm Email Address') ?></div>
							<input type="email" value="" placeholder="<?= __('Confirm your Email Address') ?>" name="confirm_email" required/>
						</div>
						<div class="clear"></div>
						<div class="form_element">
							<div><?= __('Phone Number') ?></div>
							<input type="text" value="" placeholder="<?= __('Enter your Phone Number') ?>" name="phone" required/>
						</div>
						<div class="form_element">
							<div><?= __('Driver\'s License Number') ?></div>
							<input type="text" value="" placeholder="<?= __('Driver\'s License Number') ?>" name="drvlicense" required/>
						</div>
						<div class="clear"></div>
						<div class="form_element form_element_checkbox">
							<input id="newsletter-chkbx" type="checkbox" class="styled" name="newsletter" value="1" /> 
							<label for="newsletter-chkbx"><?= __('Send me latest news &amp; updates') ?></label>
						</div>
						<div class="clear"></div>
						<?= $this->Form->end(); ?>
					</div>
				</div>	
			<?php else: ?>
				<h4><?= __('Car') ?></h4>
				<div class="post" data-price="<?= $car->prices[0]->price_mad; ?>" data-type="<?= $car->type_id; ?>">
                    <div class="main-block_container">
                        <div class="additional-block_container">
                            <div class="main-block">                                    
                                <div class="product-img" data-id="<?= $car->id; ?>">
                                    <?= $this->Html->image("cars/".$car->medias[0]->name,['class'=>'grow']);?>
                                </div>
                                <div style="display: none" id="gallery<?= $car->id; ?>" class="gl">
                                    <?php foreach ($car->medias as $media) :?>
                                        <a href="/img/cars/<?= $media->name; ?>">
                                            <?= $this->Html->image("cars/".$media->name);?>
                                        </a>
                                    <?php endforeach;?>
                                </div>
                                <div class="product-info">
                                    <h3 class="entry-format"><?= $car->brand->name." ".$car->model->name; ?> <span>|  <?= ucfirst($car->type->{'name_'.$lang}); ?></span></h3>
                                    <div class="features">
                                        <p><?= $this->Html->image('icons/passengers.png')?>
                                        <?= $car->passengers; ?> <?= __('passengers') ?></p>
                                        <?php if ($car->auto == 1): ?>
                                            <p>
                                                <?= $this->Html->image('icons/autotransmission.png')?> 
                                                <?= __('automatic transmission') ?>
                                            </p>
                                        <?php endif ?>
                                        <?php if ($car->airconditioning): ?>
                                            <p>
                                                <?= $this->Html->image('icons/airconditioning.png')?> 
                                                <?= __('air conditioning') ?>
                                            </p>
                                            
                                        <?php endif ?>
                                        <p>
                                            <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                                        </p>
                                    </div>
                                </div>                              
                            </div>
                            <div class="additional-block">
                                <input class="shortcode_button red_button changecar " type="submit" value="<?= __('Change'); ?>" style="white-space: normal;" />
                            </div>
                        </div>                                  
                    </div>
                    <div class="clear"></div>
                </div>
				<h4><?= __('Extras') ?></h4>
				<?php foreach ($cartextras as $extra) :?>
		            <div class="post" id="extra_<?= $extra->id ?>">
		                <div class="main-block_container">
		                    <div class="additional-block_container">
		                        <div class="main-block">                                    
		                            <div class="product-img">
		                                <?= $this->Html->image("extras/".$extra->img);?>
		                            </div>
		                            <div class="product-info">
		                                <h3 class="entry-format"><?= $extra->{'name_'.$lang}; ?></h3>
		                                <div class="features">
		                                    <?= $extra->{'description_'.$lang}; ?>
		                                </div>
		                            </div>                              
		                        </div>
		                        <div class="additional-block">
		                            <input class="shortcode_button red_button rmextra" type="button" value="<?= __('Remove')?>" data-extra="<?= $extra->id ?>"/>
		                        </div>
		                    </div>                                  
		                </div>
		                <div class="clear"></div>
		            </div>
		        <?php endforeach; ?>
			<?php endif; ?>																				
		</div>
        <br>
        <div class="next_page">
        	<?php if (!$islogged) :?>
            	<input id="anon" class="continue_button blue_button" type="button" value="<?= __('Continue Anonymously') ?>" />
        	<?php else : ?>
                <?= $this->Form->create(null, ['url'=>['controller'=>'Reservations','action'=>'add'], 'id'=>'authForm'])?>
        		<input id="confirmres" class="continue_button blue_button" type="submit" value="<?= __('Confirm Reservation') ?>" />
                <?= $this->Form->end() ?>
            <?php endif; ?>
        </div>
    </div>              
    <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
$(document).on('click','#anon', function(){
	$('.notlogged').hide();
	$('.ifanonymous').show();
	$('.next_page').html('<input id="submitreq" class="continue_button blue_button" type="button" value="<?= __('Submit Request') ?>" />');
});

$(document).on('click','.removextra', function(){
    if (confirm("<?= __('Are you sur you want to remove this Extra from Cart?')?>")) {
        var extra_id = $(this).data("extra");
        $.post('/extras/removefromcart', {extra_id: extra_id})
            .done(function(data){
                if (data == '0') {
                    $('p#pex'+extra_id).remove();
                }else{
                    alert("<?= __('Could Not Remove the Extra From your Cart, please Try Again') ?>")
                }
            })
    }
});

$(document).on('click','.rmextra', function(){
    if (confirm("<?= __('Are you sur you want to remove this Extra from Cart?')?>")) {
        var extra_id = $(this).data("extra");
        $.post('/extras/removefromcart', {extra_id: extra_id})
            .done(function(data){
                if (data == '0') {
                    $('#extra_'+extra_id).remove();
                }else{
                    alert("<?= __('Could Not Remove the Extra From your Cart, please Try Again') ?>")
                }
            })
    }
});

</script>