<?php use Cake\I18n\I18n; ?>
<?php //print_r($this->request->session()->read('Cart')); die(); ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
        
<div id="main">
    <?= $this->Form->create(null, ['url'=>['controller'=>'Cars', 'action'=>'find'], 'class'=>'main-form','id'=>'slider-form','style'=>'float: none; width: 100%; margin: 18px auto;']) ?>
        <div id="book_car" class="title-form current">
            <?= $this->Html->image('icons/search.png') ?>
            <?= __('Search for a car'); ?>:
        </div>
        <div id="book_car_content" class="content-form ">
            <div class="location-block">
                <div class="form-block location"><?= __('Location'); ?></div>
                    <?= $this->Form->input('pickup_location',[
                        'label'=>false,
                        'class'=>'location',
                        'id'=>'pickup-location',
                        'placeholder'=>__('Enter a location')
                    ]);?>
                <input id="location-checkbox" type="checkbox" class="styled" name="checkbox_location" value="1" />
                <label for="location-checkbox"> <?= __('Return at different location'); ?></label>
                <div class="location-block return_location">
                    <div class="form-block location"><?= __('Return location'); ?></div>
                    <?= $this->Form->input('return_location',[
                        'label'=>false,
                        'class'=>'location',
                        'id'=>'return-location',
                        'placeholder'=>__('Return location')
                    ]);?>
                </div>
            </div>
            <div class="form-block pick-up">
                <h4><?= __('Pick-up date'); ?></h4>
                    <?= $this->Form->input('from_date',[
                        'label'=>false,
                        'autocomplete'=>'off',
                        'class'=>'datepicker',
                        'required'=>'required',
                        'id'=>'pickup_date'
                    ]);?>
                    <?= $this->Form->input('from_time',[
                        'label'=>false,
                        'autocomplete'=>'off',
                        'class'=>'time-select',
                        'id'=>'from-time',
                        'required'=>'required',
                        'size'=>'5'
                    ]);?>
            </div>
            <div class="form-block drop-off">
                <h4><?= __('Drop-off date'); ?></h4>
                <input class="datepicker" id="return_date" type="text" name="to_date" value="" autocomplete="off" required/>
                <input class="time-select" type="text" size="5" value="" name="to_time" id="to-time" readonly="readonly" />
            </div>
            <div class="form-block car-type">
                <h4><?= __('Car type'); ?></h4>
                <div class="car-type-select">
                <?= $this->Form->select(
                        'cartype',
                        $cartypes,
                        ['empty' => __('All'),'class'=>'select','id'=>'cartype']
                        ); 
                ?>
                </div>
            </div>
            <div class="form-block form-submit">
                <?= $this->Form->submit(__('Search'),['class'=>'orange_button form-search']) ?>
                <!-- <input class="orange_button form-update" type="submit" value="Update" /> -->
            </div>
        </div>                  
        <div class="clear"></div>
    <?= $this->Form->end(); ?> 
    <div id="primary">                  
        <div class="clear"></div>
        <aside class="sidebar-left"> 
            <div class="widget info_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/info.png') ?>
                    Info
                </h3>
                <div class="widget-content"><strong><?= __('Please Note') ?>: </strong> <?= __('The vehicles shown are examples. Specific models within a car class may vary in availability and features, such as passenger seating, luggage capacity and mileage') ?>.</div>
            </div>
            <div class="widget filter_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/filter.png') ?>
                    <?= __('Filter results') ?>
                </h3>
                <h4><?= __('Price range') ?></h4>
                <div class="widget-content-range">                              
                    <input class="price_range" value="0;1500" name="price_range" />
                </div>
                <h4><?= __('Fuel') ?></h4>
                <div class="widget-content-range">                              
                    <?php foreach ($fuels as $fuel) :?>    
                        <div class="filter">
                            <input id="fuels_<?= $fuel->{'name_'.$lang}; ?>" type="checkbox" class="styled" name="fuels_<?= $fuel->name; ?>" value="<?= $fuel->id; ?>" data-type="<?= $fuel->{'name_'.$lang}; ?>" checked/> 
                            <label for="manufacturers_<?= $fuel->{'name_'.$lang}; ?>">
                                <?= $fuel->{'name_'.$lang}; ?>
                            </label> 
                            <div class="filter_quantity"><?= sizeof($fuel->cars); ?></div>
                        </div>
                    <?php endforeach; ?>  
                </div>
                <h4><?= __('Manufacturers') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($carsbybrand as $brand) :?>    
                        <?php if (sizeof($brand->cars) > 0) :?> 
                        <div class="filter">
                            <input id="manufacturers_<?= str_replace(' ', '_', $brand->name); ?>" type="checkbox" class="styled" name="manufacturers_<?= $brand->name; ?>" value="<?= $brand->id; ?>" data-type="<?= str_replace(' ', '_', $brand->name); ?>" checked/> 
                            <label for="manufacturers_<?= str_replace(' ', '_', $brand->name); ?>"><?= $brand->name; ?></label> 
                            <div class="filter_quantity"><?= sizeof($brand->cars); ?></div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>                             
                </div>
                <h4><?= __('Number of passangers') ?></h4>
                <div class="widget-content-range">        
                    <div class="shortcode_single_slider">
                        <input class="passangers_range" value="0;5" name="shortcode_single_range" />
                    </div>
                </div>
                <h4><?= __('Vehicle type') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($types as $type) :?>
                    <div class="filter">
                        <input id="type_<?= $type->{'name_'.$lang} ?>" type="checkbox" class="styled" name="type_<?= $type->{'name_'.$lang} ?>" value="<?= $type->id ?>" data-type="<?= $type->{'name_'.$lang} ?>" checked /> 
                        <label for="type_<?= $type->{'name_'.$lang} ?>"><?= ucfirst($type->{'name_'.$lang})?></label> 
                        <div class="filter_quantity"><?= sizeof($type->cars) ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>                                                          
        </aside>    
        <div id="content" class="sidebar-middle" style="margin-left: 10px; margin-right: 0px;">
            <div class="widget main-widget product-widget">
                <div class="content-overlay">
                    <?= $this->Html->image('ajax-loader.gif',['class'=>'ajax-loader']); ?>
                    
                </div>
                <div class="frm sortable">
                    <div class="widget-title">
                        <div>
                            <?= $this->Html->image('icons/results.png') ?>
                            <?= __('All Cars') ?> <span><?= $this->Paginator->counter()." ".__('Pages'); ?> </span>
                        </div>
                        <div class="widget-title-sort">
                            <span class="viev-all"><?= __('Sort by') ?>: </span> 
                             <a href="#"  onclick="sort('type');return false;" class="current sortby" id="sortbytype">
                                <?= __('Type') ?>
                             </a> 
                             | 
                             <a href="#" onclick="sort('price');return false;" class="sortby" id="sortbyprice">
                                <?= __('Price') ?>
                             </a>
                        </div>
                        <div class="clear"></div>   
                    </div>
                    <!-- <div class="sortable"> -->
                    <?php foreach ($cars as $car) :?>
                        <?php 
                            if (empty($car->prices)) {
                                $price_mad = 0;
                                $price_ox = 0;
                            }else{
                                $price_mad = $car->prices[0]->price_mad;
                                if ($lang == 'fr') {
                                    $price_ox = $car->prices[0]->price_euro." €";
                                    $price = $car->prices[0]->price_euro;
                                }else{
                                    $price_ox = "$".$car->prices[0]->price_usd;
                                    $price = $car->prices[0]->price_usd;
                                }
                            }
                        ?>

                        <div class="post <?= str_replace(' ', '_', $car->brand->name); ?> <?= $car->fuel->{'name_'.$lang}; ?> <?= $car->type->{'name_'.$lang}; ?>" data-pmad="<?= $price_mad; ?>" data-price="<?= $price; ?>" data-type="<?= $car->type_id; ?>" data-passengers="<?= $car->passengers; ?>">
                            <div class="main-block_container">
                                <!-- <div class="additional-block_container"> -->
                                    <div class="main-block">                                    
                                        <div class="product-img" data-id="<?= $car->id; ?>">
                                            <?= $this->Html->image("cars/".$car->medias[0]->name,['class'=>'grow']);?>
                                        </div>
                                        <div style="display: none" id="gallery<?= $car->id; ?>" class="gl">
                                            <?php foreach ($car->medias as $media) :?>
                                                <a href="/img/cars/<?= $media->name; ?>">
                                                    <?= $this->Html->image("cars/".$media->name);?>
                                                </a>
                                            <?php endforeach;?>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="entry-format"><?php $carname = $car->brand->name." ".$car->model->name; ?> <?=(strlen($carname) > 24)?$car->model->name : $carname ?><span>|  <?= ucfirst($car->type->{'name_'.$lang}); ?></span></h3>
                                            <div class="features">
                                                <p><?= $this->Html->image('icons/passengers.png')?>
                                                <?= $car->passengers; ?> <?= __('passengers') ?></p>
                                                <?php if ($car->auto == 1): ?>
                                                    <p>
                                                        <?= $this->Html->image('icons/autotransmission.png')?> 
                                                        <?= __('automatic transmission') ?>
                                                    </p>
                                                <?php endif ?>
                                                <?php if ($car->airconditioning): ?>
                                                    <p>
                                                        <?= $this->Html->image('icons/airconditioning.png')?> 
                                                        <?= __('air conditioning') ?>
                                                    </p>
                                                    
                                                <?php endif ?>
                                                <p>
                                                    <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                                                </p>
                                            </div>
                                            <?php if (!empty($car->{'details_'.$lang})): ?>
                                                <div class="details">
                                                    <div class="view-details">[+] <?= __('View car details'); ?></div>
                                                    <div class="close-details">[-] <?= __('Close car details'); ?></div>
                                                    <div class="details-more">
                                                        <?= $car->{'details_'.$lang} ?>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>                              
                                    </div>
                                    <div class="additional-block">
                                        <p>
                                            <?= ($price_ox == '0') ? __('Not Available') : $price_ox ; ?></p>
                                        <p class="price_mad"><?= ($price_mad == '0') ? '' : '('.$price_mad." MAD)" ; ?></p>
                                        <p class="span"><?= __('Unlimited free miles included')?></p>
                                        <?php if ($price_ox != '0') :?>
                                            <input class="continue_button blue_button check" type="submit" value="<?= __('Check Availability'); ?>" style="white-space: normal;" />
                                        <?php endif; ?>
                                    </div>
                                <!-- </div> ++++ -->
                            </div> 
                            <div class="clear"></div>
                        </div>
                        <div class="post-delimiter"></div>
                    <?php endforeach; ?>
                    <!-- </div> -->
                </div>
            </div>
            <div class="pagination-block"> 
                <div class="pagination"> 
                    <ul class="pagination">
                        <?= $this->Paginator->prev('') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('') ?>
                    </ul>
                    <p class="clear"></p> 
                </div> 
            </div>
        </div>              
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
    .pagination li{
        padding: 0px;
        margin-bottom: 15px;
    }
    .pagination li a{
        margin: 0px; 
        display: block; 
        width: 100%; 
        height: 100%
    }
    .product-widget .additional-block div {
        margin: 10px 0 10px;
    }
    .product-img img{
        border-radius: 5px;
    }
    .price_mad{
        font-size: 14px;
    }
    <?php if ($lang == 'fr'):?>
    input.form-search.orange_button:hover{
        min-width: 140px !important;
        background:url(/img/icons/update.png) no-repeat 115px center #ec702f !important;
    }

    input.form-search.orange_button{
        min-width: 140px !important;
        background:url(/img/icons/update.png) no-repeat 115px center #f08643 !important;
    }
    <?php endif; ?>
</style>
<script type="text/javascript">
    $(function() {
        <?php if ($lang == 'en') :?>
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                numberOfMonths: 2
            });
        <?php else: ?>
            $( ".datepicker" ).datepicker({
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
                weekHeader: 'Sem.',
                dateFormat: 'yy-mm-dd',
                numberOfMonths: 2
            });
        <?php endif; ?>
        $(document).click(function(e) { var ele = $(e.toElement); if (!ele.hasClass("hasDatepicker") && !ele.hasClass("ui-datepicker") && !ele.hasClass("ui-icon") && !$(ele).parent().parents(".ui-datepicker").length) $(".hasDatepicker").datepicker("hide"); });
        var availableTags = [
          <?php foreach ($locations as $location) {
              echo '"'.$location->{'name_'.$lang}.'",';
          } ?>
        ];
        $( "#pickup-location" ).autocomplete({
            minLength : 0,
            source: availableTags,
            select: function( event, ui ) {
                var returnlocation = $( "#return-location" ).val();
                if (!returnlocation) {
                    $( "#return-location" ).val(ui.item.label);
                }
            }
        }).on('focus', function(event) {
                $(this).autocomplete("search", "");
            });

        $( "#return-location" ).autocomplete({
          minLength: 0,
          source: availableTags
        }).on('focus', function(event) {
                $(this).autocomplete("search", "");
            });

         
    });
$(document).ready(function(){
    if(window.location.hash) {
        var pickup = "<?= $this->request->session()->read('Cart.searchinfo.pickup_location') ?>";
        var dropoff = "<?= $this->request->session()->read('Cart.searchinfo.return_location') ?>";
        var startdate = "<?= $this->request->session()->read('Cart.searchinfo.from_date') ?>";
        var starttime = "<?= $this->request->session()->read('Cart.searchinfo.from_time') ?>";
        var enddate = "<?= $this->request->session()->read('Cart.searchinfo.to_date') ?>";
        var endtime = "<?= $this->request->session()->read('Cart.searchinfo.to_time') ?>";
        var type = "<?= $this->request->session()->read('Cart.searchinfo.cartype') ?>";
        $('#pickup-location').val(pickup);
        $('#return-location').val(dropoff);
        $('#pickup_date').val(startdate);
        $('#from-time').val(starttime);
        $('#return_date').val(enddate);
        $('#to-time').val(endtime);
        $('#cartype').val(type);
        $('#slider-form').submit();
        //checkavalaibility()    
    };
})
</script>