<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<?php //print_r($reservation); die(); ?>
<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;">
    	<center style="margin-top: 40px; margin-bottom: 30px; margin-left: 20px;">
    		<?= $this->Form->create(null, ['url'=>['controller'=>'Reservations', 'action'=>'status']]);?>
            <span>
            <input type="text" name="code" class="input_placeholder" placeholder="<?= __('Enter your Code') ?>" required="required">
            </span>
            <span>
            <input type="submit" class='orange_button form-continue' value="<?= __('Check') ?>">
            </span>
            <?= $this->Form->end() ?>
    	</center>
        <?php if (isset($reservation)) :?>
            <h4>
                <?= __('Customer Personal Informations') ?>
            </h4>
            <div id="guestinfo">
                <span><?= ucfirst($reservation->guest->first_name)?> </span>
                <span> <?= ucfirst($reservation->guest->last_name)?></span>
            </div>
            <h4>
                <?= __('Car Informations') ?>
            </h4>
            <div id="carinfo" class="widget main-widget product-widget">
                <?php foreach ($reservation->cars as $car) :?>
                        <div class="post">
                            <div class="main-block_container">
                                <div class="additional-block_container">
                                    <div class="main-block">                                    
                                        <div class="product-img">
                                            <?= $this->Html->image("cars/".$car->medias[0]->name,['class'=>'grow','style'=>'width: 180px']);?>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="entry-format"><?= $car->brand->name." ".$car->model->name; ?> <span>|  <?= ucfirst($car->type->{'name_'.$lang}); ?></span></h3>
                                            <div class="features">
                                                <p><?= $this->Html->image('icons/passengers.png')?>
                                                <?= $car->passengers; ?> <?= __('passengers') ?></p>
                                                <?php if ($car->auto == 1): ?>
                                                    <p>
                                                        <?= $this->Html->image('icons/autotransmission.png')?> 
                                                        <?= __('automatic transmission') ?>
                                                    </p>
                                                <?php endif ?>
                                                <?php if ($car->airconditioning): ?>
                                                    <p>
                                                        <?= $this->Html->image('icons/airconditioning.png')?> 
                                                        <?= __('air conditioning') ?>
                                                    </p>
                                                    
                                                <?php endif ?>
                                                <p>
                                                    <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                                                </p>
                                            </div>
                                            <?php if (!empty($car->{'details_'.$lang})): ?>
                                                <div class="details">
                                                    <div class="view-details">[+] <?= __('View car details'); ?></div>
                                                    <div class="close-details">[-] <?= __('Close car details'); ?></div>
                                                    <span class="details-more">
                                                        <?= $car->{'details_'.$lang} ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                        </div>                              
                                    </div>
                                    <div class="additional-block">
                                        <table>
                                            <tr>
                                                <td class="title">
                                                    <?= __('Pick-up Date') ?>
                                                </td>
                                                <td>:</td>
                                                <td class="val">
                                                <?php 
                                                $from_date = ($lang == 'fr') ? $reservation->from_date->i18nFormat('yyyy-MM-dd') : $reservation->from_date->i18nFormat('dd-MM-yyyy') ; 

                                                ?>
                                                <?= $from_date.' '.__('At').' '.$reservation->from_time->i18nFormat('HH:mm') ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title">
                                                    <?= __('Drop-off Date') ?>   
                                                </td>
                                                <td>:</td>
                                                <td class="val">
                                                <?php 
                                                $to_date = ($lang == 'fr') ? $reservation->to_date->i18nFormat('yyyy-MM-dd') : $reservation->to_date->i18nFormat('dd-MM-yyyy') ; 
                                                ?>
                                                
                                                    <?= $to_date.' '.__('At').' '.$reservation->to_time->i18nFormat('HH:mm'); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title"><?= __('Pick-up Location') ?></td>
                                                <td>:</td>
                                                <td class="val">
                                                    <?= $reservation->pickup_location; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="title"><?= __('Drop-off Location') ?></td>
                                                <td>:</td>
                                                <td class="val">
                                                    <?= $reservation->return_location; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>                                  
                            </div>
                            <div class="clear"></div>
                        </div>
                <?php endforeach; ?>
            </div>
            <h4>
                <?= __('Extras Informations') ?>
            </h4>
            <div id="extrasinfo" class="widget main-widget product-widget">
                <?php foreach ($reservation->extras as $extra) :?>
                    <div class="post">
                            <div class="main-block_container">
                                <div class="additional-block_container" style="background: none;">
                                    <div class="main-block">                                    
                                        <div class="product-img extra">
                                            <?= $this->Html->image("extras/".$extra->img,['class'=>'grow']);?>
                                        </div>
                                        <div class="product-info">
                                            <h3 class="entry-format"><?= $extra->{'name_'.$lang} ?></h3>
                                            <div class="features">

                                            </div>
                                            <?php if (!empty($extra->{'details_'.$lang})): ?>
                                                <div class="details">
                                                    <div class="view-details">[+] <?= __('View car details'); ?></div>
                                                    <div class="close-details">[-] <?= __('Close car details'); ?></div>
                                                    <span class="details-more">
                                                        <?= $extra->{'details_'.$lang} ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                        </div>                              
                                    </div>
                                </div>                                  
                            </div>
                            <div class="clear"></div>
                        </div>
                <?php endforeach; ?>
            </div>
            <h4>
                <?= __('Request Status') ?>
            </h4>
            <center>
                <div id="status">
                    <?php if ($reservation->confirmed == 1) {

                        echo __('We\'re happy to inform you that your request has been Confirmed, you will be contacted by phone or email to complete the process');
                    }elseif ($reservation->rejected == 1) {
                        echo __('We\'re very sorry to inform you that your request could not be confirmed, please feel free to'). $this->Html->link(__('choose from our cars'),['controller'=>'Cars','action'=>'index']).('and make another request');
                    }else{
                        echo __('Your request has not been reviewed yet, you will be contacted once an agent has confimed your request');
                    } ?>
                </div>
            </center>
        <?php else: ?>
            <center style="font-size: 14px;">
            <?= '<span class="notice">'.__('Notice').'</span> : '.__('The code is an alphanumerical case sensitive string, please make sur to write it correctlly') ?>
            </center>
        <?php endif; ?>
    </div>
</div>
<style type="text/css">
.notice{
    font-weight: bold;
    text-decoration: underline;
}
input.input_placeholder {
    padding-left: 15px;
    width: 170px;
}

.product-widget .main-block{
    float: none;
}

.product-widget .additional-block{
    padding: 0px 0px 0px 0px;
    width: 100%;
}
table{
    text-align: left;
    margin-left: 5px;
}
td {
    padding-left: 3px;
    padding-bottom: 10px;
    padding-right: 3px;
}
#carinfo{
    width: 90%; 
    margin-left: 5%;
}
#extrasinfo {
    width: 90%; 
    margin-left: 5%;
}

#guestinfo {
    margin-left: 5%;
    font-size: 30px;
    font-family: fantasy;
}
.extra img{
    max-width: 100px;
}

.main-block_container {
    width: 453px;
}
h4 {
    width: 90%;
    margin-left: 3%;
    margin-top: 35px;
    margin-bottom: 15px;
    font-size: 14px !important;
}

.title {
    font-size: 15px;
    font-weight: bold;
}
.val {
    font-size: 15px;
}

#status{
    font-size: 30px;
    width: 80%;
    font-family: initial;
    margin-top: 20px;
    margin-bottom: 40px;
    line-height: 1.2;
}
</style>