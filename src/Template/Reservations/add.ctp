<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>

<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;">
    	<center>
            <?= $this->Html->image('happy.jpg',['style'=>'width: 170px;margin-top: 30px;margin-bottom: 30px;']); ?>
        </center>
        <center class="txt">
            <?= __('Your request has been successfully submitted and we will contact you once your request is processed') ?>
        </center>
        <center class="txt">
            <?= __('You can check the status of your request at any time using the code bellow:') ?>
        </center>
        <br><br>
        <center>
            <span class="code"><?= $rescode; ?></span>
        </center>
        <br><br>
        <center class="txt">
            <?= __('Please be warrned that the request is not confirmed until you get a call or an email from us.') ?>
        </center>
        <center class="txt">
            <?= __('You can use').' '.$this->Html->link(__('this link'),['controller'=>'Reservations', 'action'=>'status']).' '.__('to check the status of your request or take a look at our').' '.$this->Html->link(__('available cars'),['controller'=>'Cars', 'action'=>'index']) ?>
        </center>
        <br><br>
    </div>
</div>

<style type="text/css">
    .code{
        font-size: 50px;
        font-weight: bold;
        color: #000;
    }
    .txt{
        font-size: 15px;
    }
</style>