<?php $this->assign('title', $title); ?>
<?php //print_r($this->request->query['status']); die();?>
<?php $status = (isset($this->request->query['status'])) ? $this->request->query['status'] : 3;?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Reservation Rejetées</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Réservations Rejetées
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <br/>
                <?php if (isset($status) && $status==0): ?>
                    <div class="alert alert-danger alert-dismissable" id="msgErr">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <div style="font-weight: bold;">
                            Une Erreur est survenu lors de la mise à jour, veuillez reessayer
                        </div>
                    </div>
                <?php elseif(isset($status) && $status==1): ?>
                    <div class="alert alert-success alert-dismissable" id="msgSuccess">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <div style="font-weight: bold;">
                            La Réservation a était mise à jour avec succes
                        </div>
                    </div>
                <?php endif; ?>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-reservations">
                        <thead>
                            <tr>
                                <th>Voiture</th>
                                <th>Extras</th>
                                <th>Du</th>
                                <th>Au</th>
                                <th>Etat</th>
                                <th>Modifier</th>
                                <th>Annuler</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($reservations as $reservation) :?>
                            <tr class="">
                                <td><?= $this->Html->image('cars/'.$reservation->cars[0]->medias[0]->name,['style'=>'width: 110px']); ?></td>
                                <td style="text-align: left;">
                                    <?php if (!empty($reservation->extras)): ?>
                                        <ul style="margin-left: -20px;">
                                            <?php foreach ($reservation->extras as $extra) :?>
                                                <li><?= $extra->name_fr; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif ?>
                                    
                                </td>
                                <td>
                                    <?= $reservation->from_date->i18nFormat('dd-MM-yyyy').' à '.$reservation->from_time->i18nFormat('HH:mm');?>
                                </td>
                                <td>
                                    <?= $reservation->to_date->i18nFormat('dd-MM-yyyy').' à '.$reservation->to_time->i18nFormat('HH:mm');?>
                                </td>
                                <td>
                                    <?php if ($reservation->canceled == 1) {
                                        echo "<b style='color: red'>Annulé</b>";
                                    }else{
                                        echo "<b style='color: green'>Confirmé</b>";
                                        } ?>
                                </td>
                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editReservation<?= $reservation->id ?>"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Car -->
                                    <div class="modal fade" id="editReservation<?= $reservation->id ?>" tabindex="-1" role="dialog" aria-labelledby="editReservationLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editCarLabel">
                                                        Edition des informations de la Réservation
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $reservation->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $reservation->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create($reservation,['name'=>'ReservationForm'.$reservation->id,'url' => ['action' => 'edit']]) ?>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('from_date',[
                                                            'label' => 'Date Debut',
                                                            'class'=>'form-control dte',
                                                            'placeholder'=>'Date Debut',
                                                            'type'=>'text',
                                                            'value'=>$reservation->from_date->i18nFormat('yyyy-MM-dd')
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('from_time',[
                                                            'label' => 'Temps Debut',
                                                            'class'=>'form-control time',
                                                            'placeholder'=>'Temps Debut',
                                                            'type'=>'text',
                                                            'value'=>$reservation->from_time->i18nFormat('HH:mm')
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('to_date',[
                                                            'label' => 'Date Fin',
                                                            'class'=>'form-control dte',
                                                            'placeholder'=>'Date Fin',
                                                            'type'=>'text',
                                                            'value'=>$reservation->to_date->i18nFormat('yyyy-MM-dd')
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('to_time',[
                                                            'label' => 'Temps Fin',
                                                            'class'=>'form-control time',
                                                            'placeholder'=>'Temps Fin',
                                                            'type'=>'text',
                                                            'value'=>$reservation->to_time->i18nFormat('HH:mm')
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('pickup_location',[
                                                            'label' => 'Emplacement de Prise',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Emplacement de Prise',
                                                            'type'=>'text'
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('return_location',[
                                                            'label' => 'Emplacement de Retour',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Emplacement de Retour',
                                                            'type'=>'text'
                                                        ]) ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <label>Extras <i class="fa fa-plus addextra" data-resid="<?= $reservation->id; ?>"></i></label>
                                                        <ul id="extralist<?= $reservation->id; ?>">
                                                        <?php foreach ($reservation->extras as $extra):?>
                                                            <li>
                                                                <?= $this->Form->hidden('extras[]',['value'=>$extra->id])?>
                                                                <?= $extra->name_fr;?>
                                                                <i class="fa fa-times removextra"></i>
                                                            </li>
                                                        <?php endforeach; ?>
                                                        </ul>
                                                        <div id="choosextra<?= $reservation->id; ?>" style="display: none;">
                                                            <?= $this->Form->select('extra_id',
                                                                $extras,
                                                                ['class'=>'form-control',
                                                                'empty' => '--',
                                                                'style'=>'width: 300px; display: inline-block'
                                                                ]
                                                            ) ?>
                                                            <button type="button" class="btn btn-success" style="margin-left: 15px;" data-resid="<?= $reservation->id; ?>">
                                                                Ajouter
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">
                                                        Modifier
                                                    </button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                                <td>
                                <?php
                                    $msg = 'Confirmer?';
                                    $confirm = 'Etes Vous sur de vouloir confirmer cette réservation?';
                                    $class = 'warning';

                                    echo $this->Html->link($msg,['action'=>'confirm',$reservation->id],
                                                            ['confirm' => $confirm, 
                                                             'class'=>'btn btn-'.$class
                                                             ]);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $(document).ready(function() {
        $('#dataTables-reservations').DataTable({
                responsive: true
        });
        $('.dte').attr('type','date');
        $('.time').attr('type','time');
    });
    $(document).on('click','.addextra', function(){
        var resid = $(this).data('resid');
        $('#choosextra'+resid).show();
        $(".modal-body").scrollTop($(".modal-body")[0].scrollHeight);
    });

    $(document).on('click','.btn-success', function(){
        var resid = $(this).data('resid');
        var extra = $('#choosextra'+resid).find('select[name=extra_id] option:selected');
        $('#extralist'+resid).append('<li>'
                                        +'<input type="hidden" name="extras[]" value="'+extra.val()+'">'
                                        +''+extra.text()+''
                                        +'<i class="fa fa-times removextra"></i>'
                                    +'</li>');
        $('#choosextra'+resid).hide();
        $('#choosextra'+resid).find('select').prop('selectedIndex',0);
    });

    $(document).on('click','.removextra', function(){
        $(this).parent().remove();
    });

    var msgbox = $('#msgSuccess');
    if (msgbox) {
        setTimeout(function(){
            msgbox.fadeOut('slow');
        }, 2500);
    }
</script>
<style type="text/css">
    .addextra{
        color: blue;
        font-size: 14px;
    }
    .removextra{
        color: red;
        font-size: 14px;
        margin-left: 8px;
    }
    .removextra:hover, .addextra:hover{
        font-size: 18px;
        cursor: pointer;
    }
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
        text-align: left;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: 60%;
    }
    td{
        padding: 2px !important;
    }
    .form-group{
        margin-bottom: 15px !important;;
    }
</style>