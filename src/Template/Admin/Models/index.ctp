<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Modeles</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Modeles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModel" style="float: right; margin-bottom: 25px;">
                    Ajouter un Modele
                </button>
                <br/>
                <!-- Modal Add Model-->
                <div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="editModelLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addModelLabel">
                                    Ajouter un Nouveau Modele
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create($modelEntity,['name'=>'ModelForm','url' => ['action' => 'add']]) ?>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('name',[
                                        'label' => 'Modele',
                                        'class'=>'form-control',
                                        'placeholder'=>'Modele'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Marque') ?>
                                    </div>
                                    <?= $this->Form->select('brand_id',
                                        $brands,
                                        ['class'=>'form-control',
                                        'empty' => '--'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Annee')?>
                                    </div>
                                    <?= $this->Form->select('year',$range,
                                    ['class'=>'form-control','empty' => '--']) ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="button" class="btn btn-primary" onclick="validateForm('')">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal Add Model -->
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-models">
                        <thead>
                            <tr>
                                <th>Marque</th>
                                <th>Model</th>
                                <th>Annee</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($models as $model) :?>
                            <tr class="">
                                <td><?= $model->brand->name; ?></td>
                                <td><?= $model->name; ?></td>
                                <td><?= $model->year; ?></td>
                                <td class="center">

                                <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editModel<?= $model->id ?>" onclick="clearform();"><i class="fa fa-pencil"></i>
                                </button>
                                <!-- Modal Edit Model -->
                                <div class="modal fade" id="editModel<?= $model->id ?>" tabindex="-1" role="dialog" aria-labelledby="editModelLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="editModelLabel">
                                                    Edition des informations du Model
                                                </h3>
                                            </div>
                                            <div class="modal-body">
                                                <div class="alert alert-danger alert-dismissable" id="msgErr<?= $model->id ?>" style="display: none;">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <div style="font-weight: bold;">
                                                        
                                                    </div>
                                                </div>
                                                <div class="alert alert-danger" id="err-msg<?= $model->id ?>" style="display: none;">
                                                    Veuillez remplir les champs vide
                                                </div>
                                                <?= $this->Form->create($model,['name'=>'ModelForm'.$model->id,'url' => ['action' => 'edit']]) ?>
                                                <div class="form-group" style="display: block;">
                                                <?= $this->Form->input('name',[
                                                    'label' => 'Modele',
                                                    'class'=>'form-control',
                                                    'placeholder'=>'Modele'
                                                    ]
                                                ) ?>
                                            </div>
                                            <br/>
                                            <div class="form-group" style="display: block;">
                                                <div>
                                                    <?= $this->Form->label('Marque') ?>
                                                </div>
                                                <?= $this->Form->select('brand_id',
                                                    $brands,
                                                    ['class'=>'form-control',
                                                    'empty' => '--'
                                                    ]
                                                ) ?>
                                            </div>
                                            <br/>
                                            <div class="form-group" style="display: block;">
                                                <div>
                                                    <?= $this->Form->label('Annee')?>
                                                </div>
                                                <?= $this->Form->select('year',$range,
                                                ['class'=>'form-control','empty' => '--']) ?>
                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                <button type="button" class="btn btn-primary" onclick="validateForm('<?= $model->id ?>')">Modifier</button>
                                            </div>
                                            <?= $this->Form->end() ?>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /Modal Edit Model -->
                                </td>
                                <td class="center">
                                    <?php 
                                    echo $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Models', 'action' => 'delete', $model->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer ce modele ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script>
    $(document).ready(function() {
        $('#dataTables-models').DataTable({
                responsive: true
        });
        tinymce.init({
          selector: 'textarea',
          language: 'fr_FR'
        });
    });

      function validateForm(id){
            var fullformname = 'ModelForm'+id;
            var errClass = "#err-msg"+id;
            $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
            $(errClass).hide();

            var cpt = 0;
            $.each($('form[name='+fullformname+']').find('.form-control'), function(){
                //console.log($(this).val());
                if (!$(this).val()) {
                    $(this).addClass("validation");
                    cpt++;
                }
            })

            if (cpt == 0) {
                $(errClass).hide();
                $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
                var name = $('form[name='+fullformname+']').find("input[name=name]").val();
                var brand_id = $('form[name='+fullformname+']').find("select[name=brand_id]").val();
                var year = $('form[name='+fullformname+']').find("select[name=year]").val();
                $.post( "/admin/models/exists/"+id, {name: name, brand_id: brand_id, year: year })
                    .done(function( data ) {
                        $("#msgErr"+id+" div").empty();
                        $("#msgErr"+id).hide();
                        if (data == "ok") {
                            $('form[name='+fullformname+']').submit();
                        }else{
                              $("#msgErr"+id+" div").append("Ce model exist deja, veuillez verifier les données");
                              $("#msgErr"+id).show('slow');
                        }
                });
            }else{
                $(errClass).show('slow');
            }
        }

        function clearform(){
            $(".alert-dismissable div").empty();
            $(".alert-dismissable").hide();
            $("input, select").removeClass("validation");
        }

</script>
<style type="text/css">

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>