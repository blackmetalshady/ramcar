<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Texte de la page d'accueil</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des textes d'accueil
            </div>
            <div class="panel-body">
        		<div class="panel-group" id="accordion">
	        	<?php foreach ($hometexts as $ht) :?>
					<div class="panel panel-primary">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $ht->position; ?>">
						        <span>Position <strong><?= $ht->position; ?> : </strong></span>
						        <?= $ht->title_fr; ?>
					        </a>
					        <i class="fa fa-pencil" aria-hidden="true" style="float: right; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#editText<?= $ht->position; ?>"></i>
					        <!-- Modal Add Car-->
			                <div class="modal fade" id="editText<?= $ht->position; ?>" tabindex="-1" role="dialog" aria-labelledby="editTextLabel<?= $ht->position; ?>" aria-hidden="true">
			                    <div class="modal-dialog">
			                        <div class="modal-content"  style="color: black;">
			                            <div class="modal-header">
			                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                                <h3 class="modal-title" id="addCarLabel">
			                                    Edition du Texte
			                                </h3>
			                            </div>
			                            <div class="modal-body">
			                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
			                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                                    <div style="font-weight: bold;">
			                                        
			                                    </div>
			                                </div>
			                                <div class="alert alert-danger" id="err-msg" style="display: none;">
			                                    Veuillez remplir les champs vide
			                                </div>
			                                <?= $this->Form->create($ht,['name'=>'TextForm','url' => ['action' => 'edit']]) ?>
			                                <div class="form-group" style="display: block;">
			                                    <?= $this->Form->input('title_fr',[
			                                        'label' => 'Titre Français',
			                                        'class'=>'form-control',
			                                        'placeholder'=>'Titre Français'
			                                        ]
			                                    ) ?>
			                                </div>
			                                <div class="form-group" style="display: block;">
			                                    <?= $this->Form->input('title_en',[
			                                        'label' => 'Titre Anglais',
			                                        'class'=>'form-control',
			                                        'placeholder'=>'Titre Anglais'
			                                        ]
			                                    ) ?>
			                                </div>
			                                <br/>
			                                <div class="form-group" style="display: block;">
			                                	<?= $this->Form->input('content_fr',[
			                                        'label' => 'Contenu Français',
			                                        'class'=>'form-control',
			                                        'placeholder'=>'Contenu Français'
			                                        ]
			                                    ) ?>
			                                </div>
			                                <div class="form-group" style="display: block;">
			                                	<?= $this->Form->input('content_en',[
			                                        'label' => 'Contenu Anglais',
			                                        'class'=>'form-control',
			                                        'placeholder'=>'Contenu Anglais'
			                                        ]
			                                    ) ?>
			                                </div>
			                            </div>
			                            <div class="modal-footer">
			                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			                                <button type="submit" class="btn btn-primary" onclick="">Modifier</button>
			                            </div>
			                            <?= $this->Form->end() ?>
			                        </div>
			                        <!-- /.modal-content -->
			                    </div>
			                    <!-- /.modal-dialog -->
			                </div>
			                <!-- /.modal Add Car -->
					      </h4>					      
					    </div>
					    <div id="collapse<?= $ht->position; ?>" class="panel-collapse collapse">
					        <div class="panel-body">
					      		<?= $ht->content_fr; ?>
					        </div>
					    </div>
					</div>
				<?php endforeach; ?>
				</div>
        	</div>
        </div>
    </div>
</div>



