<?php $this->assign('title', $title); ?>
<?php //print_r($messages->toArray()); die();?>
<?php $status = (isset($this->request->query['status'])) ? $this->request->query['status'] : 3;?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des nouveaux Messages</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des nouveaux Messages
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <br/>
                <?php if (isset($status) && $status==0): ?>
                    <div class="alert alert-danger alert-dismissable" id="msgErr">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <div style="font-weight: bold;">
                            Une Erreur est survenu lors de la mise à jour, veuillez reessayer
                        </div>
                    </div>
                <?php elseif(isset($status) && $status==1): ?>
                    <div class="alert alert-success alert-dismissable" id="msgSuccess">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <div style="font-weight: bold;">
                            La Réservation a était mise à jour avec succes
                        </div>
                    </div>
                <?php endif; ?>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-messages">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Client</th>
                                <th>Sujet</th>
                                <th>Message</th>
                                <th>Date</th>
                                <th>Repondre</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($messages as $message) :?>
                            <?php if ($message->fromUser->role != 'admin') :?>
                                <tr class="">
                                    <td><input type="checkbox" name="read[]"></td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#usrinfo<?= $message->id ?>">
                                            <?php if(!empty($message->fromUser)) {
                                                    $fullname = $message->fromUser->firstname.' '.$message->fromUser->lastname;
                                                    $email = $message->fromUser->email;
                                                }else{ 
                                                    $fullname = $message->full_name ;
                                                    $email = $message->email;
                                                } ?>
                                            <?= $fullname; ?>
                                        </a>
                                        <!-- Modal User Info -->
                                        <div class="modal fade" id="usrinfo<?= $message->id ?>" tabindex="-1" role="dialog" aria-labelledby="userLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="modal-title" id="editCarLabel">
                                                            Informations Client
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group" style="display: block;">
                                                        <label class="usr-lbl">Nom Complet</label>
                                                        <span><b>:</b></span>
                                                        <span class="usr-val"><?= $fullname; ?><span>
                                                        </div>
                                                        <div class="form-group" style="display: block;">
                                                            <label class="usr-lbl">E-mail</label>
                                                            <span><b>:</b></span>
                                                            <span class="usr-val">
                                                                <a href="mailto:<?= $message->email; ?>">
                                                                    <?= $email; ?>
                                                                </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /Modal User Info -->
                                    </td>
                                    <td>
                                        <?= $message->subject; ?>
                                    </td>
                                    <td style="text-align: -webkit-center;">
                                        <div style="max-width: 300px;">
                                            <?= $this->Text->truncate($message->message,100,['ellipsis' => '','exact' => false])."... "; ?>
                                            <a href="" data-toggle="modal" data-target="#fullmsg<?= $message->id ?>" data-msgid="<?= $message->id ?>" class="answer">
                                                Lire
                                            </a> 
                                            <?php if (!empty($message->parent_id) || !empty($message->child_messages)) {
                                                $p_id = (!empty($message->parent_id)) ? $message->parent_id : $message->id;
                                                echo ", ";
                                                echo $this->Html->link('Voir la conversation',['controller'=>'Messages','action'=>'conversation', $p_id]);
                                            }?>
                                        <!-- Modal Full Message -->
                                        <div class="modal fade" id="fullmsg<?= $message->id ?>" tabindex="-1" role="dialog" aria-labelledby="userLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="modal-title" style="text-align: left;">
                                                            Message de la part de : <?= $fullname; ?>
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group" style="display: block;text-align: justify;">
                                                            <?= $message->message; ?>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /Modal Full Message -->
                                        </div>
                                    </td>
                                    <td>
                                        <?= $message->sent_at->i18nFormat('dd-MM-yyyy').' à '.$message->sent_at->i18nFormat('HH:mm');?>
                                    </td>
                                    <td>
                                        <a class='btn btn-primary btn-circle'  href="" data-toggle="modal" data-target="#answer<?= $message->id ?>" >
                                            <i class="fa fa-envelope-o"></i>
                                        </a>
                                         <!-- Modal Answer -->
                                        <div class="modal fade" id="answer<?= $message->id ?>" tabindex="-1" role="dialog" aria-labelledby="userLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                <?= $this->Form->create(null, ['url'=>['action'=>'addanswer'], 'class'=>'answerForm']) ?>
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="modal-title" style="text-align: left;">
                                                            Repondre a : <?= $fullname; ?>
                                                        </h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group" style="display: block;">
                                                            <input type="text" name="subject" value="<?= $message->subject; ?>" class="form-control" style="width: 100%;" required>
                                                        </div>
                                                        <div class="form-group" style="display: block;">
                                                            <textarea name="message" class="form-control" style="width: 100%;min-height: 150px;" required></textarea>
                                                            <?php 
                                                            if (!empty($message->parent_id)) {
                                                                $parent = $message->parent_id;
                                                            }else{
                                                                $parent = $message->id;
                                                            } 
                                                            ?>
                                                            <input type="hidden" name="user_id" value="<?= $this->request->session()->read('Auth.User.id'); ?>">
                                                            <input type="hidden" name="parent_id" value="<?= $parent?>">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                        <button type="submit" class="btn btn-default btn-primary">Repondre</button>
                                                    </div>
                                                    <?= $this->Form->end(); ?>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /Modal Answer -->
                                    </td>
                                    <td>
                                    <?php
                                        $msg = '<i class="fa fa-times"></i>';
                                        $confirm = 'Etes Vous sur de vouloir supprimer ce message?';
                                    ?>
                                        <?= 
                                            $this->Html->link($msg,['action'=>'delete',$message->id],
                                                                ['confirm' => $confirm, 
                                                                 'class'=>'btn btn-danger btn-circle',
                                                                 'escape'=>false
                                                                 ]);
                                        ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $(document).ready(function() {
        $('#dataTables-messages').DataTable({
                responsive: true
        });
        $('.dte').attr('type','date');
        $('.time').attr('type','time');
    });

</script>
<style type="text/css">
    .modal-body{
        max-height: 400px;
        text-align: left;
    }
    .modal-body.edit{
        overflow-y: scroll;
    }
    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: 60%;
    }
    .form-group{
        margin-bottom: 15px !important;;
    }

    td{
        padding: 2px !important;
    }
    .usr-lbl, .usr-val{
        min-width: 150px;
        font-size: 16px;
        margin-left: 15px;
    }
</style>
<script type="text/javascript">
    $(document).on('click','.answer', function(){
        var msgid = $(this).data('msgid');
        $.post('/admin/messages/markasread',{id: msgid});
    })
</script>
<script type="text/javascript">
    $(document).on('submit','.answerForm', function(e){
        e.preventDefault();
            subject =  $(this).find('input[name=subject]');
            message =  $(this).find('textarea[name=message]');
            user_id =  $(this).find('input[name=user_id]');
            parent_id =  $(this).find('input[name=parent_id]');

            //console.log(message.val());
        if (!subject.val()) {
            subject.parent().addClass('has-error');
        }else if(!message.val()){
            message.parent().addClass('has-error');
        }else{
            $('input, textarea').parent().removeClass('has-error');
            $.post('/admin/messages/addanswer', {subject: subject.val(), message: message.val(), from_user: user_id.val(), parent_id: parent_id.val()})
                .done(function(data){
                    if(data === 'success'){
                        alert("Message envoyer avec succes");
                    }else{
                        alert("Erreur lors de l'envois du message, veuillez réessayer");
                    }
            });
        }
    });
</script>