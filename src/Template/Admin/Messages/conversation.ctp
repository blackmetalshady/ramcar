<?php $this->assign('title', $title); ?>
<?php //print_r($message->toArray()); die(); ?>
<?php $user_id ='';?>
<?php $status = (isset($this->request->query['status'])) ? $this->request->query['status'] : 3;?>
<br>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php if(!empty($message->fromUser)) {
                    $fullname = $message->fromUser->firstname.' '.$message->fromUser->lastname;
                    $email = $message->fromUser->email;
                    $user_id = $message->fromUser->id;
                }else{ 
                    $fullname = $message->full_name ;
                    $email = $message->email;
                } ?>
                                        
                Votre conversation avec <b>"<?= ucfirst($fullname); ?>"</b>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="<?= ($message->from_user == $this->request->session()->read('Auth.User.id')) ? 'admin' : 'user' ;?>">
                    <h4>
                        <span id="firstmsg-subject"><?= ucfirst($message->subject); ?></span>
                        <span class="sent_at">
                            (<?= 'Le '.$message->sent_at->i18nFormat('dd-MM-yyyy').' à '.$message->sent_at->i18nFormat('HH:mm');?>)
                        </span>
                    </h4>
                    <div><?= ucfirst($message->message); ?></div>
                </div>
                <?php foreach ($message->child_messages as $msg) :?>
                    <div style="float: left; width: 100%;">
                    <div class="<?= ($msg->from_user == $this->request->session()->read('Auth.User.id')) ? 'admin' : 'user' ;?>">
                        <h4>
                            <?= ucfirst($msg->subject); ?>
                            <span class="sent_at">
                                (<?= 'Le '.$msg->sent_at->i18nFormat('dd-MM-yyyy').' à '.$msg->sent_at->i18nFormat('HH:mm');?>)
                            </span>
                        </h4>
                        <div><?= ucfirst($msg->message); ?></div>
                    </div>
                    </div>
                <?php endforeach; ?>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        <table style="margin: 20px auto 20px auto">
            <tr>
                <td><textarea class="form-control" id="msg"></textarea></td>
                <td><button class="btn btn-primary" id="sendbtn">></button></td>
            </tr>
        </table>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<style type="text/css">
.user {
    max-width: 500px;
    text-align: justify;
    background-color: #337ab7;
    color: #fff;
    padding: 10px 15px;
    border-color: #2e6da4;
    border-radius: 10px;
    margin-top: 20px;
}

.admin {
    max-width: 500px;
    float: right;
    text-align: justify;
    background-color: #f1f0f0;
    color: #000;
    padding: 10px 15px;
    border: 1px solid #bdbdbd;
    border-radius: 10px;
    margin-top: 20px;
}
.admin h4, .user h4{
    border-bottom: 1px solid;
    padding-bottom: 3px;
    margin-top: 0px !important;
}
.sent_at{
    font-size: 11px;
}
#sendbtn{
    height: 115%;
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
}
#msg{
    width:500px; height: 90px; 
    border-top-right-radius: 0px;
    border-bottom-right-radius: 0px;
    resize: none;
}
</style>
<script type="text/javascript">
    $(document).on('click', '#sendbtn', function(){
        var msg = $('#msg').val();
        
        if (msg) {
            var subject = 'Re: '+$('#firstmsg-subject').text();
            var currentdate = new Date(); 
            var datetime = currentdate.getDate() + "/"
                            + (currentdate.getMonth()+1)  + "/" 
                            + currentdate.getFullYear() + " à "  
                            + currentdate.getHours() + ":"  
                            + currentdate.getMinutes();
            var parent_id = '<?= $message->id; ?>';
            $.post('/admin/messages/addanswer',{subject: subject, message: msg, parent_id: parent_id, to_user: '<?= $user_id; ?>'})
                .done(function(data){
                    if (data == 'success') {
                        $('.panel-body').append('<div style="float: left; width: 100%;">'
                            +'<div class="admin">'
                                +'<h4>'
                                    +subject
                                    +'<span class="sent_at">'
                                        +' ('+datetime+')'
                                    +'</span>'
                                +'</h4>'
                                +'<div>'+msg+'</div>'
                            +'</div>'
                            +'</div>');
                    }else{
                        alert('Une Erreur s\'est produit, veuillez reessayer');
                    }
                });
        }
    })
</script>