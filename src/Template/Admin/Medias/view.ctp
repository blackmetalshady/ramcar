<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header" style="margin-top: 10px;">
        <span>
            <?= $this->Html->image('cars/logos/'.$car->brand->logo,['style'=>'margin-top: -5px;'])?>
        </span>
        <span>
            <?= $car->brand->name.' '.$car->model->name.' '.$car->model->year."  (".$car->fuel->name_fr.")"; ?>
        </span>
        </h2>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-12">
        <center>
        <?php $img = (empty($medias[0])) ? 'unknown.jpg' : $medias[0]->name ; ?>
        <?= $this->Html->image('cars/'.$img, ['style'=>'width: 50%','class'=>'primary-img']);?></center>
    </div>
    <div class="col-lg-12" style="margin-top: 50px;">
        <?php if(!empty($medias) && sizeof($medias) > 1): ?>
        <?php for ($i=1; $i < sizeof($medias); $i++) : ?>
        <div class="col-lg-3 col-md-6" >
        	<div class="panel car-thumb" style="background: url('<?= '/img/cars/'.$medias[$i]->name ?> ') no-repeat;">
                <div class="actions">
                    <div>
                        <input type="hidden" value="<?= $medias[$i]->id; ?>" class="img_id">
                        <input type="checkbox" name="">
                    </div>
                    <div>
                    <?= 
                        $this->Form->postLink(
                            "<i class='fa fa-remove'></i>", 
                            ['controller' => 'Medias','action' => 'delete', $medias[$i]->id],
                            ['confirm' => 'Etes vous sur de vouloir supprimer cette image ?',
                             'escape' => false, 
                             'title' => 'Delete'] 
                        );
                    ?>
                    </div>
                </div>
    	    </div>
        </div>
        <?php endfor; ?>
        <?php endif; ?>
        <div class="col-lg-3 col-md-6" >
            <div class="panel add-pic">
                <?= $this->Form->create(null, ['type' => 'file', 'id'=>'imgup' ,'url' => ['action' => 'upload']]);?>
                <?= $this->Form->hidden('car_id',['value'=>$car->id]); ?>
                <center style="margin-top: 25%;">
                    <div class="fileUpload btn btn-primary">
                        <span class="fa fa-plus"></span>
                        <?= $this->Form->input('img', [
                            'type' => 'file',
                            'accept'=>'image/*',
                            'class'=>'upload',
                            'onchange'=>'$("#imgup").submit();',
                            'label'=>false
                        ]);?>
                    </div>
                </center>
                <?= $this->Form->end();?>
                <center style="margin-top: -5px;">
                    <div id="status" style="display: none;"></div>
                </center>
                <div id="progress" >
                    <div id="bar"></div>
                    <center style="position: relative;"><div id="percent">0%</div></center>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<script type="text/javascript">
    $(document).ready(function() {
        $(".car-thumb").hover(
            function() {
                $( this ).find(".actions").show('slide', {direction: 'right'}, 300);;
            }, 
            function() {
                $( this ).find(".actions").hide('slide', {direction: 'right'}, 300);;
            }
        );

        var bar = $('#bar');
        var percent = $('#percent');
        var status = $('#status');
        var progress = $("#progress");

        $('#imgup').ajaxForm({
            beforeSend: function() {
                status.empty();
                progress.show();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                if (xhr.responseText == 'success') {
                    status.append('<img src="/img/success.png" width="30"/>');
                }else{
                    status.append('<img src="/img/fail.png" width="30"/>');
                }
                status.fadeIn();
                setTimeout(function(){location.reload()},1000);

            }
        });
        $( "input[type=checkbox]" ).on( "click", function(){
            if($( this ).is(':checked')){
                var img_id = $( this ).parent().find('.img_id').val();
                var car_id = "<?= $car->id ?>";
                $.post( "/admin/medias/setprimary", { id: img_id, primary_img: 1, car_id: car_id })
                  .done(function( data ) {
                    console.log(data);
                    if (data == "ok") {
                        location.reload();
                    }else{
                        $("#err-msg").show('slow');
                    }
                  });
            }
        });
    });
</script>