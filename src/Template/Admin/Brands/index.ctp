<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Marques</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Marques
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <br/>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-brands">
                        <thead>
                            <tr>
                                <th>Logo</th>
                                <th>Marque</th>
                                <th>Activer/Désactiver</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($brands as $brand) :?>
                            <tr class="">
                                <td><?= $this->Html->image('cars/logos/'.$brand->logo); ?></td>
                                <td style="font-size: 18px;"><?= $brand->name ?></td>
                                <td class="center">
                                    <?php 
                                    if ($brand->disabled == 0) {
                                        $msg = "Activé";
                                        $confirm = "Désactivé";
                                        $btn = "primary";
                                        $action = "disable";
                                    }else {
                                        $msg = "Désactivé";
                                        $confirm = "Activé";
                                        $btn = "danger";
                                        $action = "enable";
                                    }
                                    echo $this->Html->link(
                                        $msg,
                                        ['controller' => 'Brands', 'action' => $action, $brand->id],
                                        ['confirm' => 'Etes vous sur de vouloir '.$confirm.' cette marque?','class'=>'btn btn-'.$btn,'escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $(document).ready(function() {
        $('#dataTables-brands').DataTable({
                responsive: true
        });
    })
</script>