<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion du Carousel d'Acceuil</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<?php if (!empty($cars) && sizeof($carousels[0]->cars) < 6) : ?>
		<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addCarToCarousel" style="float: right; margin-bottom: 25px;">
            Ajouter une Voiture au Carousel
        </button>
        <!-- Modal Add Car-->
        <div class="modal fade" id="addCarToCarousel" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="addCarLabel">
                            Ajouter une Nouvelle Voiture au Carousel
                        </h3>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <div style="font-weight: bold;">
                                
                            </div>
                        </div>
                        <div class="alert alert-danger" id="err-msg" style="display: none;">
                            Veuillez remplir les champs vide
                        </div>
                        <?= $this->Form->create($slidercarEntity,['name'=>'AddCarToCarouselForm','url' => ['controller'=>'Slidercars','action' => 'add']]) ?>
                        <div class="form-group" style="display: block;">
                            <div>
                                <?= $this->Form->label('Voiture') ?>
                            </div>
                            <?= $this->Form->select('car_id',
                                $cars,
                                ['class'=>'form-control',
                                'required'=>'required',
                                'empty' => '(Choisissez une voiture a ajouter au carousel)'
                                ]
                            ) ?>
                            <?= $this->Form->hidden('slider_id',['value'=>$carousels[0]->id]);?>
                            <?php $order = sizeof($carousels[0]->cars)+1 ?>
                            <?= $this->Form->hidden('cars_order',['value'=>$order]);?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal Add Car -->
        <br/><br/><br/>
    	<?php endif; ?>
    	<?= $this->Form->hidden('s_id',['value'=>$carousels[0]->id]);?>
		<ul id="sortable">
		  <?php foreach ($carousels[0]->cars as $car) : ?>
		  	<li class="sortable-li" id="<?= $car->_joinData->cars_order; ?>">
		  		<div class='thumb-div'>
		  		<?= $this->Html->image('cars/'.$car->medias[0]->name,['class'=>'thumb-img']) ?>
		  		</div>
		  		<div class="sortable-data">
		  			<div class="data">
		  				<span>
		  					<?= $this->Html->image('cars/logos/'.$car->brand->logo,
		  						['style'=>'width: 35px;']
		  					); ?>
		  				</span>
		  					<h4 class="h4-carname">
		  						<?= $car->brand->name." ".$car->model->name." (".$car->model->year.")"; ?>
		  					</h4>
		  				<h5>
		  					Carburant: <b><?= $car->fuel->name_fr ?></b>
		  				</h5>
		  				<h5>
		  					Matricule: <b><?= $car->registration ?></b>
		  				</h5>
		  			</div>
		  			<?= $this->Form->postLink(
                        "<i class='fa fa-remove remove'></i>", 
                        ['controller' => 'Slidercars','action' => 'delete', $car->_joinData->id ],
                        ['confirm' => 'Etes vous sur de vouloir supprimer cette Voiture du Carousel ?',
                         'escape' => false, 
                         'title' => 'Supprimer']);
                    ?>
		  		</div>
		  	</li>
		  <?php endforeach; ?>
		</ul>
	</div>
</div>
<script type="text/javascript">
	$(function() {
	    $( "#sortable" ).sortable({
	    	update: function (event, ui) {
		        var data = $(this).sortable('toArray');
		        //console.log(data);
		        // POST to server using $.post or $.ajax
		        $.post('/admin/Slidercars/updateorder',{sortable: data, slider_id: $("input[name=s_id]").val()}).done(function(data){
		        	console.log(data);
		        })
		        /*$.ajax({
		            data: {data, slider_id: $("input[name=s_id]").val()},
		            type: 'POST',
		            url: '/admin/Slidercars/updateorder'
		        });*/
		    }
	    }).disableSelection();
	  });
</script>
<style type="text/css">
    .modal-body .form-group div label{
        float: left;
        min-width: 80px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
</style>