<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header" style="margin-top: 10px;">
        Slider d'Acceuil
        </h2>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-12">
        <center>
            <div class="car-thumb" style="background: url('<?= '/img/sliders/'.$homeslider->medias[0]->name ?> ') no-repeat; width: 470px; height: 220px; padding-right: 7px;">
                <div class="actions">
                    <input type="hidden" value="<?= $homeslider->medias[0]->id; ?>" class="img_id">
                    <div>
                        <i class="fa fa-pencil" data-toggle="modal" data-target="#updateFrontImg" style="font-size: 20px; padding-top: 4px; cursor: pointer;"></i>
                    </div>
                </div>
            </div>
        </center>
    </div>
    <div class="col-lg-12" style="margin-top: 50px;">
        <div class="alert alert-warning">
           <center>
                Il est preferable que les images soit des dimension <b>1400x350</b> au format <b>JPG</b> pour une bonne qualitee
           </center>
        </div>
    </div>
    <div class="col-lg-12">
        <?php if(!empty($homeslider->medias) && sizeof($homeslider->medias) > 1): ?>
        <?php for ($i=1; $i < sizeof($homeslider->medias); $i++) : ?>
        <div class="col-lg-3 col-md-6" >
        	<div class="panel car-thumb" style="background: url('<?= '/img/sliders/'.$homeslider->medias[$i]->name ?> ') no-repeat;">
                <div class="actions">
                    <input type="hidden" value="<?= $homeslider->medias[$i]->id; ?>" class="img_id">
                    <div>
                    <?= 
                        $this->Form->postLink(
                            "<i class='fa fa-remove'></i>",['controller' => 'Medias','action' => 'delete', 
                            $homeslider->medias[$i]->id,'slider'],
                            ['confirm' => 'Etes vous sur de vouloir supprimer cette image ?',
                             'escape' => false, 
                             'title' => 'Delete'] 
                        );
                    ?>
                    </div>
                </div>
    	    </div>
        </div>
        <?php endfor; ?>
        <?php endif; ?>
        <?php if (sizeof($homeslider->medias) < 5) :?>
        <div class="col-lg-3 col-md-6" >
            <div class="panel add-pic">
                <?= $this->Form->create(null, ['type' => 'file', 'id'=>'imgup' ,'url' => ['controller'=>'Medias','action' => 'upload']]);?>
                <?= $this->Form->hidden('slider_id',['value'=>$homeslider->id]); ?>
                <?= $this->Form->hidden('type',['value'=>'slider']); ?>
                <center style="margin-top: 25%;">
                    <div class="fileUpload btn btn-primary">
                        <span class="fa fa-plus"></span>
                        <?= $this->Form->input('img', [
                            'type' => 'file',
                            'accept'=>'image/*',
                            'class'=>'upload',
                            'onchange'=>'$("#imgup").submit();',
                            'label'=>false
                        ]);?>
                    </div>
                </center>
                <?= $this->Form->end();?>
                <center style="margin-top: -5px;">
                    <div id="status" style="display: none;"></div>
                </center>
                <div id="progress" >
                    <div id="bar"></div>
                    <center style="position: relative;"><div id="percent">0%</div></center>
                </div>
                
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<!-- /.row -->
<!-- Modal Update Front Image-->
<div class="modal fade" id="updateFrontImg" tabindex="-1" role="dialog" aria-labelledby="updateFrontImgLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title" id="addCarLabel">
                    Modifier l'image d'Acceuil
                </h3>
            </div>
            <div class="modal-body">
                <center>
                    <div class="panel add-pic" style="width: 206px">
                        <?= $this->Form->create(null, ['type' => 'file', 'id'=>'homeimgup' ,'url' => ['controller'=>'Medias','action' => 'upload']]);?>
                        <?= $this->Form->hidden('slider_id',['value'=>$homeslider->id]); ?>
                        <?= $this->Form->hidden('type',['value'=>'slider']); ?>
                        <?= $this->Form->hidden('frontimg',['value'=>'frontimg']); ?>
                        <center style="margin-top: 25%;">
                            <div class="fileUpload btn btn-primary">
                                <span class="fa fa-plus"></span>
                                <?= $this->Form->input('img', [
                                    'type' => 'file',
                                    'accept'=>'image/png',
                                    'class'=>'upload',
                                    'onchange'=>'$("#homeimgup").submit();',
                                    'label'=>false
                                ]);?>
                            </div>
                        </center>
                        <?= $this->Form->end();?>
                        <center style="margin-top: -5px;">
                            <div id="homestatus" style="display: none;"></div>
                        </center>
                        <div id="homeprogress" >
                            <div id="homebar"></div>
                            <center style="position: relative;"><div id="homepercent">0%</div></center>
                        </div>
                    </div>
                </center>
                <div class="alert alert-warning">
                    N.B : l'image doit etre au format PNG et doit etre des dimension 470x220 avec arriere plan transparent
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
            <?= $this->Form->end() ?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal Update Front Image -->
<!-- /.col-lg-12 -->
<div class="col-lg-12">
    <div style="max-width: 70%;">
          <?= $this->Form->create($homeslider, ['url'=>['controller'=>'Sliders','action' => 'edit']]);?>
          <br/>
          <?= $this->Form->input('title_fr',[
                                    'label'=>'Titre Francais',
                                    'class'=>'form-control',
                                    'style'=>'display: inline;',
                                    'required'=>'required'
                                    ]) ?>
          <br/>
          <?= $this->Form->input('title_en',[
                                    'label'=>'Titre Anglais',
                                    'class'=>'form-control',
                                    'required'=>'required'
                                    ]) ?>
          <br/>
          <?= $this->Form->input('description_fr',[
                                    'label'=>'Description Francais',
                                    'class'=>'form-control',
                                    'type'=>'textarea',
                                    'required'=>'required'
                                ]) ?>
          <br/>
          <?= $this->Form->input('description_en',[
                                    'label'=>'Description Anglais',
                                    'class'=>'form-control',
                                    'type'=>'textarea',
                                    'required'=>'required'
                                ]) ?>
          <br/>
          <?= $this->Form->submit('Enregistrer', ['class'=>'btn btn-primary btn-lg btn-block']); ?> 
          <br/>
          <?= $this->Form->end() ?>
      </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".car-thumb").hover(
            function() {
                $( this ).find(".actions").show('slide', {direction: 'right'}, 300);;
            }, 
            function() {
                $( this ).find(".actions").hide('slide', {direction: 'right'}, 300);;
            }
        );

        var bar = $('#bar');
        var percent = $('#percent');
        var status = $('#status');
        var progress = $("#progress");

        $('#imgup').ajaxForm({
            beforeSend: function() {
                status.empty();
                progress.show();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function(xhr) {
                if (xhr.responseText == 'success') {
                    status.append('<img src="/img/success.png" width="30"/>');
                }else{
                    status.append('<img src="/img/fail.png" width="30"/>');
                }
                status.fadeIn();
                setTimeout(function(){location.reload()},1500);
            }
        });

        var homebar = $('#homebar');
        var homepercent = $('#homepercent');
        var homestatus = $('#homestatus');
        var homeprogress = $("#homeprogress");

        $('#homeimgup').ajaxForm({
            beforeSend: function() {
                homestatus.empty();
                homeprogress.show();
                var percentVal = '0%';
                homebar.width(percentVal);
                homepercent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                homebar.width(percentVal);
                homepercent.html(percentVal);
            },
            complete: function(xhr) {
                if (xhr.responseText == 'success') {
                    homestatus.append('<img src="/img/success.png" width="30"/>');
                }else{
                    homestatus.append('<img src="/img/fail.png" width="30"/>');
                }
                homestatus.fadeIn();
                setTimeout(function(){location.reload()},1500);
            }
        });

        tinymce.init({
          selector: 'textarea',
          language: 'fr_FR',
          onSubmit: function(f){
            alert(f.get('tinyeditor').getContent())
          }
        });
    });
</script>
<style type="text/css">
    label:after{
        content:"*";
        color:red;
    }
</style>