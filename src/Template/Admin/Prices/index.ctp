<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Prix</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Prix <?= ($car != null) ? ' pour <b>'.$car->brand->name." ".$car->model->name." ".$car->model->year.'</b>' : '' ?> 
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addPrice" style="float: right; margin-bottom: 25px;">
                    Ajouter un Prix
                </button>
                <!-- Modal Add Price -->
                <div class="modal fade" id="addPrice" tabindex="-1" role="dialog" aria-labelledby="editPriceLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addPriceLabel">
                                    Ajouter un Prix
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create($priceEntity,['name'=>'PriceForm','url' => ['action' => 'add']]) ?>
                                <?php if (isset($car_id) && !empty($car_id)) {
                                    echo $this->Form->hidden('car_id', ['value'=>$car_id]);
                                }else{ ?>
                                    <div class="form-group" style="display: block;">
                                        <div>
                                            <?= $this->Form->label('Voiture') ?>
                                        </div>
                                        <?= $this->Form->select('car_id',
                                            $cars,
                                            ['class'=>'form-control',
                                            'empty' => '(choisissez une Voiture)'
                                            ]
                                        ) ?>
                                    </div>
                                    <br/>
                                <?php } ?>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('from_day',[
                                        'label' => 'De (Jours)',
                                        'class'=>'form-control',
                                        'placeholder'=>'Jour Debut',
                                        'type'=>'number'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('to_day',[
                                        'label' => 'A (Jours)',
                                        'class'=>'form-control',
                                        'placeholder'=>'Jour Fin',
                                        'type'=>'number'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('price_mad',[
                                        'label' => 'Prix en Dh',
                                        'class'=>'form-control',
                                        'placeholder'=>'Prix en MAD',
                                        'type'=>'number'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('price_euro',[
                                        'label' => 'Prix en Euro',
                                        'class'=>'form-control',
                                        'placeholder'=>'Prix en Euro',
                                        'type'=>'number'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('price_usd',[
                                    'label' => 'Prix en USD',
                                    'class'=>'form-control',
                                    'placeholder'=>'Prix en USD',
                                    'type'=>'number'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="button" class="btn btn-primary" onclick="validateForm('')">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /Modal Add Price -->
                <br/>
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-prices">
                        <thead>
                            <tr>
                                <th>Voiture</th>
                                <th>De (jours)</th>
                                <th>A (jours)</th>
                                <th>Prix (MAD)</th>
                                <th>Prix (Euro)</th>
                                <th>Prix (USD)</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($prices)) : ?>
                            <?php foreach ($prices as $price) :?>
                            <tr class="">
                                <td>
                                    <?= $price->car->brand->name." ".$price->car->model->name." ".$price->car->model->year; ?>
                                </td>
                                <td><?= $price->from_day."j"; ?></td>
                                <td><?= $price->to_day."j"; ?></td>
                                <td><?= $price->price_mad; ?></td>
                                <td><?= $price->price_euro; ?></td>
                                <td><?= $price->price_usd; ?></td>
                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editPrice<?= $price->id ?>" onclick=""><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Price -->
                                    <div class="modal fade" id="editPrice<?= $price->id ?>" tabindex="-1" role="dialog" aria-labelledby="editPriceLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editPriceLabel">
                                                        Edition des informations de la Voiture
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $price->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $price->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create($price,['name'=>'PriceForm'.$price->id,'url' => ['action' => 'edit']]) ?>
                                                    <?php if (isset($car_id) && !empty($car_id)) {
                                                        echo $this->Form->hidden('car_id', ['car_id'=>$car_id]);
                                                    }else{ ?>
                                                        <div class="form-group" style="display: block;">
                                                            <div>
                                                                <?= $this->Form->label('Voiture') ?>
                                                            </div>
                                                            <?= $this->Form->select('car_id',
                                                                $cars,
                                                                ['class'=>'form-control',
                                                                'empty' => '(choisissez une Voiture)'
                                                                ]
                                                            ) ?>
                                                        </div>
                                                        <br/>
                                                    <?php } ?>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('from_day',[
                                                            'label' => 'De (Jours)',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Jour Debut',
                                                            'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('to_day',[
                                                            'label' => 'A (Jours)',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Jour Fin',
                                                            'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('price_mad',[
                                                            'label' => 'Prix en Dh',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Prix en MAD',
                                                            'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('price_euro',[
                                                            'label' => 'Prix en Euro',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Prix en Euro',
                                                            'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('price_usd',[
                                                        'label' => 'Prix en USD',
                                                        'class'=>'form-control',
                                                        'placeholder'=>'Prix en USD',
                                                        'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="button" class="btn btn-primary" onclick="validateForm('<?= $price->id ?>')">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Price -->
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Prices', 'action' => 'delete', $price->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer la voiture ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="8"><center>Aucun Prix a afficher</center></td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
    $(document).ready(function() {
        $('#dataTables-prices').DataTable({
                responsive: true
        });
    })

    function validateForm(id){
            var fullformname = 'PriceForm'+id;
            var errClass = "#err-msg"+id;
            $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
            $(errClass).hide();

            var cpt = 0;
            $.each($('form[name='+fullformname+']').find('.form-control'), function(){
                //console.log($(this).val());
                if (!$(this).val()) {
                    $(this).addClass("validation");
                    cpt++;
                }
            });
            if (cpt == 0) {
                $('form[name='+fullformname+']').submit();
            }
        }
</script>

<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>