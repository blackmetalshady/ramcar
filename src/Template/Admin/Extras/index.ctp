<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Extras</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Extras
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Extras -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addExtra" style="float: right; margin-bottom: 25px;">
                    Ajouter une Extras
                </button>
                <br/>
                <!-- Modal Add Extra -->
                <div class="modal fade" id="addExtra" tabindex="-1" role="dialog" aria-labelledby="addExtraLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="editExtraLabel">
                                    Edition des informations de la Extras
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create($extraEntity,['type'=>'file','name'=>'ExtraForm','url' => ['action' => 'add']]) ?>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('name_fr',[
                                    'label' => 'Nom de l\'Extra en Fr',
                                    'class'=>'form-control',
                                    'placeholder'=>'Nom du Produit en Fr'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('name_en',[
                                    'label' => 'Nom de l\'Extra en Ang',
                                    'class'=>'form-control',
                                    'placeholder'=>'Nom de l\'Extra en Ang'
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Type') ?>
                                    </div>
                                    <?= $this->Form->select('type',
                                        $types,[
                                        'class'=>'form-control',
                                    ]) ?>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <div class="form-group">
                                    <?= $this->Form->label('Image du Produit');?>
                                    </div>
                                    <div class="fileUpload btn btn-primary" style="bottom: 10px;left: -10px;">
                                        <span>Ajouter une Image</span>
                                        <?= $this->Form->input('img', [
                                            'type' => 'file',
                                            'accept'=>'image/*',
                                            'class'=>'upload',
                                            'id'=>'Img',
                                            'onchange'=>'previewFile("")',
                                            'label'=>false
                                        ]);?>
                                    </div>
                                    <div class="imgpreviewdiv" style="display: none">
                                        <center><img src="" id="ImgPreview" height="100"></center>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('description_fr',[
                                        'label' => 'Déscription Français',
                                        'placeholder'=>'Déscription Français'
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('description_en',[
                                        'label' => 'Déscription Anglais',
                                        'placeholder'=>'Déscription Anglais'
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <div style="padding-bottom: 35px;">
                                    <?= $this->Form->label('Plus de Détails Français');?>
                                    </div>
                                    <?= $this->Form->input('details_fr',[
                                        'class'=>'tmce'
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <div style="padding-bottom: 35px;">
                                    <?= $this->Form->label('Plus de Détails Anglais');?>
                                    </div>
                                    <?= $this->Form->input('details_en',[
                                        'class'=>'tmce'
                                        ]
                                    ) ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="button" class="btn btn-primary" onclick="validateForm('')">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /Modal Add Extra -->
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-Extras">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Produit</th>
                                <th>Modifier</th>
                                <th>Prix</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($extras as $extra) :?>
                            <tr class="">
                                <td><?= $this->Html->image('extras/'.$extra->img,['style'=>'max-width: 80px']); ?></td>
                                <td><?= $extra->name_fr; ?></td>

                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editExtra<?= $extra->id ?>" onclick="clearform();"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Extra -->
                                    <div class="modal fade" id="editExtra<?= $extra->id ?>" tabindex="-1" role="dialog" aria-labelledby="editExtraLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editExtraLabel">
                                                        Edition des informations de la Extras
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $extra->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $extra->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create($extra,['type'=>'file','name'=>'ExtraForm'.$extra->id,'url' => ['action' => 'edit']]) ?>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('name_fr',[
                                                        'label' => 'Nom de l\'Extra en Fr',
                                                        'class'=>'form-control',
                                                        'placeholder'=>'Nom du Produit en Français'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('name_en',[
                                                        'label' => 'Nom de l\'Extra en en Ang',
                                                        'class'=>'form-control',
                                                        'placeholder'=>'Nom du Produit en Anglais'
                                                        ]) ?>
                                                    </div>
                                                    <br>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Type') ?>
                                                        </div>
                                                        <?= $this->Form->select('types',
                                                            $types,[
                                                            'class'=>'form-control',
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="width: 100%;">
                                                        <div class="form-group" style="float: left;">
                                                        <?= $this->Form->label('Image du Produit');?>
                                                        </div>
                                                        <div class="fileUpload btn btn-primary" style="bottom: 10px;left: -10px;float: left;">
                                                            <span>Ajouter une Image</span>
                                                            <?= $this->Form->input('img', [
                                                                'type' => 'file',
                                                                'accept'=>'image/*',
                                                                'class'=>'upload',
                                                                'id'=>'Img'.$extra->id,
                                                                'onchange'=>'previewFile("'.$extra->id.'")',
                                                                'label'=>false
                                                            ]);?>
                                                        </div>
                                                        <br><br><br>
                                                        <div class="imgpreviewdiv<?= $extra->id; ?>">
                                                            <center>
                                                                <?= $this->Html->image('extras/'.$extra->img, ['height'=>'100',
                                                                    'id'=>'ImgPreview'.$extra->id
                                                                ])?>
                                                            </center>
                                                        </div>
                                                    </div>
                                                    <br><br>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('description_fr',[
                                                            'placeholder'=>'Description Français',
                                                            'label'=>'Description Français'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('description_en',[
                                                            'placeholder'=>'Description Anglais',
                                                            'label'=>'Description Anglais'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div style="padding-bottom: 35px;">
                                                        <?= $this->Form->label('Plus de Détails Français');?>
                                                        </div>
                                                        <?= $this->Form->input('details_fr',[
                                                            'class'=>'tmce',
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div style="padding-bottom: 35px;">
                                                        <?= $this->Form->label('Plus de Détails Anglais');?>
                                                        </div>
                                                        <?= $this->Form->input('details_en',[
                                                            'class'=>'tmce'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="button" class="btn btn-primary" onclick="validateForm('<?= $extra->id ?>')">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Extra -->
                                </td>
                                <td>
                                    <?= $this->Html->link(
                                        '<i class="fa fa-eur"></i>',
                                        ['controller' => 'Prices', 'action' => 'extras', $extra->id],
                                        ['escape'=>false,'class'=>'btn btn-primary btn-circle']); ?>
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Extras', 'action' => 'delete', $extra->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer cette article ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script>
    $(document).ready(function() {
        $('#dataTables-Extras').DataTable({
                responsive: true
        });
        tinymce.init({
          selector: 'textarea.tmce',
          language: 'fr_FR'
        });
    });

      function validateForm(id){
            var fullformname = 'ExtraForm'+id;
            var errClass = "#err-msg"+id;
            $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
            $(errClass).hide();

            var cpt = 0;
            $.each($('form[name='+fullformname+']').find('.form-control'), function(){
                //console.log($(this).val());
                if (!$(this).val()) {
                    $(this).addClass("validation");
                    cpt++;
                }
            });
            if (cpt == 0) {
                $('form[name='+fullformname+']').submit();
            }
        }

        function clearform(){
            $(".alert-dismissable div").empty();
            $(".alert-dismissable").hide();
            $("input, select").removeClass("validation");
        }
        function previewFile(id) {
          var preview = document.querySelector('#ImgPreview'+id);
          var file    = document.querySelector('#Img'+id).files[0];
          var reader  = new FileReader();

          reader.onloadend = function () {
            preview.src = reader.result;
          }

          if (file) {
            reader.readAsDataURL(file);
            $('.imgpreviewdiv'+id).show();
          } else {
            preview.src = "";
          }
        }
</script>
<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: 50%;
    }
    
</style>