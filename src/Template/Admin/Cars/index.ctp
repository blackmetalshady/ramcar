<?php $this->assign('title', $title); ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gestion des Voitures</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Liste des Voitures
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!-- Button trigger modal Ajout Voiture -->
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addCar" style="float: right; margin-bottom: 25px;">
                    Ajouter une Voiture
                </button>
                <br/>
                <!-- Modal Add Car-->
                <div class="modal fade" id="addCar" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 class="modal-title" id="addCarLabel">
                                    Ajouter une Nouvelle Voiture
                                </h3>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-danger alert-dismissable" id="msgErr" style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <div style="font-weight: bold;">
                                        
                                    </div>
                                </div>
                                <div class="alert alert-danger" id="err-msg" style="display: none;">
                                    Veuillez remplir les champs vide
                                </div>
                                <?= $this->Form->create($carEntity,['name'=>'CarForm','url' => ['action' => 'add']]) ?>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Marque') ?>
                                    </div>
                                    <?= $this->Form->select('brand_id',
                                        $brands,
                                        ['class'=>'form-control',
                                        'onchange'=>'loadcarmodels(\'\',this);',
                                        'empty' => '--'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Modele')?>
                                    </div>
                                    <?= $this->Form->select('model_id',
                                        $models,
                                        ['class'=>'form-control',
                                        'empty'=>'--',
                                        'disabled'=>'disabled'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Type')?>
                                    </div>
                                    <?= $this->Form->select('type_id',$types,
                                    ['class'=>'form-control','empty' => '--']) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Carburant')?>
                                    </div>
                                    <?= $this->Form->select('fuel_id',
                                        $fuels,
                                        ['class'=>'form-control','empty' => '--']
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('horses',[
                                    'label' => 'Nombre de Chevaux',
                                    'class'=>'form-control',
                                    'placeholder'=>'Nombre de Chevaux',
                                    'type'=>'number'
                                    ]) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('registration',[
                                        'label' => 'Matricule',
                                        'class'=>'form-control',
                                        'placeholder'=>'Matricule'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('passengers',[
                                        'label' => 'Nombre de Passager',
                                        'class'=>'form-control',
                                        'placeholder'=>'Nombre de Passager'
                                        ]
                                    ) ?>
                                </div>
                                <br/>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Vitesse Automatique') ?>
                                    </div>
                                    <?= $this->Form->select('auto',
                                        ['0'=>'Non','1'=>'Oui'],
                                        ['class'=>'form-control',
                                        'default'=>0
                                    ]) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div>
                                        <?= $this->Form->label('Climatisation') ?>
                                    </div>
                                    <?= $this->Form->select('airconditioning',
                                        ['0'=>'Non','1'=>'Oui'],[
                                        'class'=>'form-control',
                                        'default'=>1
                                    ]) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <?= $this->Form->input('color',[
                                        'label' => 'Couleur',
                                        'style'=>'max-width: 100px;',
                                        'class'=>'form-control jscolor{width:243, height:150,borderColor:\'#FFF\', insetColor:\'#FFF\', backgroundColor:\'#666\'}',
                                        'placeholder'=>'Couleur'
                                        ]
                                    ) ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div style="padding-bottom: 35px;">
                                    <?= $this->Form->label('Détails Francais');?>
                                    </div>
                                    <?= $this->Form->input('details_fr'); ?>
                                </div>
                                <div class="form-group" style="display: block;">
                                    <div style="padding-bottom: 35px;">
                                    <?= $this->Form->label('Détails Anglais');?>
                                    </div>
                                    <?= $this->Form->input('details_en') ?>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="button" class="btn btn-primary" onclick="validateForm('')">Ajouter</button>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal Add Car -->
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-cars">
                        <thead>
                            <tr>
                                <th>Marque</th>
                                <th>Model</th>
                                <th>Type</th>
                                <th>Couleur</th>
                                <th>Vitesse Auto</th>
                                <th>Clim</th>
                                <th>Photo</th>
                                <th>Modifier</th>
                                <th>Prix</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach ($cars as $car) :?>
                            <tr class="">
                                <td><?= $this->Html->image('cars/logos/'.$car->brand->logo); ?></td>
                                <td><?= $car->model->name; ?></td>
                                <td><?= $car->type->name_fr; ?></td>
                                <td>
                                    <center>
                                        <div class="circle" style="background-color: #<?= $car->color; ?>"></div>
                                    </center>
                                </td>
                                <td><?=  ($car->auto == 1) ? "Oui" : "Non" ; ?></td>
                                <td><?=  ($car->airconditioning == 1) ? "Oui" : "Non" ; ?></td>
                                <td><?php if(empty($car->medias)){ 
                                        echo $this->Html->image('cars/unknown.jpg',[
                                            'style'=>'max-width: 80px',
                                            'url' => ['controller' => 'Medias', 'action' => 'view', $car->id]
                                            ]) ;
                                    }else{ 
                                        echo $this->Html->image('cars/'.$car->medias[0]->name,[
                                            'style'=>'max-width: 80px',
                                            'url' => ['controller' => 'Medias', 'action' => 'view', $car->id]
                                            ]);
                                    }  ?>
                                </td>

                                <td class="center">
                                    <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#editCar<?= $car->id ?>" onclick="clearform();"><i class="fa fa-pencil"></i>
                                    </button>
                                    <!-- Modal Edit Car -->
                                    <div class="modal fade" id="editCar<?= $car->id ?>" tabindex="-1" role="dialog" aria-labelledby="editCarLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3 class="modal-title" id="editCarLabel">
                                                        Edition des informations de la Voiture
                                                    </h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="alert alert-danger alert-dismissable" id="msgErr<?= $car->id ?>" style="display: none;">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                        <div style="font-weight: bold;">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="alert alert-danger" id="err-msg<?= $car->id ?>" style="display: none;">
                                                        Veuillez remplir les champs vide
                                                    </div>
                                                    <?= $this->Form->create($car,['name'=>'CarForm'.$car->id,'url' => ['action' => 'edit']]) ?>
                                                    <div class="form-group" style="display: block;text-align: left;">
                                                        <label style="min-width: 180px">
                                                            <?= $this->Form->checkbox('published'); ?>
                                                            Publié
                                                        </label>
                                                        <?php if ($car->published == '1') :?>
                                                            <div class="alert alert-success" style="    min-width: 300px;display: inline-block;">
                                                                La voiture est publié sur le site
                                                            </div>
                                                        <?php else: ?>
                                                            <div class="alert alert-danger" style="    min-width: 300px;display: inline-block;">
                                                                Attention : La voiture n'est pas publié sur le site
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Marque') ?>
                                                        </div>
                                                        <?= $this->Form->select('brand_id',
                                                            $brands,
                                                            ['class'=>'form-control',
                                                            'onchange'=>'loadcarmodels(\''.$car->id.'\',this);',
                                                            'empty' => '(choisissez une Marque)'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Modele')?>
                                                        </div>
                                                        <?= $this->Form->select('model_id',
                                                            $models,
                                                            ['class'=>'form-control',
                                                            'empty' => '(choisissez un Model)'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Type')?>
                                                        </div>
                                                        <?= $this->Form->select('type_id',$types,
                                                        ['class'=>'form-control',
                                                        'empty' => '(choisissez un Type)']) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Carburant')?>
                                                        </div>
                                                        <?= $this->Form->select('fuel_id',
                                                            $fuels,
                                                            ['class'=>'form-control',
                                                            'empty' => '(choisissez un Carburant)']
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('horses',[
                                                        'label' => 'Nombre de Chevaux',
                                                        'class'=>'form-control',
                                                        'placeholder'=>'Nombre de Chevaux',
                                                        'type'=>'number'
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('registration',[
                                                            'label' => 'Matricule',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Matricule'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('passengers',[
                                                            'label' => 'Nombre de Passager',
                                                            'class'=>'form-control',
                                                            'placeholder'=>'Nombre de Passager'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/><br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Vitesse Automatique') ?>
                                                        </div>
                                                        <?= $this->Form->select('auto',
                                                            ['0'=>'Non','1'=>'Oui'],[
                                                            'class'=>'form-control',
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div>
                                                            <?= $this->Form->label('Climatisation') ?>
                                                        </div>
                                                        <?= $this->Form->select('airconditioning',
                                                            ['0'=>'Non','1'=>'Oui'],[
                                                            'class'=>'form-control',
                                                        ]) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <?= $this->Form->input('color',[
                                                            'label' => 'Couleur',
                                                            'style'=>'max-width: 100px;',
                                                            'class'=>'form-control jscolor{width:243, height:150,borderColor:\'#FFF\', insetColor:\'#FFF\', backgroundColor:\'#666\'}',
                                                            'placeholder'=>'Couleur'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div style="padding-bottom: 35px;">
                                                        <?= $this->Form->label('Details Francais');?>
                                                        </div>
                                                        <?= $this->Form->input('details_fr',[
                                                            'placeholder'=>'Details Francais'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <br/>
                                                    <div class="form-group" style="display: block;">
                                                        <div style="padding-bottom: 35px;">
                                                        <?= $this->Form->label('Details Anglais');?>
                                                        </div>
                                                        <?= $this->Form->input('details_en',[
                                                            'placeholder'=>'Details Anglais'
                                                            ]
                                                        ) ?>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                    <button type="button" class="btn btn-primary" onclick="validateForm('<?= $car->id ?>')">Modifier</button>
                                                </div>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /Modal Edit Car -->
                                </td>
                                <td>
                                    <?= $this->Html->link(
                                        '<i class="fa fa-eur"></i>',
                                        ['controller' => 'Prices', 'action' => 'index', $car->id],
                                        ['escape'=>false,'class'=>'btn btn-primary btn-circle']); ?>
                                </td>
                                <td class="center">
                                    <?= $this->Form->postLink(
                                        '<i class="fa fa-times"></i>',
                                        ['controller' => 'Cars', 'action' => 'delete', $car->id],
                                        ['confirm' => 'Etes vous sur de vouloir supprimer la voiture ?','class'=>'btn btn-danger btn-circle','escape'=>false]
                                    ); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script>
    $(document).ready(function() {
        $('#dataTables-cars').DataTable({
                responsive: true
        });
        tinymce.init({
          selector: 'textarea',
          language: 'fr_FR'
        });
    });

      function validateForm(id){
            var fullformname = 'CarForm'+id;
            var errClass = "#err-msg"+id;
            $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
            $(errClass).hide();

            var cpt = 0;
            $.each($('form[name='+fullformname+']').find('.form-control'), function(){
                //console.log($(this).val());
                if (!$(this).val()) {
                    $(this).addClass("validation");
                    cpt++;
                    console.log($(this));
                }
            })

            if (cpt == 0) {
                $(errClass).hide();
                $('form[name='+fullformname+']').find('.form-control').removeClass("validation");
                var matricule = $('form[name='+fullformname+']').find("input[name=registration]").val();
                $.post( "/admin/cars/exists/"+id, {matricule: matricule })
                    .done(function( data ) {
                        $("#msgErr"+id+" div").empty();
                        $("#msgErr"+id).hide();
                        if (data == "ok") {
                            $('form[name='+fullformname+']').submit();
                        }else{

                              $('form[name='+fullformname+']').find("#"+key).addClass("validation");
                              $("#msgErr"+id+" div").append("Il exist déjà un voiture avec le matricule<u>"+matricule+"</u>, veuillez verifier les données");
                              $("#msgErr"+id).show('slow');
                        }
                });
            }else{
                $(errClass).show('slow');
            }
        }

        function loadcarmodels(id, brandElement){
            var fullformname = 'CarForm'+id;
            var brand_id = $(brandElement).val();
            if (brand_id) {
               $.post( "/admin/models/loadbrandmodels/", {brandid: brand_id})
                .done(function( data ) {
                    if (data) {
                        $('form[name='+fullformname+']').find("select[name=model_id]").empty();
                        $('form[name='+fullformname+']').find("select[name=model_id]").append(data);
                        $('form[name='+fullformname+']').find("select[name=model_id]").prop('disabled', false);
                    }else{
                        $('form[name='+fullformname+']').find("select[name=model_id]").empty();
                        $('form[name='+fullformname+']').find("select[name=model_id]").append('<option value="">--</option>')
                        $('form[name='+fullformname+']').find("select[name=model_id]").prop('disabled', true);
                    }
                });
            }else{
                $('form[name='+fullformname+']').find("select[name=model_id]").empty();
                $('form[name='+fullformname+']').find("select[name=model_id]").prop('disabled', true);
            }
        }
        function clearform(){
            $(".alert-dismissable div").empty();
            $(".alert-dismissable").hide();
            $("input, select").removeClass("validation");
        }

</script>
<style type="text/css">
    .modal-body{
        overflow-y: scroll;
        max-height: 400px;
    }

    .modal-body .form-group div label{
        float: left;
        min-width: 180px;
        text-align: left;
        padding-top: 7px;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, .4);
    }
    .modal-body .form-group div input,
    .modal-body .form-group div textarea, 
    .modal-body .form-group select{
        display: block;
        width: auto;
    }
    
</style>