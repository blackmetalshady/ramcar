<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<br> 
<div id="main">
    <div id="primary" style="background: none;">                  
        <div class="clear"></div>
        <aside class="sidebar-left" style="margin-left: -2px"> 
            <div class="widget info_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/info.png') ?>
                    Info
                </h3>
                <div class="widget-content"><strong><?= __('Please Note') ?>: </strong> <?= __('The vehicles shown are examples. Specific models within a car class may vary in availability and features, such as passenger seating, luggage capacity and mileage'); ?>.</div>
            </div>
            <div class="widget filter_widget">
                <h3 class="widget-title">
                    <?= $this->Html->image('icons/filter.png') ?>
                    <?= __('Filter results') ?>
                </h3>
                <h4><?= __('Price range') ?></h4>
                <div class="widget-content-range">                              
                    <input class="price_range" value="0;1500" name="price_range" />
                </div>
                <h4><?= __('Fuel') ?></h4>
                <div class="widget-content-range">                              
                    <?php foreach ($fuels as $fuel) :?>    
                        <div class="filter">
                            <input id="fuels_<?= $fuel->{'name_'.$lang}; ?>" type="checkbox" class="styled" name="fuels_<?= $fuel->name; ?>" value="<?= $fuel->id; ?>" data-type="<?= $fuel->{'name_'.$lang}; ?>" checked/> 
                            <label for="manufacturers_<?= $fuel->{'name_'.$lang}; ?>">
                                <?= $fuel->{'name_'.$lang}; ?>
                            </label> 
                            <div class="filter_quantity">
                                <?= sizeof($fuel->cars); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>  
                </div>
                <h4><?= __('Manufacturers') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($carsbybrand as $brand) :?>    
                        <?php if (sizeof($brand->cars) > 0) :?> 
                        <div class="filter">
                            <input id="manufacturers_<?= $brand->name; ?>" type="checkbox" class="styled" name="manufacturers_<?= $brand->name; ?>" value="<?= $brand->id; ?>" data-type="<?= $brand->name; ?>" checked/> 
                            <label for="manufacturers_<?= $brand->name; ?>">
                                <?= $brand->name; ?>
                            </label> 
                            <div class="filter_quantity">
                                <?= sizeof($brand->cars); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>                             
                </div>
                <h4><?= __('Number of passangers') ?></h4>
                <div class="widget-content-range">        
                    <div class="shortcode_single_slider">
                        <input class="passangers_range" value="0;5" name="shortcode_single_range" />
                    </div>
                </div>
                <h4><?= __('Vehicle type') ?></h4>
                <div class="widget-content widget-filter">
                    <?php foreach ($types as $type) :?>
                    <div class="filter">
                        <input id="type_<?= $type->{'name_'.$lang} ?>" type="checkbox" class="styled" name="type_<?= $type->{'name_'.$lang} ?>" value="<?= $type->id ?>" data-type="<?= $type->{'name_'.$lang} ?>" checked /> 
                        <label for="type_<?= $type->{'name_'.$lang} ?>"><?= ucfirst($type->{'name_'.$lang})?></label> 
                        <div class="filter_quantity"><?= sizeof($type->cars) ?></div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>                                                          
        </aside>    
        <div id="content" class="sidebar-middle" style="margin-left: 10px; margin-right: 0px;">
            <div class="widget main-widget product-widget">
                <div class="content-overlay">
                    <?= $this->Html->image('ajax-loader.gif',['class'=>'ajax-loader']); ?>
                    
                </div>
                <div class="frm">
                    <div class="widget-title">
                        <div>
                            <?= $this->Html->image('icons/results.png') ?>
                            <?= __('All Cars') ?> <span><?= $this->Paginator->counter()." ".__('Pages'); ?> </span>
                        </div>
                        <div class="widget-title-sort">
                            <span class="viev-all"><?= __('Sort by') ?>: </span> 
                             <a href="#"  onclick="sort('type');return false;" class="current sortby" id="sortbytype">
                                <?= __('Type') ?>
                             </a> 
                             | 
                             <a href="#" onclick="sort('price');return false;" class="sortby" id="sortbyprice">
                                <?= __('Brand') ?>
                             </a>
                        </div>
                        <div class="clear"></div>   
                    </div>
                    <div class="sortable">
                    <?php foreach ($cars as $car) :?>
                        <div class="post <?= $car->brand->name; ?> <?= $car->fuel->{'name_'.$lang}; ?> <?= $car->type->{'name_'.$lang}; ?>" data-price="<?= (!empty($car->prices))?$car->prices[0]->price_mad : '0'; ?>" data-type="<?= $car->type_id; ?>">
                            <div class="main-block_container">
                                <div class="main-block">                                    
                                    <div class="product-img" data-id="<?= $car->id; ?>">
                                        <?= $this->Html->image("cars/".$car->medias[0]->name,['class'=>'grow']);?>
                                    </div>
                                    <div style="display: none" id="gallery<?= $car->id; ?>" class="gl">
                                        <?php foreach ($car->medias as $media) :?>
                                            <a href="/img/cars/<?= $media->name; ?>">
                                                <?= $this->Html->image("cars/".$media->name);?>
                                            </a>
                                        <?php endforeach;?>
                                    </div>
                                    <div class="product-info">
                                        <h3 class="entry-format"><?php $carname = $car->brand->name." ".$car->model->name; ?> <?=(strlen($carname) > 24)?$car->model->name : $carname ?><span>|  <?= ucfirst($car->type->{'name_'.$lang}); ?></span></h3>
                                        <div class="features">
                                            <p><?= $this->Html->image('icons/passengers.png')?>
                                            <?= $car->passengers; ?> <?= __('passengers') ?></p>
                                            <?php if ($car->auto == 1): ?>
                                                <p>
                                                    <?= $this->Html->image('icons/autotransmission.png')?> 
                                                    <?= __('automatic transmission') ?>
                                                </p>
                                            <?php endif ?>
                                            <?php if ($car->airconditioning): ?>
                                                <p>
                                                    <?= $this->Html->image('icons/airconditioning.png')?> 
                                                    <?= __('air conditioning') ?>
                                                </p>
                                                
                                            <?php endif ?>
                                            <p>
                                                <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
                                            </p>
                                        </div>
                                        <?php if (!empty($car->{'details_'.$lang})): ?>
                                            <div class="details">
                                                <div class="view-details">[+] <?= __('View car details'); ?></div>
                                                <div class="close-details">[-] <?= __('Close car details'); ?></div>
                                                <span class="details-more">
                                                    <?= $car->{'details_'.$lang} ?>
                                                </span>
                                            </div>
                                        <?php endif ?>
                                    </div>                              
                                </div>
                                <div class="additional-block">
                                    <ul>
                                    <?php foreach ($car->prices as $price):?>
                                        <?php if ($lang == 'fr') {
                                            $price_ox = $price->price_euro.'€';
                                        }else{
                                            $price_ox = '$'.$price->price_usd;
                                        } ?>
                                        <li><?= __('From').' <b>'.$price->from_day.'</b> '.__('To').' <b>'.$price->to_day.'</b> '.__('Days').' : <span class="price_ox">'.$price_ox.'</span> <span class="price_mad">('.$price->price_mad.' MAD)</span>' ?></li>
                                    <?php endforeach; ?>
                                    </ul>
                                    <br>
                                    <?= $this->Html->link(__('Check Availability'), ['controller'=>'Cars','action'=>'index'],['class'=>'continue_button blue_button']);?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="post-delimiter"></div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="pagination-block"> 
                <div class="pagination"> 
                    <ul class="pagination">
                        <?= $this->Paginator->prev('') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('') ?>
                    </ul>
                    <p class="clear"></p> 
                </div> 
            </div>
            <!-- <div class="pagination">
                <span>
                    <ul class="pagination">
                        <?= $this->Paginator->prev('') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next('') ?>
                    </ul>
                </span>
            </div> -->
        </div>              
        <div class="clear"></div>
    </div>
</div>

<style type="text/css">
    .pagination{
        margin-bottom: 10px;
    }
    .pagination li{
        padding: 0px;
    }
    .pagination li a{
        margin: 0px; 
        display: block; 
        width: 100%; 
        height: 100%
    }

    .product-widget .additional-block{
        padding: 20px 0px !important;
    }
    .product-img img{
        border-radius: 5px;
    }
    .additional-block{
       /* width: inherit !important;*/
        text-align: center !important;
    }
    
    .price_ox{
        font-weight: bold;
    }
    .price_mad{
        font-size: 14px;
    }
</style>
<script type="text/javascript">
    $(function() {
         $('.gl').lightGallery({
                thumbnail:true
            }); 

         $('.product-img').on('click', function(){
            var id = $( this ).data('id');
            $( "#gallery"+id+" a:first-child" ).click();
         })
    });
$('.price_range').on('valuechange', function(){alert('test')});
</script>