<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<div id="main">
    <div id="primary" style="background: none;">   
		<aside class="sidebar-left"> 
		    <div class="widget">

		        <h3 class="widget-title">
		            <?= $this->Html->image('icons/orderinfo.png'); ?>
		            <?= __('Top Rented Cars') ?>
		        </h3>
		        <?php foreach ($cars as $car) : ?>
		        <h4>
		        <?php 
		            $price = ($lang == 'fr') ?  $car->prices[0]->price_euro.'€': '$'.$car->prices[0]->price_usd ;
		        ?>
		        <?= __('Rent this car for only').'<span class="car-price">'.$price.'</span>('.$car->prices[0]->price_mad.' MAD )' ?>
		        </h4>
		        <div class="widget-content main-block product-widget-mini">                             
		            <div class="product-img custom-img" style='width: 130px'>
		                <?= $this->Html->image('cars/'.$car->medias[0]->name,['style'=>'width: 130px']); ?>
		            </div>
		            <div class="product-info">
		                <div class="entry-format">
		                    <div><?= ucfirst($car->brand->name)." ".ucfirst($car->model->name)?></div>
		                    <span><?= ucfirst($car->type->{'name_'.$lang}) ?></span>
		                </div>
		                <div class="features">
		                    <p><?= $this->Html->image('icons/passengers.png')?>
		                    <?= $car->passengers; ?> <?= __('passengers') ?></p>
		                    <?php if ($car->auto == 1): ?>
		                        <p>
		                            <?= $this->Html->image('icons/autotransmission.png')?> 
		                            <?= __('automatic transmission') ?>
		                        </p>
		                    <?php endif ?>
		                    <?php if ($car->airconditioning): ?>
		                        <p>
		                            <?= $this->Html->image('icons/airconditioning.png')?> 
		                            <?= __('air conditioning') ?>
		                        </p>
		                        
		                    <?php endif ?>
		                    <p>
		                        <?= $this->Html->image('icons/fuel.png')." ".$car->fuel->{'name_'.$lang}?>
		                    </p>
		                </div>
		                <?php if (!empty($car->{'details_'.$lang})): ?>
		                    <div class="details">
		                        <div class="view-details">[+] <?= __('View car details'); ?></div>
		                        <div class="close-details">[-] <?= __('Close car details'); ?></div>
		                        <span class="details-more">
		                            <?= $car->{'details_'.$lang} ?>
		                        </span>
		                    </div>
		                <?php endif ?>
		            </div>
		        </div>
		        <?php endforeach; ?>

		    </div>                                                          
		</aside>  
		<div id="content" class="sidebar-middle" style="margin-left: 8px;">
		    <div class="widget main-widget product-widget" style="min-height: 460px;">
		    	<div class="pg-title">
				    <h2>
				    	<?= __('Your Messages'); ?>
				    </h2>
				    <div class="separator"></div>
				</div>
			    <div class="widget-content-range">
			    	<div class="inbox">
				    	<div class="column column-obj">
				    		<div class="inbox-head"></div>
				    		<div>
				    		<?php foreach ($messages as $msg) : ?>
				    		<div class="msg-object" id="<?= $msg->id; ?>">
					    		<div style="display: inline-block; width: 80%;margin-left: 4%;">
					    			<div class="msg-title">
					    				<?= $msg->subject; ?>
					    			</div>
					    			<div class="msg-short">
					    				<?php if (strlen($msg->message) > 50){
					    					echo substr($msg->message, 0, 49).'...';
					    				}else{
					    					echo $msg->message;
					    				} ?>
					    			</div>
				    			</div>
				    			<div class="msg-del">
				    				x
				    			</div>
				    		</div>
				    		<?php endforeach; ?>
				    		</div>
				    	</div>
				    	<div class="column column-content">
				    		<div class="inbox-head">
				    			<span class="helper"></span>
				    			<?= $this->Html->image('user.png', ['style'=>'vertical-align: middle; width: 30px;']); ?>
				    		</div>
				    		<div class="messages-box">
					    		<?php foreach ($messages as $key=>$msg) : ?>
						    		<div class="msg-content <?= ($key==0)? 'active' : 'hidden'?>" id="content_<?= $msg->id; ?>" >
						    			<?php if (!empty($msg->parent_message)) :?>
							    			<?php foreach ($msg->parent_message->child_messages as $cm) : ?>
							    				<div class="<?= ($cm->from_user == $this->request->session()->read('Auth.User.id'))? 'admin' : 'usr' ?>">
							    					<p>
							    						<?= (strlen($msg->message) > 50)? $cm->message; ?>
							    						<span class="send_time">
							    							<?= ($lang == 'fr')? $msg->sent_at->i18nFormat('dd/MM/yyyy HH:mm') : $msg->sent_at->i18nFormat('yyyy/MM/dd HH:mm'); ?>
							    						</span>
							    					</p>
							    				</div>
							    			<?php endforeach ?>
							    		<?php else: ?>
							    				<div class="<?= ($msg->from_user == $this->request->session()->read('Auth.User.id'))? 'admin' : 'usr' ?>">
							    					<p>
							    						<?= $msg->message; ?>
							    						<span class="send_time">
							    							<?= ($lang == 'fr')? $msg->sent_at->i18nFormat('dd/MM/yyyy HH:mm') : $msg->sent_at->i18nFormat('yyyy/MM/dd HH:mm'); ?>
							    						</span>
							    					</p>
							    				</div>
						    			<?php endif; ?>
						    		</div>
					    		<?php endforeach ?>
				    		</div>
				    		<div class="replay-box">
				    			<table style="width: 95%;margin-left: auto; margin-right: auto;">
				    				<tr>
				    					<td>
				    						<textarea class="txtarea"></textarea>
				    					</td>
				    					<td style="width: 30px">
				    						<button class="orange_btn send_btn">></button>
				    					</td>
				    				</tr>
				    			</table>
				    		</div>
				    	</div>
			    	</div>
			    	
			    </div>
		    </div>
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).on('click', '.msg-object', function(){
	var id = $(this).prop('id');
	$('.msg-content').removeClass('active');
	$('.msg-content').addClass('hidden');
	$('#content_'+id).removeClass('hidden');
	$('#content_'+id).addClass('active');
})
</script>
<style type="text/css">
.column-content{
	width: 65%; 
	position: relative;
}
.column-obj{
	width: 35%;
	overflow-y: auto;
}
.helper{
	height: 100%;
    vertical-align: middle;
    display: inline-block;
}
.inbox-head{
	width: 100%;
	height: 50px;
	border-bottom: 1px black solid;
}
.send_time{
	display: block;
	font-size: 11px;
	margin-top: 10px;
	color: #ababab
}
button.orange_btn{
	border: 1px solid #cc692f;
    background: #f08643;
    color: #fff;
}
button.orange_btn:hover{
	cursor: pointer;
	background: #ec702f;
}
.send_btn{
	display: inline-block;
	width: 100%; 
	height: 86px;
	vertical-align: middle;
}
.replay-box{
	position: absolute;
	bottom: 0;
	width: 100%;
	padding-top: 3px;
	padding-bottom: 7px;
	box-shadow: white 0px -5px 20px 0px;
}
.msg-object{
	background-color: grey;
	height: 70px;
	cursor: pointer;
}
.msg-del{
	display: inline-block;
    height: 70px;
    width: 16%;
    float: right;
    text-align: center;
    line-height: 70px;
}
.inbox{
	width: 95%; 
	margin-left: auto;
    margin-right: auto;
	border: 1px solid black;
	display: flex;
	padding: 5px;
	word-spacing: 2px;
	color: #000;
}
.column{
	display: flex;
	flex-direction: column;
	height: 500px;
	border: 1px solid black;
	margin: 5px 5px;
	/*padding: 5px 10px;*/
}
.msg-title{
	line-height: 25px;
	/*margin-left: 5px;
	font-weight: 5px;*/
}
.txtarea{
	width: 99%; 
	height: 80px;
	display: inline-block;
	vertical-align: middle;
	resize: none;
	border-radius: 0px 0px;
	border: 1px solid #cc692f;
}
.widget-content.product-widget-mini{
    position: relative;
    min-height: 160px;
}

h4{
	padding-left: 10px !important;
	padding-right: 0px !important;
}
.custom-img{
    position: absolute;
    right: 10px;
}
.widget>h4{
    height: 30px;
    text-transform: capitalize;
    line-height: 30px;
    font-size: 12px;
    font-weight: bold;
}

.car-price{
    margin-left: 10px;
    margin-right: 5px;
    font-size: 16px;
    color: #000;
}

.widget li {
     padding: 0px !important; 
}
.hidden{
	display: none;
}
.active{
	display: block;
}
.usr{
	margin-top: 5px;
	margin-right: 5px;
}
.admin{
	margin-top: 5px;
	margin-right: 5px;
}
.usr p{
	max-width: 65%;
	padding: 5px;
	text-align: left;
    margin-left: 10px;
    color: #66696a;
    line-height: 17px;
    font-weight: normal;
    font-size: 14px;
    background-color: #f1f3f5;
    padding: 5px 10px;
    /*white-space: pre-line;*/
    word-wrap: break-word;
    border-radius: 3px;
    text-align: left;
    box-sizing: border-box;
    display: inline-block;
}

.admin p{
	float: right;
	max-width: 65%;
	padding: 5px;
	text-align: right;
    margin-right: 10px;
    line-height: 17px;
    color: #333;
    font-weight: normal;
    font-size: 14px;
    background-color: #cfedfb;
    padding: 5px 10px;
    /*white-space: pre-line;*/
    word-wrap: break-word;
    border-radius: 3px;
    text-align: left;
    max-width: 100%;
    box-sizing: border-box;
}
.messages-box{
	overflow-y: auto;
	height: calc(100% - 100px);
}
</style>