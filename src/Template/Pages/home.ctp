<?php use Cake\I18n\I18n; ?>
<?php //print_r($hometext); die(); ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<div id="slider">
    <div id="slider-img">
        <ul class="slides">
            <?php for ($i=1; $i < sizeof($homeslider->slidermedias); $i++) : ?>
                <li><?= $this->Html->image('sliders/'.$homeslider->slidermedias[$i]->media->name); ?></li>
            <?php endfor; ?>
        </ul>
        <div id="slider-pattern"></div>
    </div>
    <div id="slider-content">
        <div id="slider-post">
            <div class="post">
                <div class="entry-header">
                    <h3 class="entry-format"><?= $homeslider->{'title_'.$lang}; ?></h3>
                </div>
                <div class="entry-content">
                    <?= $homeslider->{'description_'.$lang}; ?>
                </div>
                <!-- <div class="entry-meta"><a href="#" title="">Learn more</a></div> -->
            </div>  
            <div id="slider-front-img"> 
                <?= $this->Html->image('sliders/'.$homeslider->slidermedias[0]->media->name); ?>
            </div>    
        </div>
        <?= $this->Form->create(null, ['url'=>['controller'=>'Cars', 'action'=>'find'], 'class'=>'main-form','id'=>'slider-form']) ?>
            <div class="main_form_navigation">              
                <div id="book_car" class="title-form current"><h3 style="font-weight: bold;"><?= __('Book a Car'); ?></h3></div>
            </div>
            <div id="book_car_content" class="content-form ">
                <div class="location-block">
                    <div class="form-block location"><?= __('Location'); ?></div>
                    <?= $this->Form->input('pickup_location',[
                        'label'=>false,
                        'class'=>'location',
                        'id'=>'pickup-location',
                        'required'=>true,
                        'placeholder'=>__('Enter a location')
                    ]);?>
                   <!-- <input class="location" id="pickup-location" type="text" value="" placeholder="<?= __('Enter a location'); ?>" name="location" /> -->
                    <input id="location-checkbox" type="checkbox" class="styled" name="checkbox_location" value="1" />
                    <label for="location-checkbox"> <?= __('Return at different location'); ?></label>
                </div>
                <div class="location-block return_location">
                    <div class="form-block location"><?= __('Return location'); ?></div>
                    <?= $this->Form->input('return_location',[
                        'label'=>false,
                        'class'=>'location',
                        'id'=>'return-location',
                        'placeholder'=>__('Return location')
                    ]);?>
                </div>
                <div class="form-block pick-up">
                    <h4><?= __('Pick-up date'); ?></h4>
                    <?= $this->Form->input('from_date',[
                        'label'=>false,
                        'autocomplete'=>'off',
                        'required'=>true,
                        'class'=>'datepicker'
                    ]);?>
                    <?= $this->Form->input('from_time',[
                        'label'=>false,
                        'class'=>'time-select',
                        'size'=>'5',
                        'requied'=>true
                    ]);?>
                            
                </div>
                <div class="form-block drop-off">
                    <h4><?= __('Drop-off date'); ?></h4>
                    <?= $this->Form->input('to_date',[
                        'label'=>false,
                        'autocomplete'=>'off',
                        'required'=>true,
                        'class'=>'datepicker'
                    ]);?>
                    <?= $this->Form->input('to_time',[
                        'label'=>false,
                        'class'=>'time-select',
                        'size'=>'5',
                        'required'=>true
                    ]);?>        
                </div>
                <div class="form-block car-type">
                    <h4><?= __('Car type'); ?></h4>
                    <div class="car-type-select">
                        <select class="select" name="cartype"> 
                            <option value=""><?= __('All'); ?></option>
                            <?php foreach ($cartypes as $type) :?>
                                <option value="<?= $type->id ?>"><?= $type->{'name_'.$lang} ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-block form-submit form-continue">
                    <?= $this->Form->submit(__('Continue'),[
                        'class'=>'orange_button form-continue'
                        ]);?>
                </div>
            </div>
            <div class="clear"></div>
        <?= $this->Form->end(); ?>
    </div>          
</div><!-- #slider -->
<div id="main">
    <div id="home-carousel">
        <h3 style="margin:0 0 10px 0;font-weight: bold;">
        <?= __('Our Most Polular Cars') ?>
        </h3>
         <div id="content-slider" class="content-slider">
            <?php foreach ($homecarousel->slidercars as $scar) : ?>
                <div class="crsl-item">
                    <div class="owl-img">
                        <?= $this->Html->image("cars/".$scar->car->medias[0]->name);?>
                    </div>
                    <div class="img-overlay">
                        <div class="text-overlay">
                            <?= $scar->car->brand->name." ".$scar->car->model->name ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <br/><br/>
    <div id="primary">
        <div class="clear"></div>
        <div id="content">
            <div class="post">
                <div class="entry-header">
                    <h3 class="entry-format"><?= $hometext[0]->{'title_'.$lang} ?></h3>
                </div>
                <div class="entry-content" style="text-align: justify;">
                    <?= $hometext[0]->{'content_'.$lang} ?>
                </div>
                <div class="entry-meta"><a href="#" title="">Learn more</a></div>
            </div>              
        </div>
        <aside id="secondary">
            <div class="first-column">
                <div class="feature-single">
                    <?= $this->Html->image('icons/1-icon.png', ['class'=>'feature-title-img']) ?>
                    <div class="feature-title"><?= $hometext[1]->{'title_'.$lang} ?></div>
                    <div style="text-align: justify;"><?= $hometext[1]->{'content_'.$lang} ?></div>
                </div>                      
                <div class="feature-single">
                    <?= $this->Html->image('icons/3-icon.png', ['class'=>'feature-title-img']) ?>
                    <div class="feature-title"><?= $hometext[2]->{'title_'.$lang} ?></div>
                    <div style="text-align: justify;"><?= $hometext[2]->{'content_'.$lang} ?></div>
                </div>
            </div>
            <div class="second-column">
                <div class="feature-single">
                    <?= $this->Html->image('icons/2-icon.png', ['class'=>'feature-title-img']) ?>
                    <div class="feature-title"><?= $hometext[3]->{'title_'.$lang} ?></div>
                    <div style="text-align: justify;"><?= $hometext[3]->{'content_'.$lang} ?></div>
                </div>
                <div class="feature-single">
                    <?= $this->Html->image('icons/4-icon.png', ['class'=>'feature-title-img']) ?>
                    <div class="feature-title"><?= $hometext[4]->{'title_'.$lang} ?></div>
                    <div style="text-align: justify;"><?= $hometext[4]->{'content_'.$lang} ?></div>
                </div> 
            </div>
                             
        </aside>
        <div class="clear"></div>
    </div>
    <div id="sign_up_conteiner">
        <form action="#" id="sign_up" >
            <div class="sign_up_text">
                <?= __('Signup for our email to celebrate great specials') ?>
            </div>
            <div class="sign_up_form">
            <?php $alreadyexists = $this->request->session()->read('newsletter');?>

                <input id="sign_up_name" type="text" value="" placeholder="<?= __('Full Name') ?>" name="sign_up_name" <?=  ($alreadyexists == 1) ? 'class="disabled" disabled' : '' ; ?>/>
                <input id="sign_up_email" type="text" value="" placeholder="<?= __('Email') ?>" name="sign_up_email" <?=  ($alreadyexists == 1) ? 'class="disabled" disabled' : '' ; ?>/>
                <input class="orange_button" type="button" value="Signup" <?=  ($alreadyexists == 1) ? 'disabled' : '' ; ?> />
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $(".content-slider").owlCarousel({
                items:4,
                loop:true,
                margin:5,
                dots:true,
                //nav:true,
                autoplay:true,
                autoplayTimeout:2000,
                autoplayHoverPause:true
              });

        <?php if ($lang == 'en') :?>
            $(".datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                numberOfMonths: 2
            });
        <?php else: ?>
            $( ".datepicker" ).datepicker({
            closeText: 'Fermer',
            prevText: 'Précédent',
            nextText: 'Suivant',
            currentText: 'Aujourd\'hui',
            monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
            monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
            dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
            dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
            weekHeader: 'Sem.',
            dateFormat: 'yy-mm-dd',
            numberOfMonths: 2
            });
        <?php endif; ?>
        
        //$(document).click(function(e) { var ele = $(e.toElement); if (!ele.hasClass("hasDatepicker") && !ele.hasClass("ui-datepicker") && !ele.hasClass("ui-icon") && !$(ele).parent().parents(".ui-datepicker").length) $(".hasDatepicker").datepicker("hide"); });

        var availableTags = [
          <?php foreach ($locations as $location) {
              echo '"'.$location->{'name_'.$lang}.'",';
          } ?>
        ];
        $( "#pickup-location" ).autocomplete({
          source: availableTags,
          select: function( event, ui ) {
            var returnlocation = $( "#return-location" ).val();
            if (!returnlocation) {
                $( "#return-location" ).val(ui.item.label);
            }
          }
        }).on('focus', function(event) {
                $(this).autocomplete("search", "a");
            });

        $( "#return-location" ).autocomplete({
          source: availableTags
        }).on('focus', function(event) {
                $(this).autocomplete("search", " ");
            });

        $('.newsletterbtn').on('click', function(){
            //alert('dkhel')
            var fullname = $('#sign_up_name');
            var email = $('#sign_up_email');
            fullname.css('border-color','');
            email.css('border-color','');

            if (fullname.val() && email.val()) {
                $.post('/newsletter/add', {fullname: fullname.val(), email: email.val()})
                .done(function(data){
                    if (data == '0') {
                        fullname.prop( "disabled", true );
                        fullname.css('background-color','#e6e6e6')
                        email.prop( "disabled", true );
                        email.css('background-color','#e6e6e6')
                        $('.newsletterbtn').prop( "disabled", true );
                        alert("<?= __('Your Informations Has been Saved Successfully')?>");
                    }else if(data == '1'){
                        alert("<?= __('The Email')?>'"+" "+email.val()+"' "+"<?= __('Already Exists in our Database')?>");
                    }else{
                        alert("<?= __('An Error Occured While Saving Your Informations, Please Try Again')?>");
                    }
                })
            }else{
                if (!fullname.val()) { fullname.css('border-color','red')}
                if (!email.val()) { email.css('border-color','red')}
            }
        });
    });
</script>
<style type="text/css">
    /*.form-block .datepicker{
        width: 85px !important;
    }*/
    #main{
        /*width: 79% !important;*/
    }
    /*#content{
        width: calc(100% - 730px) !important;
        min-width: 250px !important;
    }*/
</style>