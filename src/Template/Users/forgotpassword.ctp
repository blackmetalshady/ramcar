<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<?php //print_r($reservation); die(); ?>
<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;">
    	<center style="margin-top: 40px; margin-bottom: 30px; margin-left: 20px;">
    		<?= $this->Form->create(null, ['url'=>['controller'=>'Users', 'action'=>'forgotpassword']]);?>
            <span>
            <input type="email" name="email" class="input_placeholder" placeholder="Enter your email" required="required">
            </span>
            <span>
            <input type="submit" class='orange_button form-continue' value="<?= __('Reset Password') ?>">
            </span>
            <?= $this->Form->end() ?>
    	</center>
        <center>
            <div id="status">
                <div id="notice">
                    <?php if ($status == 1){
                        echo __('A Reset Link Has Been Sent to Your E-mail, The Link is Only Valide for <b>24 hours</b>, Please Follow The Link to Set your New Password');
                    }elseif ($status == 2) {
                        echo __('An Error Occured, please Try Again Later');
                    }elseif ($status == 3) {
                        echo __('The E-mail you Provided Doesn\'t Exist in Our Database');
                    }?>
                </div>
            </div>
        </center>
    </div>
</div>
<style type="text/css">
.notice{
    font-weight: bold;
    text-decoration: underline;
}
input.input_placeholder {
    padding-left: 15px;
    width: 220px;
}

#status{
    font-size: 25px;
    color: black;
    width: 80%;
    font-family: initial;
    margin-top: 30px;
    margin-bottom: 40px;
    line-height: 1.2;
}
</style>