<div id="content" class="sidebar-middle" style="margin-left: 20%; width: 60%;margin-top: 15px;">
        <div class="widget main-widget product-widget">
            <center style="margin-top: 30px; margin-bottom: 30px;">
                <span class="title">Create an account</span>
            </center>
            <center><?= $this->Flash->render() ?></center>
            <div class="main-form admin-form form-custom2" style="padding-bottom: 58px;">
            <?= $this->Form->create(null,['url'=>['controller'=>'Users','action'=>'register'],'id'=>'registration_form']) ?>
                <?= $this->Form->input('email',[
                    'label' => [
                        'class' => 'lbl',
                        'text' => __('Email')
                    ],
                    'type'=>'email',
                    'id'=>'register_email',
                    'required' => true,
                    'class'=>'input_placeholder',
                    'placeholder'=>__('Email'),
                    'style'=>'margin-top: 8px;'
                ]);?>
                <?= $this->Form->input('firstname',[
                    'label' => [
                        'class' => 'lbl',
                        'text' => __('First Name')
                    ],
                    'type'=>'text',
                    'id'=>'register_name',
                    'required' => true,
                    'class'=>'input_placeholder',
                    'placeholder'=>__('First Name'),
                    'style'=>'margin-top: 8px;'
                ]);?>
                <?= $this->Form->input('lastname',[
                    'label' => [
                        'class' => 'lbl',
                        'text' => __('Last Name')
                    ],
                    'type'=>'text',
                    'id'=>'register_name',
                    'required' => true,
                    'class'=>'input_placeholder',
                    'placeholder'=>__('Last Name'),
                    'style'=>'margin-top: 8px;'
                ]);?>
                <?= $this->Form->input('password',[
                    'label' => [
                        'class' => 'lbl',
                        'text' => __('Password')
                    ],
                    'type'=>'password',
                    'id'=>'sign_in_pass',
                    'required' => true,
                    'class'=>'input_placeholder',
                    'pattern'=>'.{6,}',
                    'title'=>__('password must be at least 6 characters'),
                    'placeholder'=>__('Password'),
                    'style'=>'margin-top: 8px;'
                ]);?>
                <?= $this->Form->input('confirm_password',[
                    'label' => [
                        'class' => 'lbl',
                        'text' => __('Confirm Password')
                    ],
                    'type'=>'password',
                    'id'=>'sign_in_pass',
                    'pattern'=>'.{6,}',
                    'title'=>__('password must be at least 6 characters'),
                    'required' => true,
                    'class'=>'input_placeholder',
                    'placeholder'=>__('Confirm Password'),
                    'style'=>'margin-top: 8px;'
                ]);?>
                <center>
                    <?= $this->Form->submit(__('Join Us'),[
                    'class'=>'shortcode_button orange_button',
                    'style'=>'width: 150px;margin-bottom: 20px;'
                    ]); ?>
                </center>
            <?= $this->Form->end() ?>
            </div>
        </div>
</div>
<style type="text/css">
    .message.error{
        background-color: #f2dede;
        border-color: #ebccd1;
        color: #a94442;
        max-width: 40%;
        text-align: center;
        border-radius: 5px;
        font-size: 15px;
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .message.success{
        background-color: #dff0d8;
        border-color: #d6e9c6;
        color: #3c763d;
        max-width: 40%;
        text-align: center;
        border-radius: 5px;
        font-size: 15px;
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    .title{
        font-size: 40px;
        text-shadow: 2px 2px grey;
        color: black;
    }
</style>