<div id="content" class="sidebar-middle" style="margin-left: 20%; width: 60%;margin-top: 15px;">
        <div class="widget main-widget product-widget">
        	<center style="margin-top: 15px; margin-bottom: 30px;">
        		<span class="icon-locked overlay-lock"></span>
        	</center>
			<center>
                <?= $this->Flash->render('auth') ?>
                <?= $this->Flash->render() ?>
            </center>
			<div class="main-form admin-form form-custom2" style="padding-bottom: 58px;">
		    <?= $this->Form->create() ?>
		        <?= $this->Form->input('email',[
		        	'label' => [
				        'class' => 'lbl',
				        'text' => __('Email')
				    ],
        			'type'=>'text',
        			'id'=>'register_email',
        			'required' => true,
        			'class'=>'input_placeholder',
        			'placeholder'=>__('Email'),
        			'style'=>'margin-top: 8px;'
        		]);?>
        		<br>
                <?= $this->Form->input('password',[
                	'label' => [
				        'class' => 'lbl',
				        'text' => __('Password')
				    ],
        			'type'=>'password',
        			'id'=>'sign_in_pass',
        			'required' => true,
        			'class'=>'input_placeholder',
        			'placeholder'=>__('Password'),
        			'style'=>'margin-top: 8px;'
        		]);?>

		    	<center>
            		<?= $this->Form->submit(__('Login'),[
            		'class'=>'shortcode_button orange_button',
            		'style'=>'width: 150px;margin-bottom: 20px;'
            		]); ?>
            	</center>
		    <?= $this->Form->end() ?>
            <center><a href="/forgotpassword"><?= __('Forgot your password?') ?></a></center>
		    </div>
        </div>
</div>
<style type="text/css">
    .message.error{
        background-color: #f2dede;
        border-color: #ebccd1;
        color: #a94442;
        max-width: 40%;
        text-align: center;
        border-radius: 5px;
        font-size: 15px;
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>