<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang'); ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<?php //print_r($reservation); die(); ?>
<div id="content" class="sidebar-middle" style="margin-left: 10%; min-width: 80%;margin-top: 15px;">
    <div class="widget main-widget product-widget" style="min-height: 460px;" id="toprint">
    	<div class="pg-title">
		    <h2>
		    	<?= __('Your Reservations'); ?>
		    </h2>
		    <div class="separator"></div>
		</div>
	    <div class="widget-content-range">
	    	<?php if (!empty($cart)) :?>
		    	<table style="width:100%;border: 1px solid #d8d8d8;">
		    		<tr>
		    			<td style="width: 170px;text-indent: 0em !important;">
		    				<?= $this->Html->image('cars/'.$cart['current_car']->medias[0]->name,['style'=>'width: 160px']) ?>
		    			</td>
		    			<td style="width: 150px;background-color: #efefef;">
		    				<?php if (!empty($cart['current_extras'])) :?>
		    					<ul>
		    						<?php foreach ($cart['current_extras'] as $extra) :?>
		    							<li>
		    								<?= $extra->{'name_'.$lang}?>
		    							</li>
		    						<?php endforeach; ?>
		    					</ul>
		    				<?php endif; ?>
		    			</td>
		    			<td style="width: 350px;">
			    			<ul>
			    				<li>
			    					<?= __('Pick-up')." : ".$cart['searchinfo']['pickup_location'] ?>
			    				</li>
			    				<li>
			    					<?= __('Drop-off')." : ".$cart['searchinfo']['return_location'] ?>
			    				</li>
			    			</ul>
		    			</td>
		    			<td style="width: 300px;background-color: #efefef;">
		    				<ul>
			    				<li>
			    					<?= __('on')." : ".$cart['searchinfo']['from_date']." ".__('at')." ".$cart['searchinfo']['from_time'] ?>
			    				</li>
			    				<li>
			    					<?= __('on')." : ".$cart['searchinfo']['to_date']." ".__('at')." ".$cart['searchinfo']['to_time'] ?>
			    				</li>
			    			</ul>
		    			</td>
		    			<td style="text-indent: 0em !important; text-align: center;">
		    				<input type="button" class="shortcode_button red_button" value="<?= __('Update') ?>" style="white-space: normal;" />
		    				<input type="button" class="shortcode_button red_button" value="<?= __('Delete') ?>" style="white-space: normal;" />
		    			</td>
		    		</tr>
		    	</table>
		    <?php else: ?>
		    	<center><?= __('Your Cart is Empty') ?></center>
	    	<?php endif; ?>
	    </div>
	    <?php if (!empty($reservations->toArray())) :?>
	    <h4><?= __('Your Booking History') ?></h4>
	    <div class="widget-content-range">   
	    <?php foreach ($reservations as $reservation): ?>
	    	<table style="width:100%;border: 1px solid #d8d8d8;">
	    		<tr>
	    			<td style="width: 170px;text-indent: 0em !important;">
	    				<?= $this->Html->image('cars/'.$reservation->cars[0]->medias[0]->name,['style'=>'width: inherit; vertical-align: middle;']) ?>
	    			</td>
	    			<td style="width: 150px;background-color: #efefef;">
	    				<?php if (!empty($reservation->extras)) :?>
	    					<ul>
	    						<?php foreach ($reservation->extras as $extra) :?>
	    							<li>
	    								<?= $extra->{'name_'.$lang}?>
	    							</li>
	    						<?php endforeach; ?>
	    					</ul>
	    				<?php endif; ?>
	    			</td>
	    			<td style="width: 350px;">
		    			<ul>
		    				<li>
		    					<?= __('Pick-up')." : ".$reservation->pickup_location ?>
		    				</li>
		    				<li>
		    					<?= __('Drop-off')." : ".$reservation->return_location ?>
		    				</li>
		    			</ul>
	    			</td>
	    			<td style="width: 300px;background-color: #efefef;">
	    				<ul>
		    				<li>
		    					<?= __('on')." : ".$reservation->from_date." ".__('at')." ".$reservation->from_time ?>
		    				</li>
		    				<li>
		    					<?= __('on')." : ".$reservation->to_date." ".__('at')." ".$reservation->to_time ?>
		    				</li>
		    			</ul>
	    			</td>
	    			<td style="text-indent: 0em !important; text-align: center;">
	    				<?php if ($reservation->confirmed == 1) {
	    					echo "<div class='confirmed'>".__('Confirmed')."</div>";
	    				}else if ($reservation->rejected == 1) {
	    					echo "<div class='rejected'>".__('Rejected')."</div>";
	    				}else if ($reservation->canceled == 1) {
	    					echo "<div class='canceled'>".__('Canceled')."</div>";
	    				}else{
	    					echo "<div class='pending'>".__('Pending')."</div>";
	    				} ?>
	    			</td>
	    		</tr>
	    	</table>
	    	<br>
	    <?php endforeach; ?>
	    </div>
		<?php endif; ?>
    </div>
</div>
<style type="text/css">
h4{
	font-size: 15px !important;
	background: #434b56 !important;
    color: #fff !important;
    padding: 12px 20px !important;
}

.widget-content-range {
    color: #000 !important;
    text-align: justify !important;
    font-size: 15px !important;
    word-spacing: 2px !important; 
    margin-bottom: 30px;
    text-indent: 2em;
}
.widget li {
     padding: 0px !important; 
}
td{
	vertical-align: middle;
	text-indent: 0.5em !important;
}
.confirmed,.rejected,.canceled,.pending{
	font-size: 15px;
	font-weight: bold;
}
.confirmed{
	color: green;
}
.rejected, .canceled{
	color: red;
}
.pending{
	color: orange;
}
td>ul>li:before {
    content: "-";
}
td .red_button{
	min-width: 100px;
}
</style>