<div id="content" class="sidebar-middle" style="margin-left: 20%; width: 60%;margin-top: 15px;">
        <div class="widget main-widget product-widget">
        <?= $this->Form->create($user, ['url' => ['action' => 'edit']]);?>
            <div>
                <div class="widget-title" style="margin-bottom: 25px;">
                    <div>
                        <?= $this->Html->image('noImage.png',['style'=>'vertical-align: middle; width: 75px;']);  ?>
                        <div class="profile-title"><?= __('Complete or Edit your Informations') ?></div>
                    </div>
                    <div class="clear"></div>   
                </div>
                <div class="post post-custom" >
                <span class="icon-locked icon-custom"></span>
                <div class="title-custom"><?= __('Account\'s Informations') ?></div>
                <span class="custom-separator" ></span>
	                <div class="main-form admin-form form-custom">
                		<div>
	                		<?= $this->Form->input('email',[
	                			'label'=>false,
	                			'type'=>'text',
	                			'id'=>'register_email',
	                			'required' => true,
	                			'class'=>'input_placeholder',
	                			'placeholder'=>'Email address',
	                		]);?>
	                    </div>
	                    <div>
	                    	<?= $this->Form->input('username',[
	                			'label'=>false,
	                			'type'=>'text',
	                			'id'=>'register_name',
	                			'required' => true,
	                			'class'=>'input_placeholder',
	                			'placeholder'=>'Username',

	                		]);?>
	                    </div>
	                    <div>
	                    	<span style="font-size: 18px;">Password</span>
	                    	<span style="font-size: 18px;margin-left: 10px;margin-right: 10px;vertical-align: bottom;">*******</span>
	                    	<span class="icon-key" id="changepass" rel="#overlay"></span>
	                    </div>
	                </div>
                    <div class="clear"></div>
                </div>
                <div class="post post-custom" >
					<span class="icon-user icon-custom"></span>
	                <div class="title-custom">User's Informations</div>
	                <span class="custom-separator"></span>
	                    <div class="main-form admin-form form-custom">
	                		<div>
		                    	<?= $this->Form->input('firstname',[
		                			'label'=>false,
		                			'type'=>'text',
		                			'class'=>'shortcode_input',
		                			'placeholder'=>'First Name',

		                		]);?>
		                    </div>
		                    <div>
		                    	<?= $this->Form->input('lastname',[
		                			'label'=>false,
		                			'type'=>'text',
		                			'class'=>'shortcode_input',
		                			'placeholder'=>'Last Name',

		                		]);?>
		                    </div>
		                    <div>
		                    	<h4>Age</h4>
		                    	<select class="custom-select" style="max-width: 50px;display: block;" name="age">
									<?php for($i=16; $i<=60; $i++): ?>
										<?php if ($user->age == $i): ?>
											<option selected="selected"><?= $i; ?> </option>
										<?php else: ?>	
											<option><?= $i; ?> </option>
										<?php endif ?>
									<?php endfor; ?>
								</select>
		                    </div>
		                    <div style="margin-top: 15px;">
		                    	<span>Country</span>
		                    	<select name="country" id="countries" style="width:300px;">
		                    	  <?php foreach ($countries as $country) :?>
		                    	  	<?php if ($country->id == $user->country): ?>
		                    	  		<option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>" selected><?= $country->name; ?></option>
		                    	  	<?php else: ?>
		                    	  		<option value='<?= $country->id; ?>' data-image="/img/blank.gif" data-imagecss="flag <?= $country->code; ?>" data-title="<?= $country->name; ?>"><?= $country->name; ?></option>
		                    	  	<?php endif ?>
		                    	  <?php endforeach; ?>
								</select>
		                    </div>
		                </div>
	                    <div class="clear"></div>
                </div>
                <div class="post post-custom">
					<span class="icon-car icon-custom"></span>
	                <div class="title-custom">Driver's Informations</div>
	                <span class="custom-separator"></span>
	                    <div class="main-form admin-form form-custom">
	                		<div>
	                			<?= $this->Form->input('driverslicense',[
		                			'label'=>false,
		                			'type'=>'text',
		                			'class'=>'shortcode_input',
		                			'placeholder'=>'Driver\'s License',
		                		]);?>
		                    </div>
	                	</div>
                    <div class="clear"></div>
                </div>
                <div class="post post-custom">
                	<center>
                		<?= $this->Form->submit(__('Update'),[
                		'class'=>'shortcode_button orange_button',
                		'style'=>'width: 150px;margin-bottom: 20px;'
                		]); ?>
                	</center>
                </div>
            </div>   
            <?= $this->Form->end();?>
        </div>
    </div>  
<div class="simple_overlay" id="overlay">
	<div class="details">
 		<center><span class="icon-locked overlay-lock"></span></center>
 		<br>
 		<center><span style="font-size: 30px"><?= __('Change your Password') ?></span></center>
 	</div>
  <!-- image details -->
  <div class="details">
    <center>
    	<div class="main-form admin-form form-custom2">
    		<div id="successmsg-box" class="allmsg-box"></div>
    		<div id="errmsg-box" class="allmsg-box"></div>
    		<div>
        		<?= $this->Form->input('password',[
        			'label'=>false,
        			'type'=>'password',
        			'id'=>'sign_in_pass',
        			'required' => true,
        			'class'=>'input_placeholder pass',
        			'placeholder'=>__('Old Password'),
        		]);?>
            </div>
            <div>
        		<?= $this->Form->input('newpassword',[
        			'label'=>false,
        			'type'=>'password',
        			'id'=>'sign_in_pass',
        			'required' => true,
        			'class'=>'input_placeholder pass',
        			'placeholder'=>__('New Password'),
        		]);?>
            </div>
            <div>
        		<?= $this->Form->input('renewpassword',[
        			'label'=>false,
        			'type'=>'password',
        			'id'=>'sign_in_pass',
        			'required' => true,
        			'class'=>'input_placeholder pass',
        			'placeholder'=>__('Retype New Password'),
        		]);?>
            </div>
            <center>
        		<?= $this->Form->submit(__('Change Password'),[
        		'class'=>'shortcode_button orange_button',
        		'id'=>'changepassbtn',
        		'style'=>'min-width: 200px;margin-bottom: 20px;'
        		]); ?>
        	</center>
        </div>
    </center>
  </div>
</div>
<script>
$(document).ready(function() {
	$("#countries").msDropdown();
	$("#changepass[rel]").overlay({mask2: '#000'});
	$("#changepassbtn").click(function(){changepass();});
});
</script>