<div id="content" class="sidebar-middle" style="margin-left: 20%; width: 60%;margin-top: 15px;">
    <div class="widget main-widget product-widget">
    <?php if ($status == 0) :?>
    <?= $this->Form->create(null, ['url' => ['action' => 'resetpassword']]);?>
            <div class="widget-title" style="margin-bottom: 25px;">
                <div>
                    <span class="icon-locked icon-custom"></span>
                    <div class="profile-title"><?= __('Reset Your Password') ?></div>
                </div>
                <div class="clear"></div>   
            </div>
            <center>
	            <div class="post post-custom" >
	                <div class="main-form admin-form form-custom">
	            		<div>
	                		<?= $this->Form->input('oldpassword',[
	                			'label'=>false,
	                			'type'=>'password',
	                			'id'=>'sign_in_pass',
	                			'required' => true,
	                			'class'=>'input_placeholder pass',
	                			'placeholder'=>'Old Password',
	                		]);?>
	                    </div>
	                    <div>
	                    	<?= $this->Form->input('newpassword',[
	                			'label'=>false,
	                			'type'=>'password',
	                			'id'=>'sign_in_pass',
	                			'required' => true,
	                			'class'=>'input_placeholder pass',
	                			'placeholder'=>'New Password',

	                		]);?>
	                    </div>
	                    <div>
	                    	<?= $this->Form->input('renewpassword',[
	                			'label'=>false,
	                			'type'=>'password',
	                			'id'=>'sign_in_pass',
	                			'required' => true,
	                			'class'=>'input_placeholder pass',
	                			'placeholder'=>'Retype your New Password',

	                		]);?>
	                    </div>
	                </div>
	                <div class="clear"></div>
	            </div>
	            <div class="post post-custom">
                	<center>
                		<?= $this->Form->submit(__('Update'),[
                		'class'=>'shortcode_button orange_button form-update',
                		'style'=>'width: 130px;padding-right: 40px;'
                		]); ?>
                	</center>
                </div>
            </center> 
        <?= $this->Form->end();?>
    <?php else : ?>
    	<center>
    		<?= $this->Html->image('sad.jpg',['style'=>'width: 170px;margin-top: 60px;margin-bottom: 30px;']); ?>
    	</center>
		<div id="msg">
			<center>
				<?= __('The link is invalid or your request has expired, if you wish to reset you password please follow ').$this->Html->link(__('this link'), ['controller'=>'Users','action'=>'forgotpassword']); ?>
			</center>
		</div>
    <?php endif; ?>
        <br><br>
    </div>

</div>  
<style type="text/css">
	.form-custom{
		margin-left: auto !important;
    	margin-right: auto !important;
	}
	.profile-title {
		vertical-align: baseline !important;
		font-size: 30px;
	}
	.icon-custom{
		color: #000 !important;
		font-size: 60px !important;
	}
	.main-widget{
		min-height: 460px;
		/*position: relative;*/
	}
	#msg{
		/*position: absolute;
	    top: 50%;
	    transform: translateY(-50%);*/
	    width: 80%;
	    margin-left: 10%;
	    font-size: 23px;
	    line-height: 150%;
	    color: #000;
	    text-transform: capitalize;
	}
</style>