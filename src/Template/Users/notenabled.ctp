<div id="content" class="sidebar-middle" style="margin-left: 20%; width: 60%;margin-top: 15px;">
    <div class="widget main-widget product-widget">

        <center>
            <?= $this->Html->image('sad.jpg',['style'=>'width: 170px;margin-top: 60px;margin-bottom: 30px;']); ?>
        </center>
        <div id="msg">
            <center>
                <?= __('This account has been disabled, if you wish to enable your account, please fill your email above, verify your email and follow the enabling link'); ?>
            </center>
        </div>
        <div id="enableform">
            <?= $this->Form->create(null, ['url'=>['controller'=>'Users','action'=>'enable']]); ?>
            <?= $this->Form->input('confirm_password',[
                    'label' => false,
                    'type'=>'email',
                    'id'=>'register_email',
                    'required' => true,
                    'class'=>'input_placeholder',
                    'placeholder'=>__('Email'),
                    'style'=>'margin-top: 8px;'
                ]);?>
            <?= $this->Form->end() ?>
        </div>
        <br><br>
    </div>

</div>  
<style type="text/css">
    .main-widget{
        min-height: 460px;
    }

    #msg{
        width: 80%;
        margin-left: 10%;
        font-size: 23px;
        line-height: 150%;
        color: #000;
        text-transform: capitalize;
    }
</style>