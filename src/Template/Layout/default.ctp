<?php use Cake\I18n\I18n; ?>
<?php $lang = $this->request->session()->read('lang');
$sessionData = $this->request->session()->read('Auth.User');
 ?>
<?php if ($lang == 'fr') {
    I18n::locale('fr_FR');
} ?>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title><?= __('RamCar - a Car Rental Agency at Marrakech'); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <?= $this->Html->css(['jquery-ui.css','style.css','jquery.slider.min.css','custom.css','icomoon.css','dd.css','flags.css','font-awesome.min.css','lightgallery.css','lightslider.css','lg-transitions.css','owl.carousel.css']) ?>

        <!--[if IE]>
        <script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" id="stylesheet-ie" href="css/css_ie.css" type="text/css" media="all" />
        <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?= $this->Html->script(['jquery-2.2.3.min.js','jquery.cookie.js']); ?>
    <style type="text/css">
        <?php if ($lang == 'fr') :?>
            .language .sbHolder{
                background:url(/img/icons/fr.png) no-repeat right bottom #f7f7f7;
                border:1px solid #d2d2d2;
                margin:0 0 0 8px;
            }
            <?php endif; ?>

            <?php if($lang == 'fr'):?>
            a.sign_button{
                font-size: 12px;
            }
            <?php endif; ?>
            a.cart:link{
                color: #ef7c37;
                text-decoration: none;
            }
            a.cart:visited{
                color: #ef7c37;
                text-decoration: none;
            }
            a.cart:hover{
                color: #ef7c37;
                text-decoration: none;
                zoom: 1.1;
            }
            a.cart:active{
                color: #ef7c37;
                text-decoration: none;
            }
        </style>
    </head>
    <?php //print_r($this->request->params); die(); ?>
    <body class="<?= ($this->request->params['controller'] == 'Pages' && $this->request->params['action'] == 'display') ? 'left-slider two-column' : 'page page-two-column' ;?>">
        <div id="conteiner">
            <div id="branding">
                <div id="branding-content">
                    <div class="title-content">
                        <?= $this->Html->image("logo.jpg", [
                            'alt' => "RamCar",
                            'class'=>'logo',
                            'url' => ['controller' => 'Pages', 'action' => 'display', 'home']
                        ]);?>
                    </div>                  
                    <div class="access-content">
                        <label for="menu-icon" class="menu-icon">
                            <?= $this->Html->image('icons/menu.png') ?>
                        </label>
                        <input type=checkbox id=menu-icon value=1 />
                        <ul>
                            <li class="menu-close">
                                <span>✖</span>
                            </li>
                            <li id="home"><?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display', 'home']); ?></li>
                            <li id="cars"><?= $this->Html->link(__('Cars'), ['controller'=>'Cars', 'action'=>'index'])  ?></li>
                            <li id="extras"><?= $this->Html->link(__('Extras'), ['controller'=>'Extras', 'action'=>'index'])  ?></li>
                            <li id="prices"><?= $this->Html->link(__('Pricing'), ['controller'=>'Prices', 'action'=>'index'])  ?></li>
                            <li id="termsconditions"><?= $this->Html->link(__('Terms & Conditions'), ['controller'=>'Infos', 'action'=>'terms']); ?></li>
                            <li id="contactus"><?= $this->Html->link(__('Contact Us'), ['controller'=>'Infos', 'action'=>'contactus']); ?></li>
                        </ul>
                    </div><!-- .access -->          
                    <div class="menus-content"> 
                        <div class="menus" style="float: right; margin-right: <?= ($lang =='fr') ? '25' : '15' ;?>%;">                         
                            <div class="language">
                                <select class="select" name="select_language" onchange="changelang(this.value)"> <?php foreach ($languages as $k => $v) {
                                    if ($k == $lang) {
                                        echo "<option value=\"".$k."\" selected>".$v."</option>";
                                    }else{
                                        echo "<option value=\"".$k."\">".$v."</option>";
                                    }
                                } ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="member">
                            <?php if (isset($sessionData['id']) && !empty($sessionData['id'])): ?>
                                <div class="user" style="margin-top: 20px;">
                                    <?= $this->Html->image('user.png',['style'=>'vertical-align: middle; width: 30px;']);  ?>
                                    <span style="vertical-align: middle; font-weight: bold;">
                                        <?= $this->Html->link(ucfirst($sessionData['firstname'])." ".ucfirst($sessionData['lastname']), ['controller'=>'Users', 'action'=>'profile'],['class'=>'showsub']); ?>
                                        <ul class="children">
                                            <li>
                                                <?= $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> '.__('My Profile'),['controller'=>'Users', 'action'=>'profile'],['escape'=>false]); ?>
                                            </li>
                                            <!--<li>
                                                <?= $this->Html->link('<i class="icon-envelope" aria-hidden="true"></i> '.__('My Messages'),['controller'=>'Messages', 'action'=>'index'],['escape'=>false]); ?>
                                            </li> -->
                                            <li>
                                                <?= $this->Html->link('<i class="fa fa-shopping-cart" aria-hidden="true"></i> '.__('Cart'),['controller'=>'Users', 'action'=>'cart'],['escape'=>false]); ?>
                                            </li>
                                            <li>
                                                <?= $this->Html->link('<i class="fa fa-sign-out" aria-hidden="true"></i> '.__('Logout'),['controller'=>'Users', 'action'=>'logout'],['escape'=>false]); ?>
                                            </li>
                                        </ul>
                                    </span>
                                </div>
                            <?php else: ?>
                                <span class="sign_in"><a class="sign_button tab_link_button" style="cursor: pointer;"><?= __('Sign in') ?></a></span>
                                <span class="register"><a class="sign_button tab_link_button" style="cursor: pointer;"><?= __('Register') ?></a></span>
                                <br>
                                <?php if (!empty($this->request->session()->read('Cart.car_id'))) :?>
                                    <div id="cart"><?= $this->Html->link('<i class="icon-shopping-cart"></i>',['controller'=>'Cars','action'=>'review'],['class'=>'cart','escape'=>false]) ?></div>
                                <?php endif; ?>
                            <?php endif ?>
                        </div>
                    </div>
                </div><!-- #branding-content -->
                <div class="clear"></div>
            </div><!-- #branding -->
            <?= $this->fetch('content') ?>
            <div class="clear"></div>
            <footer id="footer"> 
                <div id="footer-menu"> 
                <div class="footer-content"> 
                    <div class="footer-navbar"> 
                        <div class="footer-nav"> 
                            <h3 class="widget-title">Ramcar</h3>
                                <ul>
                                    <li>
                                        <?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display', 'home']); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Cars'), ['controller'=>'Cars', 'action'=>'index'])  ?></li>
                                    <li>
                                        <?= $this->Html->link(__('Extras'), ['controller'=>'Extras', 'action'=>'index'])  ?>
                                    </li>
                                </ul>
                        </div> 
                        <div class="footer-nav"> 
                            <h3 class="widget-title"><?= __('About') ?></h3>
                                <ul>
                                    <li><a href="#" title="">RamCar</a></li>
                                    <li>
                                        <?= $this->Html->link(__('Pricing'), ['controller'=>'Prices', 'action'=>'index'])  ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Terms & Conditions'), ['controller'=>'Infos', 'action'=>'terms']); ?>
                                    </li>
                                </ul>
                        </div> 
                        <div class="footer-nav"> 
                            <h3 class="widget-title"><?= __('Support')?></h3>
                                <ul>
                                    <li>
                                        <?= $this->Html->link(__('Contact Us'), ['controller'=>'Infos', 'action'=>'contactus']); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Your Booking'), ['controller'=>'Reservations', 'action'=>'status']); ?>
                                    </li>
                                    <li><a href="" title="">FAQ</a></li>
                                </ul>
                        </div> 
                    </div> 
                <div class="widget-area"> 
                    <div class="recent_tweets footer-widget-single"> 
                        <h3>Recent tweets</h3> 
                        <div class="recent_tweets_content">
                            <a href="#" title="">@bestwebsoft</a> 
                            velit, vitae tincidunt orci. Proin vitae auctor lectus.
                        </div> 
                        <div class="recent_tweets_url">
                            <a href="#" title="">http://bestwebsoft.com</a>
                        </div> 
                        <div class="recent_tweets_time">posted 2 days ago</div> 
                    </div> 
                </div> 
                <div class="custom-info footer-widget-single"> 
                    <div class="support"> 
                        <?= $this->Html->image('xsupport-icon.png'); ?>
                        <div> 
                            <div class="title"><?= __('Online Support') ?></div> 
                            <div class="phone">+212 524-303-100</div> 
                            <div class="email">
                                <a href="#" title="">info@ramcarloc.com</a>
                            </div> 
                            <div class="clear"></div> 
                        </div> 
                        <div class="clear"></div> 
                    </div>  
                </div> 
                <div class="clear"></div> 
                </div> 
                </div> 
                <div id="footer-content"> 
                    <?= $this->Html->image('icons/car.png',['class'=>'site-logo'])?>
                    <h1 class="site-title">Ramcar</h1> 
                    <div class="company-name">N°2, Imm. 4, Complex Habssi, Bord Hadji Av Allal El Fassi Marrakech</div> 
                </div> 
            </footer>
            
            <!-- <footer id="footer">            
                <div id="footer-menu">
                    <div class="footer-content"> 
                        <div class="footer-navbar">
                            <div class="footer-nav">
                                <h3 class="widget-title">Ramcar</h3>
                                <ul>
                                    <li>
                                        <?= $this->Html->link(__('Home'), ['controller' => 'Pages', 'action' => 'display', 'home']); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Cars'), ['controller'=>'Cars', 'action'=>'index'])  ?></li>
                                    <li>
                                        <?= $this->Html->link(__('Extras'), ['controller'=>'Extras', 'action'=>'index'])  ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer-nav">
                                <h3 class="widget-title"><?= __('About') ?></h3>
                                <ul>
                                    <li><a href="#" title="">RamCar</a></li>
                                    <li>
                                        <?= $this->Html->link(__('Pricing'), ['controller'=>'Prices', 'action'=>'index'])  ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Terms & Conditions'), ['controller'=>'Infos', 'action'=>'terms']); ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer-nav">
                                <h3 class="widget-title">Support</h3>
                                <ul>
                                    <li>
                                        <?= $this->Html->link(__('Contact Us'), ['controller'=>'Infos', 'action'=>'contactus']); ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link(__('Your Booking'), ['controller'=>'Reservations', 'action'=>'status']); ?>
                                    </li>
                                    <li><a href="" title="">FAQ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget-area">
                            <div class="recent_tweets footer-widget-single"> 
                                <h3>Recent tweets</h3> 
                            <div class="recent_tweets_content">
                                <a href="#" title="">@bestwebsoft</a> 
                                velit, vitae tincidunt orci. Proin vitae auctor lectus.
                            </div> 
                            <div class="recent_tweets_url">
                                <a href="#" title="">http://bestwebsoft.com</a>
                            </div> 
                            <div class="recent_tweets_time">posted 2 days ago</div> 
                            </div>

                             <div class="recent_tweets">
                                <h3>Recent tweets</h3>
                                <div class="recent_tweets_content">
                                    <a href="#" title="">@Vincent</a> 
                                    Tres bon service est voiture comme decrite
                                </div>
                                <div class="recent_tweets_url">
                                    <a href="#" title="">http://ramcarloc.com</a>
                                </div>
                                <div class="recent_tweets_time">posted 2 days ago</div>                         
                            </div> 
                        </div>
                        <div class="custom-info footer-widget-single"> 
                            <div class="support"> 
                                <?= $this->Html->image('xsupport-icon.png'); ?>
                            </div> 
                            <div class="title"><?= __('Online Support') ?></div> 
                            <div class="phone">+212 524-303-100</div> 
                            <div class="email">
                                <a href="#" title="">info@ramcarloc.com</a>
                            </div> 
                            <div class="clear"></div> 
                        </div> 
                    </div>
                </div>
                <div id="footer-content">
                    <?= $this->Html->image('icons/car.png',['class'=>'site-logo'])?>
                    <h1 class="site-title">Ramcar</h1>           
                    <div class="company-name">N°2, Imm. 4, Complex Habssi, Bord Hadji Av Allal El Fassi Marrakech</div>
                </div> #footer-content -->
            <!--</footer>end:footer-->
        </div><!--end:conteiner-->
        <div id="overlay_block"></div>
        <div class="admin-form-content sign_register_block">
            <div class="admin-form-block">
                <div class="main-form admin-form form" >    
                    <div class="main_form_navigation">
                        <div id="tab_register" class="title-form back"><span class="register"><a style="cursor: pointer;font-weight: bold;"><?= __('Register') ?></a></span></div>                
                        <div id="tab_sign_in" class="title-form current"><span class="sign_in"><a style="cursor: pointer;font-weight: bold;"><?= __('Sign In') ?></a></span></div> 
                    </div>          
                    <div id="tab_sign_in_content" class="content-form">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'login']]); ?>
                       
                            <input id="sign_in_email" class="input_placeholder" type="text" placeholder="Email" name="email" />
                        
                            <input id="sign_in_pass" type="password" value="" name="password" />
                        
                        <div>
                            <input id="remember_me_checkbox" type="checkbox" class="styled" name="remember_me" value="1" />
                            <label for="remember_me_checkbox"> <?= __('Remember me next time') ?></label>
                        </div>
                        <input class="admin-form-submit orange_button" type="submit" value="Continue" />
                        <div class="admin_form_link">
                            <span class="forgot_passwd">
                                <a class="tab_link_button" href="#forgot_passwd" title="">
                                    <?= __('Forgot password?'); ?>
                                </a>
                            </span>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                    <div id="tab_register_content" class="content-form hidden">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'Users', 'action' => 'register']]); ?>
                        <div>
                            <input id="register_email" class="input_placeholder" type="email" value="" placeholder="<?=__('Email address') ?>" name="email" required />
                        </div>
                        <div>
                            <input id="register_firstname" type="text" value="" name="first_name" placeholder="<?= __('First Name')?>" required/>
                        </div>
                        <div>
                            <input id="register_lastname" type="text" value="" name="last_name" placeholder="<?= __('Last Name')?>" required/>
                        </div>
                        <div>
                            <input id="sign_up_pass" type="password" value="" name="password" placeholder="<?=__('Password') ?>" required/>
                        </div>
                        <div>
                            <input id="sign_up_pass_confirm" type="password" value="" name="confirm_password" placeholder="<?=__('Confirm Password') ?>" required />
                        </div>
                        <input class="admin-form-submit orange_button" type="submit" value="Continue" />
                        <div class="admin_form_link">
                            <span class="sign_in">
                                <a class="tab_link_button" href="#sign_in" title="">
                                    <?= __('Already registered?') ?>
                                </a>
                            </span>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                    <div class="clear"></div>
                </div>             
            </div>
        </div>
        <div class="admin-form-content forgot_passwd_block"> 
            <div class="admin-form-block">
                <?= $this->Form->create(null, ['url'=>['controller'=>'Users', 'action'=>'forgotpassword'],'class'=>'main-form admin-form']);?>   
                    <div class="main_form_navigation">
                        <div id="tab_forgot_passwd" class="title-form current"><?= __('Forgot Password') ?>?</div>   
                    </div>                          
                    <div id="tab_forgot_passwd_content" class="content-form">
                        <input id="forgot_pass_email" class="input_placeholder" type="email" value="" placeholder="Email address" name="email" required />
                        <div id="forgot_pass_text"><?= __('We will send you a link to reset your password in few minutes.') ?></div>
                        <input class="admin-form-submit orange_button" type="submit" value="Continue" />
                        <div class="admin_form_link">
                            <span class="sign_in"><a class="tab_link_button" href="#sign_in" title=""><?= __('Sign In') ?></a></span>
                        </div>
                    </div>
                    <div class="clear"></div>
                <?= $this->Form->end() ?>
                <!-- </form> -->
            </div>
        </div>
        <?= $this->Html->script(['jquery-migrate-1.2.1.min.js','jquery-ui.js','placeholder_ie.js','jquery.slider.min.js','jquery.meio.mask.js','custom-form-elements.js','jquery.selectbox-0.2.min.js','script.js','jquery.dd.min.js','jquery.tools.min.js','lightgallery.js','lg-fullscreen.js','lg-thumbnail.js','lg-zoom.js','lightslider','custom.js','owl.carousel.js']) ?>
        <script type="text/javascript">//<![CDATA[
            (function($){$.fn.extend({blueberry:function(options){var defaults={interval:5000,duration:500,lineheight:1,height:'auto',hoverpause:false,pager:true,nav:true,keynav:true}
            var options=$.extend(defaults,options);return this.each(function(){var o=options;var obj=$(this);var slides=$('.slides li',obj);var pager=$('.pager li',obj);var current=0;var next=current+1;var imgHeight=slides.eq(current).find('img').height();var imgWidth=slides.eq(current).find('img').width();var imgRatio=imgWidth/imgHeight;var sliderWidth=0;var cropHeight=0;slides.hide().eq(current).fadeIn(o.duration).addClass('active');if(pager.length){pager.eq(current).addClass('active');}else if(o.pager){obj.append('<ul class="pager"></ul>');slides.each(function(index){$('.pager',obj).append('<li><a href="#"><span>'+index+'</span></a></li>')});pager=$('.pager li',obj);pager.eq(current).addClass('active');}
            if(pager){$('a',pager).click(function(){clearTimeout(obj.play);next=$(this).parent().index();rotate();return false;});}
            var rotate=function(){slides.eq(current).fadeOut(o.duration).removeClass('active').end().eq(next).fadeIn(o.duration).addClass('active').queue(function(){clearTimeout(obj.play);rotateTimer();$(this).dequeue()});if(pager){pager.eq(current).removeClass('active').end().eq(next).addClass('active');}
            current=next;next=current>=slides.length-1?0:current+1;};var rotateTimer=function(){obj.play=setTimeout(function(){rotate();},o.interval);};rotateTimer();if(o.hoverpause){slides.hover(function(){clearTimeout(obj.play);},function(){rotateTimer();});}
            var setsize=function(){sliderWidth=$('.slides',obj).width();cropHeight=Math.floor(((sliderWidth/imgRatio)/o.lineheight))*o.lineheight;$('.slides',obj).css({height:cropHeight});};setsize();$(window).resize(function(){setsize();});if(o.keynav){$(document).keyup(function(e){switch(e.which){case 39:case 32:clearTimeout(obj.play);rotate();break;case 37:clearTimeout(obj.play);next=current-1;rotate();break;}});}});}});})(jQuery);
            //]]>
        </script>
    </body>
</html>