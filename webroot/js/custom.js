$(function() {

	var url = window.location;
    var element = $('.access-content ul li a').filter(function() {
        return this.href == url;
    }).parent();

    if (element.is('li')) {
        element.addClass('current_page_item');
    }
    //console.log(url);
    if (url.pathname == '/cars') {
        $('li#cars').addClass('current_page_item');
    }else if (url.pathname == '/prices') {
        $('li#prices').addClass('current_page_item');
    }else if (url.pathname == '/extras') {
        $('li#extras').addClass('current_page_item');
    }
})

$(function() {
$(document).on('keyup mouseleave', '#from-time',function(){
                $('#to-time').val($(this).val());
            });

$("#sendmsg").click(function() {
	    $('html, body').animate({
	        scrollTop: $("#msgform").offset().top
	    }, 1000);
	});
})

$(".datepicker").on('blur', function(e){$(".datepicker").datepicker("hide")});

$(document).on('click','#submitreq', function(){
    $('form#anonform input').removeClass('shortcode_input_required');
    var stat = true;
    $.each($('form#anonform input'), function(){
        //console.log($(this).val());
        if (!$(this).val()) {
            $(this).addClass('shortcode_input_required');
            stat = false;
            return stat;
        }
    });
    
    if (stat) {
        var email = $('input[name=email]');
        var confirm_email = $('input[name=confirm_email]');

        if (email.val() != confirm_email.val()) {
            email.addClass('shortcode_input_required');
            confirm_email.addClass('shortcode_input_required');
        }else{
            $('form#anonform').submit();
        }
    }
});

$('.product-img').on('click', function(){
    var id = $( this ).data('id');
    $( "#gallery"+id+" a:first-child" ).click();
 });

 $(document).on('blur','.time-select', function(){
    var regexp = /([01][0-9]|[02][0-3]):[0-5][0-9]/;
    if($(this).val() && !regexp.test($(this).val())){
        var regexp2 = /([01][0-9]|[02][0-3]):[0-5]/;
        if (regexp2.test($(this).val())) {
            var newval = $(this).val()+'0'
        }else{
            var newval = $(this).val()+':00'
        }
        
        $('.time-select').val(newval);
    }
});

$(document).on('click', '#confirmres', function(){
    //console.log('dkhel');
    $('form#anonform').submit();
});
$('a#rev_sign').on('click', function(){
    $('.sign_in>a.sign_button').click();
});
$('a#rev_register').on('click', function(){
    $('.register>a.sign_button').click();
});
$(document).on('click','.changecar', function(){
    console.log('ana kayna f custom.js')
});

$('.gl').lightGallery({
        thumbnail:true
    }); 

$('.product-img').on('click', function(){
    var id = $( this ).data('id');
    $( "#gallery"+id+" a:first-child" ).click();
})

$('.check').on('click', function(){
    checkavalaibility();
});

function checkavalaibility(){
    $('html, body').animate({
        scrollTop: $("#slider-form").offset().top
    }, 
    {
        duration: 500,
        complete: function() {
            $( "#slider-form" ).animate({
              backgroundColor: "rgba(255, 224, 0, 0.28)"
            }, 600 );
            setTimeout(function(){
                $( "#slider-form" ).animate({
                  backgroundColor: "#fff"
                }, 2000 );
            }, 800);
        }
    });
}

function changelang(val){
    $.post('/cars/lang',{lang: val})
        .done(function( data ) {
            if (data == "success") {
                location.reload();
            }else{
                alert('erreur : '+data);
            }
        });
}