(function($){$(window).load(function(){"use strict";if($.fn.blueberry){$('#slider-img').blueberry();};});$(document).ready(function(){"use strict";$(".title-form").click(function(){var this_id=$(this).attr('id');$('.form').filter(':first').children('.main_form_navigation').children('.title-form').addClass('back').removeClass('current');$(this).addClass("current").removeClass('back');$('.form').filter(':first').children(".content-form").addClass("hidden");$("#"+this_id+"_content").removeClass('hidden');});if($.browser.msie){$('.location, #sign_up_name, #sign_up_email, .form_element input, .shortcode_input, .input_placeholder').placeholder();}

$(".menu-icon").on("click",function(){
	if(!$(".access-content > ul").is(":visible")){
		$(".access-content > ul").css("display","block")}
	});
$(".menu-close span").click(function(){
	$("#menu-icon").removeAttr("checked");
	$(".access-content > ul").css("display","none")
});

if($(".menu-icon").is(":visible")){
	$(".access-content > ul").height($("body").height())
}else{
	$(".access-content > ul").height("auto")
};

$(window).resize(function(){
	if($(".menu-icon").is(":visible")){
		$(".access-content > ul").height($("body").height())
	}else{
		$(".access-content > ul").height("auto")
	}
})

$(".select").selectbox();$(".select:disabled").selectbox("disable");

 if ($.cookie('lang') && $.cookie('lang') == 'fr') {
    var currency = '€';
    var maxprice = 180;
 }else{
    var currency = '$';
    var maxprice = 180;
 }
//filter by price range
 $(".price_range").slider({
    from:0,
    to:maxprice,
    step:10,
    dimension:'&nbsp;'+currency,
    callback: function( value ){
        $(".content-overlay").css('display','block')
                 .css('height',$('.product-widget > .frm').height())
                 .css('width',$('.product-widget > .frm').width());
        $(".content-overlay > img").css('display','block')
                                   .css('margin-top',$('.product-widget > .frm').height()/2-33)
                                   .css('margin-left',$('.product-widget > .frm').width()/2-33);
        
        setTimeout(function(){
            var vals = value.split(";")
            $.each($('.post'), function(){
                if($(this).data('price') < vals[0] || $(this).data('price') > vals[1]){
                    $(this).hide();
                }else{
                    $(this).show();
                }
            });
        },390);


        setTimeout(function(){
            $(".main-widget .close-details").css('display','none');
            $('.main-widget .view-details').css('display','block');
            $('.main-widget .details-more').css('display','none');
            $(".content-overlay").css('display','none');},400);
      }
});
// Filter by passenger number
 $(".passangers_range").slider({
    from:1,
	to:5,
	step:1,
    dimension:'',
    callback: function( value ){
        $(".content-overlay").css('display','block')
                 .css('height',$('.product-widget > .frm').height())
                 .css('width',$('.product-widget > .frm').width());
        $(".content-overlay > img").css('display','block')
                                   .css('margin-top',$('.product-widget > .frm').height()/2-33)
                                   .css('margin-left',$('.product-widget > .frm').width()/2-33);
        
        setTimeout(function(){
            var vals = value.split(";")
            $.each($('.post'), function(){
                if($(this).data('passengers') < vals[0] || $(this).data('passengers') > vals[1]){
                    $(this).hide();
                }else{
                    $(this).show();
                }
            });
        },390);


        setTimeout(function(){
            $(".main-widget .close-details").css('display','none');
            $('.main-widget .view-details').css('display','block');
            $('.main-widget .details-more').css('display','none');
            $(".content-overlay").css('display','none');},400);
      }
});

$(document).on('click','a#print',function(){
	print('toprint');
});

$(".shortcode_range").slider({to:5,step:1,dimension:''});
$(".details-more").css('display','none');$(".view-details").click(function(){$(this).css('display','none');$(this).closest('.main-block').find('.close-details').css('display','block');$(this).closest('.main-block').find('.details-more').css('display','block');});$(".close-details").click(function(){$(this).css('display','none');$(this).closest('.main-block').find('.view-details').css('display','block');$(this).closest('.main-block').find('.details-more').css('display','none');});$(".details div").hover(function(){$(this).css('color','#EE7835');},function(){$(this).css('color','#378EEF');});$("#overlay_block").css('height',$(document).height());$(".admin-form-content").click(function(event){if($(event.target).closest(".admin-form-block").length)return;$("#overlay_block").css('display','none');$(".admin-form-content").css('display','none');event.stopPropagation();});var anc=window.location.hash.replace("#","");if(anc!=""){Display_tab_div(anc);}
$(".tab_link_button").click(function(){$("#overlay_block").css('display','block');var this_id=$(this).parent('span').attr('class').toLowerCase().replace(' ','_');if(this_id=='forgot_passwd'){$(".admin-form-content").css('display','none');$(".forgot_passwd_block").css('display','block');}else{$(".admin-form-content").css('display','none');$(".sign_register_block").css('display','block');}
$('.admin-form-block .title-form').addClass('back').removeClass('current');$(".admin-form-block .main_form_navigation #tab_"+this_id).addClass("current").removeClass('back');$('.admin-form-block .content-form').addClass("hidden");$('.admin-form-block #tab_'+this_id+"_content").removeClass('hidden');});if($.fn.setMask){$(".time-select").setMask("29:59").keypress(function(){var currentMask=$(this).data('mask').mask;var newMask=$(this).val().match(/^2.*/)?"23:59":"29:59";if(newMask!=currentMask){$(this).setMask(newMask);}});};$(".faq_nav li").click(function(){$(".faq_nav li").removeClass('current');$(this).addClass('current');});

$(document).on('click','.checkbox, .widget-title-sort a',function(){
	//pour eviter que le checkbox de Remember Me/Location CheckBox lance l'evenement (verification par ID)
	if (!$(this).parent().find('input[type=checkbox]').is("#remember_me_checkbox") && !$(this).parent().find('input[type=checkbox]').is("#location-checkbox")) {
		$(".widget-title-sort a").removeClass('current');
		$(this).addClass('current');
		$(".content-overlay").css('display','block')
							 .css('height',$('.product-widget > .frm').height())
							 .css('width',$('.product-widget > .frm').width());
		$(".content-overlay > img").css('display','block')
								   .css('margin-top',$('.product-widget > .frm').height()/2-33)
								   .css('margin-left',$('.product-widget > .frm').width()/2-33);
		setTimeout(function(){
						$(".main-widget .close-details").css('display','none');
						$('.main-widget .view-details').css('display','block');
						$('.main-widget .details-more').css('display','none');
						$(".content-overlay").css('display','none');},400);
		
		if ( $(this).hasClass('checkbox')) {
			var chkbx = $(this).parent().find('input[type=checkbox]');
			if(chkbx.is(':checked')){
		        //console.log('checked');
		        //console.log(chkbx.data('type'));
		        setTimeout(function(){$('.'+chkbx.data('type')).show();}, 390);
		        
		    }else{
		        //console.log('unchecked');
		        //console.log(chkbx.data('type'));
		        setTimeout(function(){$('.'+chkbx.data('type')).hide();}, 390);
		        //$('.'+chkbx.data('type')).hide();
		    }
		}
	}
	
});

$('.content-form .return_location').css('display','none');
$("#location-checkbox, #location-checkbox-1").change(function(){if($(this).is(':checked')){$('.return_location').css('display','block');}else{$('.return_location').css('display','none');}});
$("span.checkbox").live('click',function(){if($(this).next('input[type="checkbox"]').attr('id')=='location-checkbox'||$(this).next('input[type="checkbox"]').attr('id')=='location-checkbox-1'){if($(this).next('input[type="checkbox"]').is(':checked')){$('.return_location').css('display','block');}else{$('.return_location').css('display','none');}}});
$('.pagination div').click(function(){$('.pagination div').removeClass('current');$(".content-overlay").css('display','block').css('height',$('.product-widget > .frm').height()).css('width',$('.product-widget > .frm').width());$(".content-overlay > img").css('display','block').css('margin-top',$('.product-widget > .frm').height()/2-33).css('margin-left',$('.product-widget > .frm').width()/2-33);if($(this).hasClass('left')||$(this).hasClass('right')){if($(this).hasClass('left')){$(this).next('div').addClass('current');}else{$(this).prev('div').addClass('current');}}else{$(this).addClass('current');}
setTimeout(function(){$(".main-widget .close-details").css('display','none');$('.main-widget .view-details').css('display','block');
	$('.main-widget .details-more').css('display','none');
	$(".content-overlay").css('display','none');},400);});});})(jQuery);

function Display_tab_div(name){(function($){"use strict";$(".admin-form .title-form").addClass('back').removeClass('current');$(".admin-form #tab_"+name).addClass("current").removeClass('back');$(".admin-form .frm").addClass("hidden");$(".admin-form #tab_"+name+"_content").removeClass('hidden');})(jQuery);}

function changepass(){
	var oldpass = $("input[name=password]").val();
	var newpass = $("input[name=newpassword]").val();
	var renewpass = $("input[name=renewpassword]").val();
	if (oldpass && newpass && renewpass) {
		if (newpass.length < 6) {
			$("#errmsg-box").empty('slow');
			$("#errmsg-box").append('The New Password must be at least 6 characters long');
			$("#errmsg-box").show();
		}else if(newpass != renewpass){
			$("#errmsg-box").empty();
			$("#errmsg-box").append('New Password doesn\'t match the Retyped New Password');
			$("#errmsg-box").show('slow');
		}else{
			$(".pass").removeClass('shortcode_input_required');
			$("#errmsg-box").hide('slow');
			$.post('/users/changepassword',{password: oldpass, newpassword: newpass,renewpassword: renewpass}).done(function( data ) {
			    //alert( "Data Loaded: " + data );
			    if (data == "success") {
			    	$(".allmsg-box").hide();
					$("#successmsg-box").append('The Password has been changed Successfuly');
					$("#successmsg-box").show('slow');
			    }else if (data == "fail") {
			    	$(".allmsg-box").hide();
			    	$("#errmsg-box").empty();
					$("#errmsg-box").append('Error while changing your password, please try again later');
					$("#errmsg-box").show('slow');
			    }else if (data == "oldpassnomatch") {
			    	$(".allmsg-box").hide();
			    	$("#errmsg-box").empty();
					$("#errmsg-box").append('The Old Password doesn\'t match the one in the Database');
					$("#errmsg-box").show('slow');
			    }else if (data == "lengtherror") {
			    	$(".allmsg-box").hide();
			    	$("#errmsg-box").empty();
					$("#errmsg-box").append('The New Password must be at least 6 characters long');
					$("#errmsg-box").show('slow');
			    }else if (data == "newpassnomatch") {
			    	$(".allmsg-box").hide();
			    	$("#errmsg-box").empty();
					$("#errmsg-box").append('New Password doesn\'t match the Retyped New Password');
					$("#errmsg-box").show('slow');
			    }
			  });
		}
	}else{
		$(".pass").removeClass('shortcode_input_required');
		$("#errmsg-box").empty('slow');
		$("#errmsg-box").append('Please fill all Left fields');
		$("#errmsg-box").show();
		$.each($(".pass"),function(){
			if (!$(this).val()) {
				$( this ).addClass('shortcode_input_required');
			}
		})
	}
}

function sort(sortby){
    var sortable = $('.sortable');
    sortable.find('.post').sort(function(a, b) {
        if (sortby == 'price') {
            $(".sortyby").removeClass('current');
            $("#sortbyprice").addClass('current');
            return +a.dataset.pmad - +b.dataset.pmad;
        }else if (sortby == 'type') {
            $(".sortyby").removeClass('current');
            $("#sortbytype").addClass('current');
            return +a.dataset.type - +b.dataset.type;
        }
        
    }).appendTo(sortable);
}

function print(divid){
	$("#"+divid).printThis({
		importCSS: true
	});
}