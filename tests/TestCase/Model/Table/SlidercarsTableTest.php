<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SlidercarsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SlidercarsTable Test Case
 */
class SlidercarsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SlidercarsTable
     */
    public $Slidercars;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.slidercars',
        'app.sliders',
        'app.cars',
        'app.types',
        'app.brands',
        'app.models',
        'app.fuels',
        'app.medias'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Slidercars') ? [] : ['className' => 'App\Model\Table\SlidercarsTable'];
        $this->Slidercars = TableRegistry::get('Slidercars', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Slidercars);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
