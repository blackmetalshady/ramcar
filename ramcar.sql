-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `ramcar`;
CREATE DATABASE `ramcar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ramcar`;

DROP TABLE IF EXISTS `brands`;
CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `brands` (`id`, `name`, `logo`, `disabled`) VALUES
(1,	'Volvo',	'volvo.png',	1),
(2,	'Volkswagen',	'volkswagen.png',	0),
(3,	'Suzuki',	'suzuki.jpg',	0),
(4,	'Rolls Royce',	'rolls_royce.png',	1),
(5,	'Renault',	'renault.png',	0),
(6,	'Porsche',	'porsche.png',	1),
(7,	'Peugeot',	'peugeot.png',	0),
(8,	'Nissan',	'nissan.png',	0),
(9,	'Mitsubishi',	'mitsubishi.jpg',	0),
(10,	'Mini',	'mini.png',	1),
(11,	'Mercedes',	'mercedes.png',	0),
(12,	'Mazda',	'mazda.png',	1),
(13,	'Lexus',	'lexus.png',	1),
(14,	'Land Rover',	'land_rover.jpg',	0),
(15,	'Lamborghini',	'lamborghini.gif',	1),
(16,	'Kia',	'kia.png',	0),
(17,	'Jeep',	'jeep.png',	0),
(18,	'Jaguar',	'jaguar.png',	1),
(19,	'Infiniti',	'infiniti.png',	1),
(20,	'Hyundai',	'hyundai.png',	0),
(21,	'Honda',	'honda.jpg',	0),
(22,	'GMC',	'gmc.png',	1),
(23,	'General Motors',	'general_motors.png',	1),
(24,	'Ford',	'ford.jpg',	0),
(25,	'Fiat',	'fiat.png',	0),
(26,	'Ferrari',	'ferrari.png',	1),
(27,	'Citroen',	'citroen.jpg',	0),
(28,	'Chevrolet',	'chevrolet.jpg',	1),
(29,	'Cadillac',	'cadillac.gif',	1),
(30,	'BMW',	'bmw.png',	1),
(31,	'Audi',	'audi.png',	1),
(32,	'Alfa Romeo',	'alfa_romeo.jpg',	1),
(33,	'Dacia',	'dacia.jpg',	0),
(34,	'Toyota',	'toyota.png',	0);

DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars` (
  `id` char(36) NOT NULL,
  `type_id` int(11) NOT NULL,
  `registration` varchar(45) NOT NULL,
  `passengers` int(11) NOT NULL DEFAULT '5',
  `auto` tinyint(4) NOT NULL DEFAULT '0',
  `airconditioning` tinyint(4) NOT NULL DEFAULT '1',
  `color` varchar(45) DEFAULT NULL,
  `details_fr` text,
  `details_en` text,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `horses` int(11) DEFAULT NULL,
  `fuel_id` int(11) DEFAULT NULL,
  `published` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_cars_brad_idx` (`brand_id`),
  KEY `fk_cars_type_idx` (`type_id`),
  KEY `model_id` (`model_id`),
  KEY `fuel_id` (`fuel_id`),
  CONSTRAINT `cars_ibfk_4` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `cars_ibfk_5` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `cars_ibfk_6` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `cars_ibfk_8` FOREIGN KEY (`fuel_id`) REFERENCES `fuels` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cars` (`id`, `type_id`, `registration`, `passengers`, `auto`, `airconditioning`, `color`, `details_fr`, `details_en`, `brand_id`, `model_id`, `horses`, `fuel_id`, `published`) VALUES
('032036ec-c622-49fa-a0e3-5fa6e2811167',	1,	'00012',	5,	0,	1,	'FFFFFF',	'',	'',	14,	16,	11,	2,	1),
('0a42b869-0ebc-43f9-ae98-9b9c5dc64344',	1,	'00009',	5,	0,	1,	'654222',	'',	'',	7,	5,	8,	2,	1),
('0d13ecb0-4ec3-4309-94d6-3953d0a0f1ef',	1,	'00008',	5,	0,	1,	'9D0C1C',	'',	'',	5,	13,	8,	1,	1),
('1',	1,	'159632',	5,	1,	1,	'F1BA75',	'<p>test francais</p>',	'<p>english test</p>',	5,	1,	7,	1,	1),
('1cdedb90-9363-4c56-9d31-26d0e9b02430',	1,	'00011',	5,	0,	1,	'A3A3A3',	'',	'',	5,	15,	8,	2,	1),
('2',	1,	'14753',	5,	0,	1,	'1165E7',	'',	'',	24,	2,	8,	2,	1),
('3',	2,	'12365478',	5,	0,	1,	'000000',	'<p>teste</p>',	'<p>test</p>',	25,	3,	7,	1,	1),
('4',	1,	'77777',	5,	0,	1,	'C8C8C8',	'<p>Logan</p>',	'<p>Logan</p>',	33,	4,	8,	1,	1),
('42fb680a-966d-4f2e-943b-67236017803a',	1,	'00001',	5,	0,	1,	'0A40FF',	'',	'',	20,	6,	8,	1,	1),
('4818ff65-0d5c-4b2e-90a2-9838b921c182',	1,	'00004',	5,	0,	1,	'AFAFAF',	'',	'',	7,	9,	8,	1,	1),
('6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	1,	'00007',	5,	0,	1,	'9A040A',	'',	'',	3,	12,	8,	1,	1),
('711acf1d-f078-466a-ba37-0985390fc9f4',	1,	'00014',	5,	0,	1,	'FFFFFF',	'',	'',	25,	3,	8,	2,	1),
('74fe991d-4737-480a-86c1-e03865dfcd65',	1,	'00005',	5,	0,	1,	'F5F5F5',	'',	'',	25,	10,	6,	1,	1),
('7554af39-d8b4-4544-93c0-54df6ec8b615',	1,	'00003',	5,	0,	1,	'E0E0E0',	'',	'',	20,	8,	8,	1,	1),
('8a804b86-5159-4334-8434-785836d09960',	1,	'00010',	5,	0,	1,	'D11B46',	'',	'',	24,	14,	8,	2,	1),
('8fe25387-d0ea-4f24-9797-aca33f8334c7',	1,	'00006',	5,	0,	1,	'0B68C8',	'',	'',	33,	11,	7,	1,	1),
('98a7cb35-f5be-449a-a29b-a290e3cebda1',	1,	'00018',	5,	0,	1,	'1A1A1A',	'',	'',	2,	20,	11,	2,	1),
('ae093131-6f2f-440c-90a7-ebf69922e666',	1,	'00000',	5,	0,	1,	'9C3D4C',	'',	'',	7,	5,	8,	1,	1),
('dd0870b9-531a-40a6-9a37-f566801adc9e',	1,	'00015',	5,	0,	1,	'854F1D',	'',	'',	33,	18,	11,	2,	1),
('ee9b6459-fa61-4b8a-bb50-8b13839aa435',	1,	'00002',	5,	0,	1,	'C5C5C5',	'',	'',	16,	7,	8,	1,	1),
('f1905d80-f1b9-44ed-8ba5-96df2c0bd6f8',	1,	'00013',	5,	0,	1,	'9A9A9A',	'',	'',	9,	17,	11,	2,	1),
('f7346044-c8f8-4fbb-aaa0-f334b55f4475',	1,	'00016',	5,	0,	1,	'000000',	'',	'',	5,	19,	8,	2,	1);

DROP TABLE IF EXISTS `carsreservations`;
CREATE TABLE `carsreservations` (
  `id` char(36) NOT NULL,
  `car_id` char(36) DEFAULT NULL,
  `reservation_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_id` (`reservation_id`),
  KEY `car_id` (`car_id`),
  CONSTRAINT `carsreservations_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `carsreservations_ibfk_2` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `carsreservations` (`id`, `car_id`, `reservation_id`) VALUES
('1',	'4',	'2'),
('14',	'2',	'15'),
('15',	'1',	'16'),
('16',	'2',	'17'),
('17',	'3',	'18'),
('18',	'1',	'19');

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1,	'ad',	'Andorra'),
(2,	'ae',	'United Arab Emirates'),
(3,	'af',	'Afghanistan'),
(4,	'ag',	'Antigua and Barbuda'),
(5,	'ai',	'Anguilla'),
(6,	'al',	'Albania'),
(7,	'am',	'Armenia'),
(8,	'an',	'Netherlands Antilles'),
(9,	'ao',	'Angola'),
(10,	'aq',	'Antarctica'),
(11,	'ar',	'Argentina'),
(12,	'as',	'American Samoa'),
(13,	'at',	'Austria'),
(14,	'au',	'Australia'),
(15,	'aw',	'Aruba'),
(16,	'ax',	'Aland Islands'),
(17,	'az',	'Azerbaijan'),
(18,	'ba',	'Bosnia and Herzegovina'),
(19,	'bb',	'Barbados'),
(20,	'bd',	'Bangladesh'),
(21,	'be',	'Belgium'),
(22,	'bf',	'Burkina Faso'),
(23,	'bg',	'Bulgaria'),
(24,	'bh',	'Bahrain'),
(25,	'bi',	'Burundi'),
(26,	'bj',	'Benin'),
(27,	'bm',	'Bermuda'),
(28,	'bn',	'Brunei Darussalam'),
(29,	'bo',	'Bolivia'),
(30,	'br',	'Brazil'),
(31,	'bs',	'Bahamas'),
(32,	'bt',	'Bhutan'),
(33,	'bv',	'Bouvet Island'),
(34,	'bw',	'Botswana'),
(35,	'by',	'Belarus'),
(36,	'bz',	'Belize'),
(37,	'ca',	'Canada'),
(38,	'cc',	'Cocos (Keeling) Islands'),
(39,	'cd',	'Democratic Republic of the Congo'),
(40,	'cf',	'Central African Republic'),
(41,	'cg',	'Congo'),
(42,	'ch',	'Switzerland'),
(43,	'ci',	'Cote D\'Ivoire (Ivory Coast)'),
(44,	'ck',	'Cook Islands'),
(45,	'cl',	'Chile'),
(46,	'cm',	'Cameroon'),
(47,	'cn',	'China'),
(48,	'co',	'Colombia'),
(49,	'cr',	'Costa Rica'),
(50,	'cs',	'Serbia and Montenegro'),
(51,	'cu',	'Cuba'),
(52,	'cv',	'Cape Verde'),
(53,	'cx',	'Christmas Island'),
(54,	'cy',	'Cyprus'),
(55,	'cz',	'Czech Republic'),
(56,	'de',	'Germany'),
(57,	'dj',	'Djibouti'),
(58,	'dk',	'Denmark'),
(59,	'dm',	'Dominica'),
(60,	'do',	'Dominican Republic'),
(61,	'dz',	'Algeria'),
(62,	'ec',	'Ecuador'),
(63,	'ee',	'Estonia'),
(64,	'eg',	'Egypt'),
(65,	'eh',	'Western Sahara'),
(66,	'er',	'Eritrea'),
(67,	'es',	'Spain'),
(68,	'et',	'Ethiopia'),
(69,	'fi',	'Finland'),
(70,	'fj',	'Fiji'),
(71,	'fk',	'Falkland Islands (Malvinas)'),
(72,	'fm',	'Federated States of Micronesia'),
(73,	'fo',	'Faroe Islands'),
(74,	'fr',	'France'),
(75,	'fx',	'France, Metropolitan'),
(76,	'ga',	'Gabon'),
(77,	'gb',	'Great Britain (UK)'),
(78,	'gd',	'Grenada'),
(79,	'ge',	'Georgia'),
(80,	'gf',	'French Guiana'),
(81,	'gh',	'Ghana'),
(82,	'gi',	'Gibraltar'),
(83,	'gl',	'Greenland'),
(84,	'gm',	'Gambia'),
(85,	'gn',	'Guinea'),
(86,	'gp',	'Guadeloupe'),
(87,	'gq',	'Equatorial Guinea'),
(88,	'gr',	'Greece'),
(89,	'gs',	'S. Georgia and S. Sandwich Islands'),
(90,	'gt',	'Guatemala'),
(91,	'gu',	'Guam'),
(92,	'gw',	'Guinea-Bissau'),
(93,	'gy',	'Guyana'),
(94,	'hk',	'Hong Kong'),
(95,	'hm',	'Heard Island and McDonald Islands'),
(96,	'hn',	'Honduras'),
(97,	'hr',	'Croatia (Hrvatska)'),
(98,	'ht',	'Haiti'),
(99,	'hu',	'Hungary'),
(100,	'id',	'Indonesia'),
(101,	'ie',	'Ireland'),
(102,	'il',	'Israel'),
(103,	'in',	'India'),
(104,	'io',	'British Indian Ocean Territory'),
(105,	'iq',	'Iraq'),
(106,	'ir',	'Iran'),
(107,	'is',	'Iceland'),
(108,	'it',	'Italy'),
(109,	'jm',	'Jamaica'),
(110,	'jo',	'Jordan'),
(111,	'jp',	'Japan'),
(112,	'ke',	'Kenya'),
(113,	'kg',	'Kyrgyzstan'),
(114,	'kh',	'Cambodia'),
(115,	'ki',	'Kiribati'),
(116,	'km',	'Comoros'),
(117,	'kn',	'Saint Kitts and Nevis'),
(118,	'kp',	'Korea (North)'),
(119,	'kr',	'Korea (South)'),
(120,	'kw',	'Kuwait'),
(121,	'ky',	'Cayman Islands'),
(122,	'kz',	'Kazakhstan'),
(123,	'la',	'Laos'),
(124,	'lb',	'Lebanon'),
(125,	'lc',	'Saint Lucia'),
(126,	'li',	'Liechtenstein'),
(127,	'lk',	'Sri Lanka'),
(128,	'lr',	'Liberia'),
(129,	'ls',	'Lesotho'),
(130,	'lt',	'Lithuania'),
(131,	'lu',	'Luxembourg'),
(132,	'lv',	'Latvia'),
(133,	'ly',	'Libya'),
(134,	'ma',	'Morocco'),
(135,	'mc',	'Monaco'),
(136,	'md',	'Moldova'),
(137,	'mg',	'Madagascar'),
(138,	'mh',	'Marshall Islands'),
(139,	'mk',	'Macedonia'),
(140,	'ml',	'Mali'),
(141,	'mm',	'Myanmar'),
(142,	'mn',	'Mongolia'),
(143,	'mo',	'Macao'),
(144,	'mp',	'Northern Mariana Islands'),
(145,	'mq',	'Martinique'),
(146,	'mr',	'Mauritania'),
(147,	'ms',	'Montserrat'),
(148,	'mt',	'Malta'),
(149,	'mu',	'Mauritius'),
(150,	'mv',	'Maldives'),
(151,	'mw',	'Malawi'),
(152,	'mx',	'Mexico'),
(153,	'my',	'Malaysia'),
(154,	'mz',	'Mozambique'),
(155,	'na',	'Namibia'),
(156,	'nc',	'New Caledonia'),
(157,	'ne',	'Niger'),
(158,	'nf',	'Norfolk Island'),
(159,	'ng',	'Nigeria'),
(160,	'ni',	'Nicaragua'),
(161,	'nl',	'Netherlands'),
(162,	'no',	'Norway'),
(163,	'np',	'Nepal'),
(164,	'nr',	'Nauru'),
(165,	'nu',	'Niue'),
(166,	'nz',	'New Zealand (Aotearoa)'),
(167,	'om',	'Oman'),
(168,	'pa',	'Panama'),
(169,	'pe',	'Peru'),
(170,	'pf',	'French Polynesia'),
(171,	'pg',	'Papua New Guinea'),
(172,	'ph',	'Philippines'),
(173,	'pk',	'Pakistan'),
(174,	'pl',	'Poland'),
(175,	'pm',	'Saint Pierre and Miquelon'),
(176,	'pn',	'Pitcairn'),
(177,	'pr',	'Puerto Rico'),
(178,	'ps',	'Palestinian Territory'),
(179,	'pt',	'Portugal'),
(180,	'pw',	'Palau'),
(181,	'py',	'Paraguay'),
(182,	'qa',	'Qatar'),
(183,	're',	'Reunion'),
(184,	'ro',	'Romania'),
(185,	'ru',	'Russian Federation'),
(186,	'rw',	'Rwanda'),
(187,	'sa',	'Saudi Arabia'),
(188,	'sb',	'Solomon Islands'),
(189,	'sc',	'Seychelles'),
(190,	'sd',	'Sudan'),
(191,	'se',	'Sweden'),
(192,	'sg',	'Singapore'),
(193,	'sh',	'Saint Helena'),
(194,	'si',	'Slovenia'),
(195,	'sj',	'Svalbard and Jan Mayen'),
(196,	'sk',	'Slovakia'),
(197,	'sl',	'Sierra Leone'),
(198,	'sm',	'San Marino'),
(199,	'sn',	'Senegal'),
(200,	'so',	'Somalia'),
(201,	'sr',	'Suriname'),
(202,	'st',	'Sao Tome and Principe'),
(203,	'su',	'USSR (former)'),
(204,	'sv',	'El Salvador'),
(205,	'sy',	'Syria'),
(206,	'sz',	'Swaziland'),
(207,	'tc',	'Turks and Caicos Islands'),
(208,	'td',	'Chad'),
(209,	'tf',	'French Southern Territories'),
(210,	'tg',	'Togo'),
(211,	'th',	'Thailand'),
(212,	'tj',	'Tajikistan'),
(213,	'tk',	'Tokelau'),
(214,	'tl',	'Timor-Leste'),
(215,	'tm',	'Turkmenistan'),
(216,	'tn',	'Tunisia'),
(217,	'to',	'Tonga'),
(218,	'tp',	'East Timor'),
(219,	'tr',	'Turkey'),
(220,	'tt',	'Trinidad and Tobago'),
(221,	'tv',	'Tuvalu'),
(222,	'tw',	'Taiwan'),
(223,	'tz',	'Tanzania'),
(224,	'ua',	'Ukraine'),
(225,	'ug',	'Uganda'),
(226,	'uk',	'United Kingdom'),
(227,	'um',	'United States Minor Outlying Islands'),
(228,	'us',	'United States'),
(229,	'uy',	'Uruguay'),
(230,	'uz',	'Uzbekistan'),
(231,	'va',	'Vatican City State (Holy See)'),
(232,	'vc',	'Saint Vincent and the Grenadines'),
(233,	've',	'Venezuela'),
(234,	'vg',	'Virgin Islands (British)'),
(235,	'vi',	'Virgin Islands (U.S.)'),
(236,	'vn',	'Viet Nam'),
(237,	'vu',	'Vanuatu'),
(238,	'wf',	'Wallis and Futuna'),
(239,	'ws',	'Samoa'),
(240,	'ye',	'Yemen'),
(241,	'yt',	'Mayotte'),
(242,	'yu',	'Yugoslavia (former)'),
(243,	'za',	'South Africa'),
(244,	'zm',	'Zambia'),
(245,	'zr',	'Zaire (former)'),
(246,	'zw',	'Zimbabwe');

DROP TABLE IF EXISTS `extras`;
CREATE TABLE `extras` (
  `id` char(36) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `name_fr` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `description_fr` tinytext,
  `description_en` tinytext,
  `details_fr` tinytext,
  `details_en` tinytext,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `extras` (`id`, `name_en`, `name_fr`, `type`, `description_fr`, `description_en`, `details_fr`, `details_en`, `img`) VALUES
('1',	'GPS test',	'GPS de teste',	'gps',	'ceci est une description',	'this is a description',	'<p>plus de details</p>',	'<p>more details</p>',	'extra_1466507075.png'),
('2',	'Infant Childe Seat',	'Siège bébé',	'ics',	'siege bebe',	'infant childe seat for babies less then 1 year old',	'',	'infant childe seat for babies less then 1 year old',	'extra_1466516289.jpg'),
('3',	'Hand Controle',	'Controle a main',	'hc',	'controle a main',	'hand controle',	'',	'',	'extra_1466516354.png');

DROP TABLE IF EXISTS `extrasreservations`;
CREATE TABLE `extrasreservations` (
  `id` char(36) NOT NULL,
  `extra_id` char(36) NOT NULL,
  `reservation_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `extra_id` (`extra_id`),
  KEY `reservation_id` (`reservation_id`),
  CONSTRAINT `extrasreservations_ibfk_1` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `extrasreservations_ibfk_2` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `extrasreservations` (`id`, `extra_id`, `reservation_id`) VALUES
('27',	'1',	'2'),
('28',	'1',	'15'),
('29',	'2',	'15'),
('30',	'2',	'16'),
('31',	'1',	'16'),
('32',	'1',	'17'),
('33',	'2',	'17'),
('34',	'1',	'18'),
('35',	'1',	'19');

DROP TABLE IF EXISTS `fuels`;
CREATE TABLE `fuels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_fr` varchar(50) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `fuels` (`id`, `name_fr`, `name_en`) VALUES
(1,	'Essence',	'Gasoline'),
(2,	'Gasoile',	'Diesel');

DROP TABLE IF EXISTS `guests`;
CREATE TABLE `guests` (
  `id` char(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `drvlicense` varchar(30) NOT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `guests` (`id`, `first_name`, `last_name`, `age`, `email`, `phone`, `drvlicense`, `newsletter`) VALUES
('11',	'yassine',	'elkhanboubi',	25,	'yas-yo@hotmail.com',	'522984605',	'4534563456',	1),
('12',	'yassine',	'elkhanboubi',	25,	'yassine.elkhanboubi@gmail.com',	'522984605',	'4534563456',	1),
('13',	'Driss',	'Chahtane',	25,	'chouftvmaroc@gmail.com',	'522984605',	'4534563456',	1),
('14',	'Driss',	'Chahtane',	25,	'chouftvmaroc@gmail.com',	'522984605',	'4534563456',	1),
('15',	'Driss',	'Chahtane',	25,	'chouftvmaroc@gmail.com',	'522984605',	'4534563456',	1),
('16',	'Driss',	'Chahtane',	25,	'chouftvmaroc@gmail.com',	'522984605',	'4534563456',	1),
('17',	'Jamal Eddin',	'El Khanboubi',	34,	'blackmetalshady@facebook.com',	'00212675342668',	'00212675342668',	1),
('18',	'Driss',	'Chahtane',	25,	'chouftvmaroc@gmail.com',	'522984605',	'1569651',	1),
('19',	'Yassine',	'El Khanboubi',	28,	'elkhanboubi.yassine@gmail.com',	'00212675342668',	'4534563456',	1);

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_fr` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `locations` (`id`, `name_fr`, `name_en`) VALUES
(1,	'Aéroport Marrakech Menara',	'Marrakech Menara Aeroport'),
(2,	'Centre Ville, Medina, Jamaa Lefna',	'Downtown, Medina Jemaa Lefna');

DROP TABLE IF EXISTS `medias`;
CREATE TABLE `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT 'unknown.jpg',
  `car_id` char(36) DEFAULT NULL,
  `extra_id` char(36) DEFAULT NULL,
  `primary_img` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `car_id` (`car_id`),
  KEY `extra_id` (`extra_id`),
  CONSTRAINT `medias_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `medias_ibfk_2` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `medias` (`id`, `type`, `size`, `name`, `car_id`, `extra_id`, `primary_img`) VALUES
(3,	'image/jpeg',	15964,	'symbol.jpg',	'1',	NULL,	1),
(4,	'image/jpeg',	25139,	'car_1465775923.jpg',	'3',	NULL,	0),
(5,	'image/jpeg',	96342,	'car_1465926261.jpg',	'4',	NULL,	1),
(6,	NULL,	NULL,	'slider1.jpg',	NULL,	NULL,	0),
(7,	NULL,	NULL,	'slider2.jpg',	NULL,	NULL,	0),
(8,	NULL,	NULL,	'slider3.jpg',	NULL,	NULL,	0),
(9,	NULL,	NULL,	'slider_front.png',	NULL,	NULL,	0),
(12,	'image/png',	122731,	'sliderimg_1466160570.png',	NULL,	NULL,	1),
(13,	'image/png',	122731,	'sliderimg_1466160901.png',	NULL,	NULL,	1),
(14,	'image/png',	122731,	'sliderimg_1466160953.png',	NULL,	NULL,	1),
(27,	'image/jpeg',	96342,	'car_1466206186.jpg',	'4',	NULL,	1),
(28,	'image/jpeg',	550808,	'car_1466206253.jpg',	'3',	NULL,	1),
(29,	'image/jpeg',	347371,	'car_1474309703.jpg',	'ae093131-6f2f-440c-90a7-ebf69922e666',	NULL,	1),
(30,	'image/jpeg',	114533,	'car_1474309831.jpg',	'ae093131-6f2f-440c-90a7-ebf69922e666',	NULL,	1),
(32,	'image/jpeg',	91644,	'car_1474310532.jpg',	'42fb680a-966d-4f2e-943b-67236017803a',	NULL,	1),
(33,	'image/jpeg',	42557,	'car_1474310546.jpg',	'42fb680a-966d-4f2e-943b-67236017803a',	NULL,	1),
(34,	'image/png',	112115,	'car_1474310906.png',	'ee9b6459-fa61-4b8a-bb50-8b13839aa435',	NULL,	1),
(35,	'image/jpeg',	35447,	'car_1474310917.jpg',	'ee9b6459-fa61-4b8a-bb50-8b13839aa435',	NULL,	1),
(36,	'image/jpeg',	129083,	'car_1474311256.jpg',	'7554af39-d8b4-4544-93c0-54df6ec8b615',	NULL,	1),
(38,	'image/jpeg',	174174,	'car_1474311398.jpg',	'7554af39-d8b4-4544-93c0-54df6ec8b615',	NULL,	1),
(39,	'image/jpeg',	145163,	'car_1474311775.jpg',	'4818ff65-0d5c-4b2e-90a2-9838b921c182',	NULL,	1),
(40,	'image/jpeg',	588837,	'car_1474311804.jpg',	'4818ff65-0d5c-4b2e-90a2-9838b921c182',	NULL,	1),
(41,	'image/jpeg',	128823,	'car_1474312218.jpg',	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL,	1),
(42,	'image/jpeg',	442695,	'car_1474312251.jpg',	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL,	0),
(43,	'image/jpeg',	245830,	'car_1474377996.jpg',	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL,	1),
(44,	'image/jpeg',	88404,	'car_1474378005.jpg',	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL,	1),
(45,	'image/png',	490194,	'car_1474378702.png',	'2',	NULL,	1),
(46,	'image/jpeg',	238068,	'car_1474378712.jpg',	'2',	NULL,	0),
(47,	'image/jpeg',	109399,	'car_1474379585.jpg',	'6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	NULL,	1),
(48,	'image/jpeg',	387776,	'car_1474379594.jpg',	'6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	NULL,	1),
(49,	'image/jpeg',	195595,	'car_1474380170.jpg',	'0d13ecb0-4ec3-4309-94d6-3953d0a0f1ef',	NULL,	1),
(51,	'image/jpeg',	238911,	'car_1474380337.jpg',	'0d13ecb0-4ec3-4309-94d6-3953d0a0f1ef',	NULL,	1),
(52,	'image/jpeg',	345876,	'car_1474380608.jpg',	'0a42b869-0ebc-43f9-ae98-9b9c5dc64344',	NULL,	1),
(53,	'image/jpeg',	114533,	'car_1474380619.jpg',	'0a42b869-0ebc-43f9-ae98-9b9c5dc64344',	NULL,	1),
(54,	'image/jpeg',	705248,	'car_1474382035.jpg',	'8a804b86-5159-4334-8434-785836d09960',	NULL,	1),
(55,	'image/jpeg',	130206,	'car_1474382055.jpg',	'8a804b86-5159-4334-8434-785836d09960',	NULL,	1),
(56,	'image/jpeg',	242243,	'car_1474382649.jpg',	'1cdedb90-9363-4c56-9d31-26d0e9b02430',	NULL,	1),
(57,	'image/jpeg',	121407,	'car_1474382660.jpg',	'1cdedb90-9363-4c56-9d31-26d0e9b02430',	NULL,	1),
(58,	'image/jpeg',	119705,	'car_1474383281.jpg',	'032036ec-c622-49fa-a0e3-5fa6e2811167',	NULL,	1),
(59,	'image/jpeg',	58920,	'car_1474383291.jpg',	'032036ec-c622-49fa-a0e3-5fa6e2811167',	NULL,	1),
(60,	'image/png',	174246,	'car_1474383786.png',	'f1905d80-f1b9-44ed-8ba5-96df2c0bd6f8',	NULL,	1),
(61,	'image/png',	168526,	'car_1474383796.png',	'f1905d80-f1b9-44ed-8ba5-96df2c0bd6f8',	NULL,	1),
(62,	'image/jpeg',	2357410,	'car_1474384057.jpg',	'711acf1d-f078-466a-ba37-0985390fc9f4',	NULL,	1),
(63,	'image/jpeg',	124418,	'car_1474384069.jpg',	'711acf1d-f078-466a-ba37-0985390fc9f4',	NULL,	1),
(64,	'image/jpeg',	216213,	'car_1474384656.jpg',	'dd0870b9-531a-40a6-9a37-f566801adc9e',	NULL,	1),
(65,	'image/jpeg',	121969,	'car_1474384666.jpg',	'dd0870b9-531a-40a6-9a37-f566801adc9e',	NULL,	1),
(66,	'image/jpeg',	165119,	'car_1474385012.jpg',	'f7346044-c8f8-4fbb-aaa0-f334b55f4475',	NULL,	1),
(67,	'image/jpeg',	136593,	'car_1474385020.jpg',	'f7346044-c8f8-4fbb-aaa0-f334b55f4475',	NULL,	1),
(68,	'image/jpeg',	45697,	'car_1474385447.jpg',	'98a7cb35-f5be-449a-a29b-a290e3cebda1',	NULL,	1),
(69,	'image/jpeg',	642772,	'car_1474385456.jpg',	'98a7cb35-f5be-449a-a29b-a290e3cebda1',	NULL,	1);

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` char(36) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `sent_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` char(36) DEFAULT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `messages` (`id`, `full_name`, `email`, `subject`, `message`, `sent_at`, `user_id`, `is_read`, `parent_id`) VALUES
('2',	'Yassine El Khanboubi',	'elkhanboubi.yassine@gmail.com',	'about your prices',	'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.',	'2016-08-22 12:47:53',	NULL,	1,	NULL),
('3',	NULL,	NULL,	'Re: message',	'reponse message avec id 2',	'2016-08-22 13:33:18',	'3',	0,	'2'),
('4',	NULL,	NULL,	'Re: Re: message',	'une autre reponse',	'2016-08-22 16:09:13',	'3',	0,	'2'),
('a493bebd-cec0-4b31-9b1c-9ad9256ed304',	'yassine el khanboubi',	'elkhanboubi.yassine@gmail.com',	'about your prices',	'hello world!',	'2016-08-29 13:54:32',	NULL,	0,	NULL);

DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `brand` (`brand_id`),
  CONSTRAINT `fk_models_brand` FOREIGN KEY (`id`) REFERENCES `brands` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `models_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `models` (`id`, `name`, `brand_id`, `year`) VALUES
(1,	'Symbole',	5,	'2014'),
(2,	'Fiesta',	24,	'2013'),
(3,	'Puntu',	25,	'2014'),
(4,	'Logan',	33,	'2013'),
(5,	'301',	7,	'2012'),
(6,	'i10',	20,	'2008'),
(7,	'picanto',	16,	'2007'),
(8,	'accent ',	20,	'2016'),
(9,	'206',	7,	'2007'),
(10,	'punto G',	25,	'2014'),
(11,	'sandero',	33,	'2008'),
(12,	'Celerio',	3,	'2015'),
(13,	'Clio III',	5,	'2015'),
(14,	'Focus',	24,	'2016'),
(15,	'Megane',	5,	'2014'),
(16,	'Range Rover Evoque ',	14,	'2016'),
(17,	'Pagero Sport 4x4',	9,	'2014'),
(18,	'Duster',	33,	'2016'),
(19,	'Fluence ',	5,	'2015'),
(20,	'Touareg V6',	2,	'2015');

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE `newsletter` (
  `id` char(36) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `newsletter` (`id`, `fullname`, `email`, `disabled`) VALUES
('1',	'yassine el khanboubi',	'yas-yo@hotmail.com',	0),
('2',	'Jamal el khanboubi',	'netshady@gmail.com',	0),
('3',	'imane',	'imane.elkhanboubi@gmail.com',	0),
('5e608797-1552-40e8-a656-1939a2d19986',	'hello world',	'theshityfuck@gmail.com',	0);

DROP TABLE IF EXISTS `passwordtokens`;
CREATE TABLE `passwordtokens` (
  `id` char(36) NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` char(36) NOT NULL,
  `start_time` datetime NOT NULL,
  `expired` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `passwordtokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `prices`;
CREATE TABLE `prices` (
  `id` char(36) NOT NULL,
  `from_day` int(11) NOT NULL,
  `to_day` int(11) NOT NULL,
  `price_mad` double NOT NULL,
  `price_euro` double NOT NULL,
  `price_usd` double NOT NULL,
  `car_id` char(36) DEFAULT NULL,
  `extra_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `car_id` (`car_id`),
  KEY `extra_id` (`extra_id`),
  CONSTRAINT `prices_ibfk_1` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `prices_ibfk_2` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `prices` (`id`, `from_day`, `to_day`, `price_mad`, `price_euro`, `price_usd`, `car_id`, `extra_id`) VALUES
('0841e950-d5b6-4b9f-82b7-d354b600c3eb',	9,	15,	1360,	135,	134,	'032036ec-c622-49fa-a0e3-5fa6e2811167',	NULL),
('091c1684-23ae-4693-a10b-6ce4eb0c75db',	9,	15,	310,	30,	29,	'711acf1d-f078-466a-ba37-0985390fc9f4',	NULL),
('0e6c1ec9-309e-4a24-adf1-2e89be4f2f1d',	4,	8,	265,	25.5,	24.5,	'6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	NULL),
('1',	1,	3,	250,	24.5,	26,	'1',	NULL),
('136be4d4-e4ab-4e82-a490-69441d4f6c78',	1,	3,	280,	27,	26,	'6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	NULL),
('1cbb50a4-2dd1-4e4e-8313-d65f06dff0e6',	4,	8,	290,	28,	27,	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL),
('1e1f5c4b-3331-4479-aa8a-e53cf9c932dd',	1,	3,	340,	33,	32,	'7554af39-d8b4-4544-93c0-54df6ec8b615',	NULL),
('2',	4,	8,	220,	22,	24,	'1',	NULL),
('225bef49-bcba-4a11-b303-fb6c87dafb72',	4,	8,	350,	34,	33,	'1cdedb90-9363-4c56-9d31-26d0e9b02430',	NULL),
('24a76649-b534-4d40-82f4-86c31210843b',	9,	15,	260,	25,	24,	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL),
('35ecc223-3246-4b74-ac8e-bb9e65ce00c6',	9,	15,	245,	23.5,	22.5,	'42fb680a-966d-4f2e-943b-67236017803a',	NULL),
('39314199-a986-4b05-bff7-720719faf3a7',	4,	8,	320,	31,	30,	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL),
('3cb86f4b-b24f-43e3-b790-04e665b66f43',	1,	3,	380,	37,	36,	'1cdedb90-9363-4c56-9d31-26d0e9b02430',	NULL),
('4',	1,	3,	20,	2,	2.5,	NULL,	'1'),
('4b91947e-39d8-4512-8c19-11da87d0ccc7',	4,	8,	350,	34,	33,	'711acf1d-f078-466a-ba37-0985390fc9f4',	NULL),
('4e957f16-43fd-4878-bdfd-9541bc476ec6',	4,	8,	265,	25.5,	24.5,	'42fb680a-966d-4f2e-943b-67236017803a',	NULL),
('5',	1,	3,	200,	20,	21.5,	'4',	NULL),
('526af6bc-5c3b-4ae1-b1a1-163977be60bf',	4,	8,	290,	28,	27,	'4818ff65-0d5c-4b2e-90a2-9838b921c182',	NULL),
('58ca9711-bb10-447b-9c51-9eb8a954038f',	1,	3,	280,	27,	26,	'42fb680a-966d-4f2e-943b-67236017803a',	NULL),
('5d444cd0-2d1e-4427-afdb-bc6d53fa6b0d',	9,	15,	300,	29,	28,	'7554af39-d8b4-4544-93c0-54df6ec8b615',	NULL),
('6',	1,	3,	260,	26,	27.5,	'3',	NULL),
('624fd005-f0fe-4e39-9e4d-f09af93c67aa',	1,	3,	1540,	153,	152,	'032036ec-c622-49fa-a0e3-5fa6e2811167',	NULL),
('6bf7cc8e-eec0-495f-9d7f-5cad146b8136',	4,	8,	1420,	141,	140,	'98a7cb35-f5be-449a-a29b-a290e3cebda1',	NULL),
('6de02a79-36ac-40c8-8dfc-9035d2edeb3b',	9,	15,	310,	30,	29,	'1cdedb90-9363-4c56-9d31-26d0e9b02430',	NULL),
('7949e1a1-aa0f-48f8-b8d4-ffae487bc5a3',	4,	8,	320,	31,	30,	'7554af39-d8b4-4544-93c0-54df6ec8b615',	NULL),
('87c67cd1-31ba-46a0-9970-7c672a354d2b',	16,	20,	270,	26,	25,	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL),
('89125b32-7ff6-4e74-a0f2-741e6d22b23a',	4,	8,	290,	28,	27,	'2',	NULL),
('8b863c7a-8eea-4076-93bc-e0fbd56ef334',	1,	3,	310,	30,	29,	'4818ff65-0d5c-4b2e-90a2-9838b921c182',	NULL),
('92011229-525e-410e-9314-36b22d97a60e',	1,	3,	310,	30,	29,	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL),
('96890214-ad67-4661-96f6-f24177464644',	1,	3,	310,	30,	29,	'2',	NULL),
('a3ed2b03-a3b0-4cb1-99db-cb05c885f1f4',	1,	3,	380,	37,	36,	'8a804b86-5159-4334-8434-785836d09960',	NULL),
('b12cc627-48b4-4b33-8ce5-e393c3d0ba48',	9,	15,	245,	23.5,	22.5,	'6aedd0cd-760d-409a-ad54-df2b7cc1feb2',	NULL),
('b368d18a-4435-4ad6-b695-52c676b5b3f7',	9,	15,	260,	25,	24,	'4818ff65-0d5c-4b2e-90a2-9838b921c182',	NULL),
('b9a1b1a1-6912-4c71-bf8e-0993dcbdeeb6',	4,	8,	360,	35,	34,	'8a804b86-5159-4334-8434-785836d09960',	NULL),
('bb8068e0-62cd-4e9a-8660-ae7e7a84e11a',	1,	3,	340,	33,	32,	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL),
('c05db87f-1627-4407-991b-29ad2c964f79',	1,	3,	980,	97,	96,	'f1905d80-f1b9-44ed-8ba5-96df2c0bd6f8',	NULL),
('c4cbd35b-9ed6-4d39-a972-a71d97d412c5',	1,	3,	380,	37,	36,	'711acf1d-f078-466a-ba37-0985390fc9f4',	NULL),
('c7a83518-5d10-4fcb-97c1-742f7cb0ce8f',	9,	15,	340,	33,	32,	'8a804b86-5159-4334-8434-785836d09960',	NULL),
('c9abca13-ce88-4632-9c9b-01081053cfe1',	20,	0,	250,	24,	26,	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL),
('da94006b-5918-4118-a846-a80f11b8c2bd',	16,	20,	245,	23.5,	22.5,	'8fe25387-d0ea-4f24-9797-aca33f8334c7',	NULL),
('db0cbbac-e5eb-4136-ab26-2ae873aa953b',	4,	8,	1420,	141,	140,	'032036ec-c622-49fa-a0e3-5fa6e2811167',	NULL),
('e1e8863a-bbec-4024-820a-d5e921c9b90a',	9,	15,	1360,	135,	134,	'98a7cb35-f5be-449a-a29b-a290e3cebda1',	NULL),
('e8871f9c-7861-4df2-9a18-27fed52a76a8',	9,	15,	260,	25,	24,	'2',	NULL),
('eca3857a-7359-4f40-8c16-cf46e26d0886',	9,	15,	300,	29,	28,	'74fe991d-4737-480a-86c1-e03865dfcd65',	NULL),
('f1c42985-dee5-4c40-8a9e-4d018308b98c',	1,	3,	1540,	153,	152,	'98a7cb35-f5be-449a-a29b-a290e3cebda1',	NULL);

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations` (
  `id` char(36) NOT NULL,
  `from_date` date NOT NULL,
  `from_time` time NOT NULL,
  `to_date` date NOT NULL,
  `to_time` time NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `return_location` varchar(255) NOT NULL,
  `user_id` char(36) DEFAULT NULL,
  `guest_id` char(36) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `rejected` tinyint(4) NOT NULL DEFAULT '0',
  `canceled` tinyint(4) NOT NULL DEFAULT '0',
  `request_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `guest_id` (`guest_id`),
  CONSTRAINT `reservations_ibfk_7` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reservations_ibfk_8` FOREIGN KEY (`guest_id`) REFERENCES `guests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `reservations` (`id`, `from_date`, `from_time`, `to_date`, `to_time`, `pickup_location`, `return_location`, `user_id`, `guest_id`, `code`, `confirmed`, `rejected`, `canceled`, `request_time`) VALUES
('15',	'2016-07-14',	'12:00:00',	'2016-07-16',	'12:00:00',	'Marrakech Menara Aeroport',	'Marrakech Menara Aeroport',	NULL,	'16',	'HMshyts3',	0,	0,	0,	'2016-09-14 15:37:04'),
('16',	'2016-07-30',	'12:00:00',	'2016-08-01',	'12:00:00',	'Marrakech Menara Aeroport',	'Marrakech Menara Aeroport',	'2',	NULL,	'O91O1h9Q',	0,	0,	0,	'2016-09-14 15:37:04'),
('17',	'2016-07-30',	'12:00:00',	'2016-08-02',	'12:00:00',	'Downtown, Medina Jemaa Lefna',	'Downtown, Medina Jemaa Lefna',	NULL,	'17',	'gvXPd4M9',	0,	0,	0,	'2016-09-14 15:37:04'),
('18',	'2016-07-30',	'12:00:00',	'2016-08-02',	'12:00:00',	'Marrakech Menara Aeroport',	'Marrakech Menara Aeroport',	NULL,	'18',	'rr4HebBw',	0,	0,	0,	'2016-09-14 15:37:04'),
('19',	'2016-08-29',	'23:00:00',	'2016-08-31',	'23:00:00',	'Marrakech Menara Aeroport',	'Marrakech Menara Aeroport',	NULL,	'19',	'lDnoDZz4',	0,	0,	0,	'2016-09-14 15:37:04'),
('2',	'2016-06-27',	'08:00:00',	'2016-06-30',	'08:30:00',	'1',	'1',	'2',	NULL,	NULL,	1,	0,	0,	'2016-09-14 15:37:04');

DROP TABLE IF EXISTS `slidercars`;
CREATE TABLE `slidercars` (
  `id` char(36) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `car_id` char(36) NOT NULL,
  `cars_order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slider_id` (`slider_id`),
  KEY `car_id` (`car_id`),
  CONSTRAINT `slidercars_ibfk_1` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `slidercars_ibfk_2` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `slidercars` (`id`, `slider_id`, `car_id`, `cars_order`) VALUES
('1',	1,	'1',	1),
('2',	1,	'2',	2),
('3',	1,	'3',	4),
('5',	1,	'4',	4);

DROP TABLE IF EXISTS `slidermedias`;
CREATE TABLE `slidermedias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `front_img` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slider_id` (`slider_id`),
  KEY `media_id` (`media_id`),
  CONSTRAINT `slidermedias_ibfk_4` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `slidermedias_ibfk_5` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `slidermedias` (`id`, `slider_id`, `media_id`, `front_img`) VALUES
(1,	2,	9,	1),
(2,	2,	8,	0),
(3,	2,	7,	0),
(4,	2,	6,	0);

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `slider_type` varchar(10) NOT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_fr` varchar(255) DEFAULT NULL,
  `description_en` tinytext,
  `description_fr` tinytext,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sliders` (`id`, `name`, `slider_type`, `title_en`, `title_fr`, `description_en`, `description_fr`, `disabled`) VALUES
(1,	'home carousel',	'carousel',	'',	'',	'',	'',	0),
(2,	'home slider',	'slider',	'English Title',	'Ce titre est en francais',	'<p>English Description</p>',	'<p>Description en francais</p>',	0);

DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_fr` varchar(45) NOT NULL,
  `name_en` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `types` (`id`, `name_fr`, `name_en`) VALUES
(1,	'Standard',	'Standard'),
(2,	'Mini',	'Mini');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT '134',
  `email` varchar(60) NOT NULL,
  `driverslicense` varchar(60) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` varchar(45) DEFAULT NULL,
  `registrationtoken` varchar(255) NOT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country` (`country`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`country`) REFERENCES `countries` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `role`, `firstname`, `lastname`, `age`, `country`, `email`, `driverslicense`, `phone`, `username`, `password`, `created`, `last_login`, `registrationtoken`, `confirmed`) VALUES
('2',	'user',	'Yassine',	'El khanboubi',	28,	134,	'yassine.elkhanboubi@gmail.com',	'11111111111111',	'0675342668',	'slimshady',	'$2y$10$5U5jw6nxm5t.YTL7ONvjrOed9eej/BSu2eRHxUz9hXiNm5dBGh/Vu',	'2016-05-25 11:03:33',	'',	'',	1),
('3',	'admin',	'admin',	'admin',	16,	134,	'yas-yo@hotmail.com',	'',	NULL,	'admin',	'$2y$10$blF09KtEGDkYW6g2o/.NCe.0S0ifui53HG7olXLWbt4zPtQLtg7sq',	'2016-06-02 08:30:58',	'',	'',	1),
('d582dc7f-2cf3-433c-bc99-bb597ba079cc',	'user',	'Jamal',	'El khanboubi',	NULL,	134,	'elkhanboubi.yassine@gmail.com',	NULL,	NULL,	NULL,	'$2y$10$blF09KtEGDkYW6g2o/.NCe.0S0ifui53HG7olXLWbt4zPtQLtg7sq',	'2016-08-31 12:47:17',	NULL,	'e6dcb5d5ef757d9b2c037f9339ee71ff',	1);

-- 2016-09-20 22:29:15
